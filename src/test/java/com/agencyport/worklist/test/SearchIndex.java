/*
 * Created on Apr 11, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;

import com.agencyport.resource.ResourceException;
import com.agencyport.security.profile.ISecurityProfile;
import com.agencyport.security.profile.impl.SecurityProfileManager;
import com.agencyport.worklist.searchindex.CompiledIndexMapping;
import com.agencyport.worklist.searchindex.ISearchIndexMapping;
import com.agencyport.worklist.security.mock.TestProfileCreator;
import com.agencyport.worklist.spring.SpringApplicationInitializer;
import com.agencyport.worklist.spring.SpringApplicationInitializer.SdkTestContextInitializer;
import com.agencyport.worklist.spring.TestConfig;

/**
 * The SearchIndex class
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes={SpringApplicationInitializer.class, TestConfig.class}, loader=AnnotationConfigWebContextLoader.class, initializers=SdkTestContextInitializer.class)
public class SearchIndex {
    
    
    @Before
    public void createTestProfile(){
        ISecurityProfile securityProfile = TestProfileCreator.createSecurityProfile("agent");
        SecurityProfileManager.get().setSecurityProfile(securityProfile);
    }
    
	
	@Test
	public void getAccountSearchIndex() throws ResourceException{
		String index ="account";
		ISearchIndexMapping indexMapping = CompiledIndexMapping.get(index);
		Assert.assertNotNull(indexMapping);  
	}
	
	@Test
	public void getWorkSearchIndex() throws ResourceException{
		String index ="worklist";
		ISearchIndexMapping indexMapping = CompiledIndexMapping.get(index);
		Assert.assertNotNull(indexMapping);  
	}

}
