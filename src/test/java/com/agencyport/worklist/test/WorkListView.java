/*
 * Created on Apr 11, 2016 by adoss AgencyPort Insurance Services, Inc.
 */

package com.agencyport.worklist.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;

import com.agencyport.resource.ResourceException;
import com.agencyport.security.profile.ISecurityProfile;
import com.agencyport.security.profile.impl.SecurityProfileManager;
import com.agencyport.worklist.security.mock.TestProfileCreator;
import com.agencyport.worklist.spring.SpringApplicationInitializer;
import com.agencyport.worklist.spring.SpringApplicationInitializer.SdkTestContextInitializer;
import com.agencyport.worklist.spring.TestConfig;
import com.agencyport.worklist.view.CompiledWorkListView;

/**
 * The WorkListView class
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes={SpringApplicationInitializer.class, TestConfig.class}, loader=AnnotationConfigWebContextLoader.class, initializers=SdkTestContextInitializer.class)

public class WorkListView {
    
    @Before
    public void createTestProfile(){
        ISecurityProfile securityProfile = TestProfileCreator.createSecurityProfile("agent");
        SecurityProfileManager.get().setSecurityProfile(securityProfile);
    }
	
	@Test
	public void getAccountItemsView() throws ResourceException{
		String viewName ="AccountItemsView";
		CompiledWorkListView compiledWorkListView = CompiledWorkListView.get(viewName);
		Assert.assertNotNull(compiledWorkListView);  
	}
	
	@Test
	public void getAccountsView() throws ResourceException{
		String viewName ="AccountsView";
		CompiledWorkListView compiledWorkListView = CompiledWorkListView.get(viewName);
		Assert.assertNotNull(compiledWorkListView);  
	}
	
	@Test
	public void getQuotesByStatus() throws ResourceException{
		String viewName ="QuotesByStatus";
		CompiledWorkListView compiledWorkListView = CompiledWorkListView.get(viewName);
		Assert.assertNotNull(compiledWorkListView);  
	}
	
	@Test
	public void getWorkItemsRecentAgentQueue() throws ResourceException{
		String viewName ="WorkItemsRecentAgentQueue";
		CompiledWorkListView compiledWorkListView = CompiledWorkListView.get(viewName);
		Assert.assertNotNull(compiledWorkListView);  
	}
	
	@Test
	public void getWorkItemsRecentUWQueue() throws ResourceException{
		String viewName ="WorkItemsRecentUWQueue";
		CompiledWorkListView compiledWorkListView = CompiledWorkListView.get(viewName);
		Assert.assertNotNull(compiledWorkListView);  
	}
	
	@Test
	public void getWorkItemsView() throws ResourceException{
		String viewName ="WorkItemsView";
		CompiledWorkListView compiledWorkListView = CompiledWorkListView.get(viewName);
		Assert.assertNotNull(compiledWorkListView);  
	}

}
