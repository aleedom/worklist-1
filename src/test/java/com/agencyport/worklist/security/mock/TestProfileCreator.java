/*
 * Created on May 4, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.security.mock;

import com.agencyport.id.Id;
import com.agencyport.security.model.IClient;
import com.agencyport.security.model.IRole;
import com.agencyport.security.model.IRoles;
import com.agencyport.security.model.ISubject;
import com.agencyport.security.model.IUserGroup;
import com.agencyport.security.model.IUserGroups;
import com.agencyport.security.model.IUserInfo;
import com.agencyport.security.model.IUserPrincipal;
import com.agencyport.security.model.factory.SecurityModelFactory;
import com.agencyport.security.profile.ISecurityProfile;
import com.agencyport.security.profile.builtin.PortalSecurityProfile;
import com.agencyport.security.profile.impl.SecurityProfileManager;

/**
 * The TestProfileCreator class
 */
public class TestProfileCreator {
    
    public static ISecurityProfile createSecurityProfile(String loginId){
        SecurityModelFactory modelFactory = SecurityModelFactory.get();
        IClient client = modelFactory.createClient(new Id(1000), "wayne", "Wayne Enterprises", null);
        IUserPrincipal user = modelFactory.createUserPrincipal(client, loginId);
        ISubject subject = modelFactory.createSubject(user);
        IUserInfo userInfo = modelFactory.createUserInfo();
        userInfo.setName("Unit", "James", "J");
        subject.setUserInfo(userInfo);
        IRoles roles = modelFactory.createRoles();
        IRole role = modelFactory.createRole(new Id(1000), user);
        roles.add(role);
        IUserGroups usergroups = modelFactory.createUserGroups();
        IUserGroup usergroup = modelFactory.createUserGroup(new Id(100));
        usergroup.addMember(user);
        usergroups.add(usergroup);
        ISecurityProfile securityProfile = new PortalSecurityProfile();
        securityProfile.setSubject(subject);
        securityProfile.setRoles(roles);
        securityProfile.setUserGroups(usergroups);
        SecurityProfileManager.get().setSecurityProfile(securityProfile);
        return securityProfile;
    }

}
