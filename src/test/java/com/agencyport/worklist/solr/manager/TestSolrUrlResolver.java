/*
 * Created on Apr 22, 2014 by dan AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.solr.manager;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;

import com.agencyport.worklist.spring.SpringApplicationInitializer;
import com.agencyport.worklist.spring.SpringApplicationInitializer.SdkTestContextInitializer;
import com.agencyport.worklist.spring.TestConfig;

/**
 * The TestSolrUrlResolver class
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes={SpringApplicationInitializer.class, TestConfig.class}, loader=AnnotationConfigWebContextLoader.class, initializers=SdkTestContextInitializer.class)
public class TestSolrUrlResolver {

	/**
	 * The <code>resolver</code> to be tested.
	 */
	SolrUrlResolver resolver;
	
	/**
	 * Set up for each test.
	 */
	@Before
	public void setUp() {
		resolver = new SolrUrlResolver();
	}
	
	
	/**
	 * Tear down for each test.
	 */
	@After
	public void tearDown() {
		resolver = null;
	}
	
	/**
	 * Test the resolveUrl method.
	 */
	@Test
	public void testResolveUrl() {
		String url = resolver.resolveUrl(SolrManager.INDEX_TYPE_WORKLIST);
		assertTrue(url.endsWith("/solr/worklist"));
		
		url = resolver.resolveUrl(SolrManager.INDEX_TYPE_ACCOUNT);
		assertTrue(url.endsWith("/solr/account"));
		
		url = resolver.resolveUrl("something else");
		assertTrue(url == null);
	}
}
