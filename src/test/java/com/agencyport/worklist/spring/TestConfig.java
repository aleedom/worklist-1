/*
 * Created on Apr 11, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.spring;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * 
 * The Test Config  class
 */
@Configuration
@Profile(SpringApplicationInitializer.TEST_PROFILE)
public class TestConfig {
	
	@Autowired
    private Environment environment; 
    
    @Bean(name="dataSource")
    public DataSource getTestDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        
        dataSource.setDriverClassName(environment.getProperty("jdbc_driver"));
        dataSource.setUrl(environment.getProperty("database_url"));
        dataSource.setUsername(environment.getProperty("database_username"));
        dataSource.setPassword(environment.getProperty("database_password"));

        return (DataSource)dataSource;
    }
    


}
