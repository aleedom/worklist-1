/**
 * Contains REST standard outputters specific to work list processing.
 * @since 5.1
 */
package com.agencyport.worklist.outputter;