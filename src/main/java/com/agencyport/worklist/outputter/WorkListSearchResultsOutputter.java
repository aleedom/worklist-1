/*
 * Created on Jan 13, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.outputter;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import com.agencyport.api.pojo.DataType;
import com.agencyport.api.worklist.pojo.Field;
import com.agencyport.resource.ResourceException;
import com.agencyport.rest.StandardOutputter;
import com.agencyport.rest.StandardResponseEntity;
import com.agencyport.rest.forwardwriter.IForwardWriter;
import com.agencyport.utils.APDate;
import com.agencyport.worklist.pojo.WorkListView;
import com.agencyport.worklist.searchindex.IQueryResults;
import com.agencyport.worklist.searchindex.IQueryResults.IQueryResultRecord;
import com.agencyport.worklist.searchindex.ISearchIndexMapping;
import com.agencyport.worklist.view.CompiledWorkListView;

/**
 * The WorkListSearchResultsOutputter class 
 */
public class WorkListSearchResultsOutputter extends StandardOutputter {
	/**
	 * The <code>queryResults</code> contains the search results from the query call.
	 */
	private final IQueryResults queryResults;
	/**
	 * The <code>workListView</code> is the work list view that ran the query.
	 */
	private final WorkListView workListView;
	/**
	 * The <code>compiledSolrWorkListView</code> is the compiled work list meta data.
	 */
	private final CompiledWorkListView compiledSolrWorkListView;
	
	/**
	 * The <code>solrIndexMap</code> is a reference to the compiled index mapping resource.
	 */
	private final ISearchIndexMapping indexMap;

	/**
	 * Creates an instance.
	 * @param mediaType is the requested media type. 
	 * @param queryResults is the result set from the query.
	 * @param solrWorkListView is the work list view that ran the query.
	 * @param clientName is the name of the client
	 * @throws ResourceException if the compiled work list view could not be accessed.
	 */
	public WorkListSearchResultsOutputter(MediaType mediaType, IQueryResults queryResults, WorkListView solrWorkListView, String clientName) throws ResourceException {
		super(mediaType);
		this.queryResults = queryResults;
		this.workListView = solrWorkListView;
		this.compiledSolrWorkListView = CompiledWorkListView.get(this.workListView.getName());
		this.indexMap = this.compiledSolrWorkListView.getIndexMap();
	}
	
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	protected void renderResults(IForwardWriter fw, StandardResponseEntity entity) throws IOException {
		fw.writeObjectFieldStart(StandardResponseEntity.RESULTS_TAG);
		fw.startAttributeSequence();
		fw.writeNumberField("hits", queryResults.getNumberOfHits());
		fw.writeNumberField("start", queryResults.getStartingRecordNumber());
		fw.endAttributeSequence();
		int docsAvailable = queryResults.getNumberOfResults();
		fw.writeArrayFieldStart("docs");
		for (int docIndex = 0; docIndex < docsAvailable; docIndex++){
			IQueryResultRecord queryResultRecord = queryResults.getQueryResult(docIndex);
			fw.writeStartEntity("doc"); 
			for (Map.Entry<String, Object> entry : queryResultRecord){
				String solrName = entry.getKey();
				Object value = entry.getValue();
				Field supportingField = indexMap.getField(solrName);
				DataType dataType = supportingField.getType(); 
				if (DataType.DATE.equals(dataType)){
					Date solrDate = (Date) value;
					APDate date = new APDate(solrDate);
					fw.writeStringField(supportingField.getId(), date.getLocalizedDate());
				} else if (DataType.TIME.equals(dataType)){
					Date solrDate = (Date) value;
					APDate time = new APDate(solrDate);
					fw.writeStringField(supportingField.getId(), time.getLocalizedDateTime());

					/**
					 * TODO: Field encryption bug logged: DP-736
					 */					
//				} else if (supportingField.getEncrypted()){
//					String decryptedValue = Field.decryptValue(value.toString());
//					fw.writeObjectField(supportingField.getId(), decryptedValue);
				} else {
					fw.writeObjectField(supportingField.getId(), value);
				}
			}
			// end doc
			fw.writeEndObject(); 
			
		}
		fw.writeEndArray();
		fw.writeEndObject();
	}

	/**
	 * @return the query results.
	 */
	protected IQueryResults getQueryResults() {
		return queryResults;
	}

	/**
	 * @return the worklist view.
	 */
	protected WorkListView getSolrWorkListView() {
		return workListView;
	}

	/**
	 * @return the compiled work list view.
	 */
	protected CompiledWorkListView getCompiledSolrWorkListView() {
		return compiledSolrWorkListView;
	}


}
