/*
 * Created on Mar 14, 2016 by ldeane AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.api;

import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.agencyport.rest.IResourceConstants;
import com.agencyport.workitem.model.WorkItemType;
import com.agencyport.worklist.search.indexing.ISolrIndexer;
import com.agencyport.worklist.searchindex.IndexNames;
import com.agencyport.worklist.solr.manager.SolrManager;

/**
 * 
 * The WorkItemSolrIndexResource class is responsible for generating the SolrInputDocument for Work Item requests and 
 * adding or updating the content into the solr index.
 */
@Path(IResourceConstants.WORKLIST_WORKITEM_SOLR_API_PATH)
public class WorkItemSolrIndexResource extends BaseSolrIndexResource implements ISolrIndexer {
	
	
	/**
	 * The index method invokes the BaseSolrIndexResource to add the provided work item into the solr index
	 * @param id is the id of the work item to be added
	 * @param workItemContent in the content of the work item in XML format
	 * @return Response whether indexing was successful
	 */
	@POST
	@Path(IResourceConstants.DOCS_PATH)
	@Produces({MediaType.APPLICATION_JSON})
	public Response index(@PathParam("id") String id, String workItemContent){		
		return indexItem(getAccepts(), workItemContent, this);
	}
	
	/**
	 * The delete method invokes the BaseSolrIndexResource to remove the provided item from the solr index
	 * @param id is the id of the work item to be removed
	 * @return Response whether removal was successful
	 */
	@DELETE
	@Path(IResourceConstants.DOCS_PATH)
	@Produces({MediaType.APPLICATION_JSON})
	public Response delete(@PathParam("id") String id){		
		return deleteItemIndex(getAccepts(), id, this);
	}
	
	/**
	 * The commit method invokes the BaseSolrIndexResource to commit pending documents or updates to make them available for search.
	 * @return Response whether committing was successful
	 */
	@POST
	@Path(IResourceConstants.COMMITS_PATH)
	@Produces({MediaType.APPLICATION_JSON})
	public Response commit(){		
		return commitIndex(getAccepts(), this);
	}
	
	@Override
	public String getSolrIndexMapName() {
		return IndexNames.WORKITEM_SOLR_INDEX_NAME;
	}
	
	@Override
	public WorkItemType getWorkItemType() {
		return WorkItemType.REGULAR_LOB_WORK_ITEM_TYPE;
	}
	
	@Override
	public String getSolrIndexType() {
		return SolrManager.INDEX_TYPE_WORKLIST;
	}

}
