/*
 * Created on Dec 5, 2014 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.api;

import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.agencyport.api.BaseResource;
import com.agencyport.api.outputter.PojoContainerOutputter;
import com.agencyport.api.outputter.pojo.PojoCollectionContainer;
import com.agencyport.api.outputter.pojo.PojoContainer;
import com.agencyport.api.outputter.pojo.PojoObjectContainer;
import com.agencyport.api.verification.Verifier;
import com.agencyport.factory.GenericFactory;
import com.agencyport.factory.InstantiationInfo;
import com.agencyport.rest.Serializer;
import com.agencyport.rest.StandardOutputter;
import com.agencyport.rest.Status;
import com.agencyport.security.profile.ISecurityProfile;
import com.agencyport.shared.APException;
import com.agencyport.utils.PerfObject;
import com.agencyport.utils.PerfObjectCollector;
import com.agencyport.utils.text.StringUtilities;
import com.agencyport.workitem.model.IWorkListConstants;
import com.agencyport.workitem.model.WorkItemType;
import com.agencyport.worklist.factory.WorklistFactory;
import com.agencyport.worklist.outputter.WorkListSearchResultsOutputter;
import com.agencyport.worklist.pojo.Filter;
import com.agencyport.worklist.pojo.FilterOption;
import com.agencyport.worklist.pojo.Operators;
import com.agencyport.worklist.pojo.QueryInfo;
import com.agencyport.worklist.pojo.SavedSearch;
import com.agencyport.worklist.pojo.SortInfo;
import com.agencyport.worklist.pojo.WorkListView;
import com.agencyport.worklist.searchindex.IQueryResults;
import com.agencyport.worklist.searchindex.IWorkListSearchProvider;
import com.agencyport.worklist.searchindex.SearchIndexFactory;
import com.agencyport.worklist.verfication.WorkListViewNameVerifier;
import com.agencyport.worklist.verfication.WorkListViewVerifier;
import com.agencyport.worklist.view.CompiledWorkListView;
import com.agencyport.worklist.view.IWorkListViewProvider;

/**
 * The BaseWorkItemListResource class provides a common derivation point
 * for both account and work item list resources.
 * @since 5.1 
 */
public class BaseWorkItemListResource extends BaseResource {

	/**
	 * Constructs an instance.
	 */
	public BaseWorkItemListResource() {
	}
	
	/**
	 * If the view is not specified then this method will provide a reasonable default depending on the work item type. All non null
	 * views are returned unadulterated.
	 * @param view is the view.
	 * @param workItemType is the work item type.
	 * @return the normalized view.
	 */
	protected final String normalizeView(String view, WorkItemType workItemType){
		if (StringUtilities.isEmpty(view)){
			return workItemType.equals(WorkItemType.REGULAR_LOB_WORK_ITEM_TYPE) ? IWorkListConstants.WORK_ITEMS_LIST_TYPE : IWorkListConstants.ACCOUNTS_WORK_LIST_TYPE;
		} else {
			return view;
		}
	}
	
	/**
	 * Verifies the view name.
	 * @param viewName is the view name.
	 * @param viewResource is the CompiledWorkListView from the configuration service
	 * @throws APException ResourceException propagated from {@link WorkListViewNameVerifier#WorkListViewNameVerifier(String, CompiledWorkListView)} or
	 * SecurityException propagated from {@link Verifier#verify(BaseResource)} or 
	 * RestValidationException from {@link Verifier#verify(BaseResource)}
	 */
	public void verifyViewName(String viewName, CompiledWorkListView viewResource) throws APException {
		Verifier verifier = new WorkListViewNameVerifier(viewName, viewResource);
		verifier.verify(this);
	}
	
	/**
	 * Returns an array of all of the work item / account based filters for a given view.
	 * @param acceptMediaType is the HTTP header Accept value.
	 * @param workItemType is the work item type
	 * @param viewName is the view name.
	 * @return a response of all of the work item / account based filters.
	 */
	public Response getFilters(MediaType acceptMediaType, WorkItemType workItemType, String viewName) {
		final String functionName = "getFilters";
		// Start performance collector for this request
        long performanceTranId = PerfObjectCollector.beginTran();
		PerfObject perfObject = startPerfObject(functionName);
		try {			
			IWorkListViewProvider provider = WorklistFactory.createWorkListViewProvider();
			CompiledWorkListView workListViewResource = provider.getWorkListViewResource(workItemType, viewName, getSecurityProfile());
			verifyViewName(viewName, workListViewResource);
			List<Filter> filters = provider.getFilters(workListViewResource, getSecurityProfile());
			PojoContainer pojoContainer = new PojoCollectionContainer("filters", Filter.class, filters); 
			return generateResponse(acceptMediaType, new PojoContainerOutputter(acceptMediaType, pojoContainer));
		} catch (Exception exception){
			return generateExceptionResponse(acceptMediaType, exception, Status.FILTER_ACCESS_ISSUE, functionName);
		} finally {
			perfObject.stop();
			PerfObjectCollector.endTran(performanceTranId);
		}
	}
	/**
	 * Returns all of the filter options for a given filter name.
	 * @param acceptMediaType is the HTTP header Accept value.
	 * @param filterName is the sort info name. This is required.
	 * @param viewName is the name of the view.
	 * @return a response containing all  of the filter options for a given filter name.
	 */
	public Response getFilterOptions(MediaType acceptMediaType, String filterName, String viewName){
		final String functionName = "getFilterOptions";
		// Start performance collector for this request
        long performanceTranId = PerfObjectCollector.beginTran();
		PerfObject perfObject = startPerfObject(functionName);
		try {			
			ISecurityProfile securityProfile = getSecurityProfile();
			CompiledWorkListView workListViewResource = CompiledWorkListView.get(viewName);
			verifyViewName(viewName, workListViewResource);
			
			IWorkListViewProvider provider = WorklistFactory.createWorkListViewProvider();
			Filter filter = new Filter(filterName);
			List<FilterOption> filterOptions = provider.getFilterOptions(workListViewResource, filter, viewName, securityProfile);
			PojoContainer pojoContainer = new PojoCollectionContainer("filterOptions", FilterOption.class, filterOptions);
			return generateResponse(acceptMediaType, new PojoContainerOutputter(acceptMediaType, pojoContainer));
		} catch (Exception exception){
			return generateExceptionResponse(acceptMediaType, exception, Status.FILTER_ACCESS_ISSUE, functionName);
		} finally {
			perfObject.stop();
			PerfObjectCollector.endTran(performanceTranId);
		}
	}
	

	/**
	 * Returns the sort information for a given view.
	 * @param acceptMediaType is the HTTP header Accept value.
	 * @param workItemType is the work item type
	 * @param viewName is the view name.
	 * @return a response of the sort information for a given view.
	 */
	public Response getSortInfo(MediaType acceptMediaType, WorkItemType workItemType, String viewName) {
		final String functionName = "getSortInfo";
		// Start performance collector for this request
        long performanceTranId = PerfObjectCollector.beginTran();
		PerfObject perfObject = startPerfObject(functionName);
		try {			
			ISecurityProfile securityProfile = getSecurityProfile();			
			IWorkListViewProvider provider = WorklistFactory.createWorkListViewProvider();
			CompiledWorkListView workListViewResource = provider.getWorkListViewResource(workItemType, viewName, securityProfile);
			verifyViewName(viewName, workListViewResource);
			SortInfo sortInfo = provider.getSortInfo(workListViewResource, securityProfile);
			PojoContainer pojoContainer = new PojoObjectContainer("sortinfo", sortInfo); 
			return generateResponse(acceptMediaType, new PojoContainerOutputter(acceptMediaType, pojoContainer));
		} catch (Exception exception){
			return generateExceptionResponse(acceptMediaType, exception, Status.SORT_INFO_ACCESS_ISSUE, functionName);
		} finally {
			perfObject.stop();
			PerfObjectCollector.endTran(performanceTranId);
		}
	}
	
	/**
	 * Returns the query information for a given view.
	 * @param acceptMediaType is the HTTP header Accept value.
	 * @param workItemType is the work item type
	 * @param viewName is the view name.
	 * @return a response of the query information for a given view.
	 */
	public Response getQueryInfo(MediaType acceptMediaType, WorkItemType workItemType, String viewName) {
		final String functionName = "getQueryInfo";
		// Start performance collector for this request
        long performanceTranId = PerfObjectCollector.beginTran();
		PerfObject perfObject = startPerfObject(functionName);
		try {			
			ISecurityProfile securityProfile = getSecurityProfile();			
			IWorkListViewProvider provider = WorklistFactory.createWorkListViewProvider();
			CompiledWorkListView workListViewResource = provider.getWorkListViewResource(workItemType, viewName, securityProfile);
			verifyViewName(viewName, workListViewResource);
			QueryInfo queryInfo = provider.getQueryInfo(workListViewResource, securityProfile);
			PojoContainer pojoContainer = new PojoObjectContainer("queryinfo", queryInfo); 
			return generateResponse(acceptMediaType, new PojoContainerOutputter(acceptMediaType, pojoContainer));
		} catch (Exception exception){
			return generateExceptionResponse(acceptMediaType, exception, Status.QUERY_INFO_ACCESS_ISSUE, functionName);
		} finally {
			perfObject.stop();
			PerfObjectCollector.endTran(performanceTranId);
		}
	}
	/**
	 * Returns the worklist view. 
	 * @param acceptMediaType is the HTTP header Accept value.
	 * @param workItemType is the work item type
	 * @param viewName is the view name.
	 * @param savedSearchId is the id of the saved search information.
	 * @return the worklist view.
	 */
	public Response getView(MediaType acceptMediaType, WorkItemType workItemType, String viewName, int savedSearchId){
		final String functionName = "getView";
		// Start performance collector for this request
        long performanceTranId = PerfObjectCollector.beginTran();
		PerfObject perfObject = startPerfObject(functionName);
		try {		
			ISecurityProfile securityProfile = getSecurityProfile();
			IWorkListViewProvider provider = WorklistFactory.createWorkListViewProvider();
			CompiledWorkListView workListViewResource = provider.getWorkListViewResource(workItemType, viewName, securityProfile);
			verifyViewName(viewName, workListViewResource);
			WorkListView view = provider.getView(workListViewResource, workItemType, viewName, savedSearchId, securityProfile, true);
			PojoContainer pojoContainer = new PojoObjectContainer("workListView", view); 
			return generateResponse(acceptMediaType, new PojoContainerOutputter(acceptMediaType, pojoContainer));
		} catch (Exception exception){
			return generateExceptionResponse(acceptMediaType, exception, Status.VIEW_ACCESS_ISSUE, functionName);
		} finally {
			perfObject.stop();
			PerfObjectCollector.endTran(performanceTranId);
		}
	}
	
	/**
	 * Saves the worklist view for the current user.
	 * @param acceptMediaType is the HTTP header Accept value.
	 * @param contentType is the content type of the search information buffer.
	 * @param workItemType is the work item type
	 * @param viewName is the view name.
	 * @param workListView is the work list view.
	 * @return if the save was successful, a successful response with a copy of the updated view.
	 */
	public Response saveView(MediaType acceptMediaType, MediaType contentType, WorkItemType workItemType, String viewName, String workListView) {
		final String functionName = "saveView";
		// Start performance collector for this request
        long performanceTranId = PerfObjectCollector.beginTran();
		PerfObject perfObject = startPerfObject(functionName);
		try {			
			IWorkListViewProvider provider = WorklistFactory.createWorkListViewProvider();
			WorkListView view = Serializer.get(contentType).deserialize(workListView, WorkListView.obtainClassInfo());
			ISecurityProfile securityProfile = getSecurityProfile();
			CompiledWorkListView workListViewResource = provider.getWorkListViewResource(workItemType, viewName, securityProfile);
			verify(WorkListViewVerifier.class, functionName, InstantiationInfo.create(view, workListViewResource, workItemType, securityProfile, ISecurityProfile.class));
			provider.saveView(workItemType, viewName, securityProfile, view);
			PojoContainer pojoContainer = new PojoObjectContainer("workListView", view); 
			return generateResponse(acceptMediaType, new PojoContainerOutputter(acceptMediaType, pojoContainer));
		} catch (Exception exception){
			return generateExceptionResponse(acceptMediaType, exception, Status.VIEW_ACCESS_ISSUE, functionName);
		} finally {
			perfObject.stop();
			PerfObjectCollector.endTran(performanceTranId);
		}
	}
	
	/**
	 * Deletes a worklist view record.
	 * @param acceptMediaType is the HTTP header Accept value.
	 * @param workItemType is the work item type
	 * @param viewName is the view name.
	 * @param savedSearchId is the id of the saved search information.
	 * @return a successful response if the delete operation was successful with the current list of saved searches.
	 */
	public Response deleteView(MediaType acceptMediaType, WorkItemType workItemType, String viewName, int savedSearchId) {
		final String functionName = "deleteView";
		// Start performance collector for this request
        long performanceTranId = PerfObjectCollector.beginTran();
		PerfObject perfObject = startPerfObject(functionName);
		try {			
			ISecurityProfile securityProfile = getSecurityProfile();
			IWorkListViewProvider provider = WorklistFactory.createWorkListViewProvider();
			CompiledWorkListView workListViewResource = provider.getWorkListViewResource(workItemType, viewName, securityProfile);
			verifyViewName(viewName, workListViewResource);
			provider.deleteView(workItemType, viewName, savedSearchId, securityProfile);
			return this.getSavedSearches(acceptMediaType, workItemType, viewName);
		} catch (Exception exception){
			return generateExceptionResponse(acceptMediaType, exception, Status.VIEW_ACCESS_ISSUE, functionName);
		} finally {
			perfObject.stop();
			PerfObjectCollector.endTran(performanceTranId);
		}
	}
	/**
	 * Gets the operator meta data.
	 * @param acceptMediaType is the HTTP header Accept value.
	 * @param workItemType is the work item type
	 * @return a successful response if the operators could be gathered.
	 */
	public Response getOperators(MediaType acceptMediaType, WorkItemType workItemType) {
		final String functionName = "getOperators";
		// Start performance collector for this request
        long performanceTranId = PerfObjectCollector.beginTran();
		PerfObject perfObject = startPerfObject(functionName);
		try {
			Operators operators = Operators.create(workItemType);
			PojoContainer pojoContainer = new PojoObjectContainer("operatorinfo", operators); 
			return generateResponse(acceptMediaType, new PojoContainerOutputter(acceptMediaType, pojoContainer));
		} catch (Exception exception){
			return generateExceptionResponse(acceptMediaType, exception, Status.VIEW_ACCESS_ISSUE, functionName);
		} finally {
			perfObject.stop();
			PerfObjectCollector.endTran(performanceTranId);
		}
	}
	
	/**
	 * Runs a query.
	 * @param acceptMediaType is the HTTP header Accept value.
	 * @param contentType is the content type of the work list view buffer.
	 * @param workItemType is the work item type
	 * @param viewName is the view name.
	 * @param workListView is the work list view.
	 * @return the results of the query.
	 */
	public Response query(MediaType acceptMediaType, MediaType contentType, WorkItemType workItemType, String viewName, String workListView) {
		final String functionName = "query";
		// Start performance collector for this request
        long performanceTranId = PerfObjectCollector.beginTran();
		PerfObject perfObject = startPerfObject(functionName);
		try {			
			WorkListView view = Serializer.get(contentType).deserialize(workListView, WorkListView.obtainClassInfo()); 
			view.setName(viewName);			
			ISecurityProfile securityProfile = getSecurityProfile();
			IWorkListSearchProvider provider = SearchIndexFactory.get().createWorkListSearchProvider(acceptMediaType, workItemType, view, securityProfile);
			// now validate the de-serialized view.
			verify(WorkListViewVerifier.class, functionName, InstantiationInfo.create(view, provider.getWorkListViewResource(), workItemType, securityProfile, ISecurityProfile.class));
			IQueryResults results = provider.search();
			customProcessQueryResults(acceptMediaType, results, view);
			StandardOutputter outputter = GenericFactory.create("application.worklist.searchresultsOutputter", 
														WorkListSearchResultsOutputter.class.getName(), acceptMediaType, results,IQueryResults.class, view, securityProfile.getClient().getName());
			return generateResponse(acceptMediaType, outputter);
		} catch (Exception exception){
			return generateExceptionResponse(acceptMediaType, exception, Status.QUERY_ISSUE, functionName);
		} finally {
			perfObject.stop();
			PerfObjectCollector.endTran(performanceTranId);
		}
	}
	
	/**
	 * @param acceptMediaType is the HTTP header Accept value.
	 * @param results is the instance of IQueryResults holding the results from solr query
	 * @param view is the instance of WorkListView being serviced
	 */
	private void customProcessQueryResults(MediaType acceptMediaType, IQueryResults results, WorkListView view) {		
	}

	/**
	 * Retrieves the saved searches for a given view name for a given user.
	 * @param acceptMediaType is the HTTP header Accept value.
	 * @param workItemType is the work item type
	 * @param viewName is the view name.
	 * @return a response with the list of saved search items.
	 */
	public Response getSavedSearches(MediaType acceptMediaType, WorkItemType workItemType, String viewName){
		final String functionName = "getSavedSearches";
		// Start performance collector for this request
        long performanceTranId = PerfObjectCollector.beginTran();
		PerfObject perfObject = startPerfObject(functionName);
		try {			
			ISecurityProfile securityProfile = getSecurityProfile();
			IWorkListViewProvider provider = WorklistFactory.createWorkListViewProvider();
			CompiledWorkListView workListViewResource = provider.getWorkListViewResource(workItemType, viewName, securityProfile);
			verifyViewName(viewName, workListViewResource);
			List<SavedSearch> savedSearches = provider.getSavedSearches(workItemType, viewName, securityProfile);
			PojoContainer pojoContainer = new PojoCollectionContainer("savedSearches", SavedSearch.class, savedSearches); 
			return generateResponse(acceptMediaType, new PojoContainerOutputter(acceptMediaType, pojoContainer));
		} catch (Exception exception){
			return generateExceptionResponse(acceptMediaType, exception, Status.SAVED_SEARCHES_ACCESS_ISSUE, functionName);
		} finally {
			perfObject.stop();
			PerfObjectCollector.endTran(performanceTranId);
		}
		
	}


}
