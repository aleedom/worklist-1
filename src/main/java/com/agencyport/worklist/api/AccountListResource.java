/*
 * Created on Dec 1, 2014 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.api;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.agencyport.rest.IConstants;
import com.agencyport.rest.IResourceConstants;
import com.agencyport.workitem.model.WorkItemType;

/**
 * The AccountListResource class provides filtering and sorting operations pertaining to account list processing.
 * @since 5.1 
 */
@Path(IResourceConstants.WORKLIST_API_PATH + IResourceConstants.ACCOUNT_PATH)
public final class AccountListResource extends BaseWorkItemListResource  {

	/**
	 * Constructs an instance.
	 */
	public AccountListResource() {
	}
	
	/**
	 * Returns an array of all of the filters for the account list context.
	 * @param viewName is an optional view name. Defaults to IWorkListConstants#ACCOUNTS_WORK_LIST_TYPE is not designated.
	 * @return a response of all of the filters for the account list context.
	 */
	@GET
	@Path(IConstants.FILTERS_PATH)
	public Response getFilters(@QueryParam(IConstants.VIEW_NAME) String viewName) {
		return getFilters(getAccepts(), WorkItemType.ACCOUNT_WORK_ITEM_TYPE, normalizeView(viewName, WorkItemType.ACCOUNT_WORK_ITEM_TYPE));
	}

	/**
	 * Returns the sort information for the account list context.
	 * @param viewName is an optional view name.
	 * @return a response of the sort information for the account list context.
	 */
	@GET
	@Path(IConstants.SORT_INFO_PATH)
	public Response getSortInfo(@QueryParam(IConstants.VIEW_NAME) String viewName) {
		return getSortInfo(getAccepts(), WorkItemType.ACCOUNT_WORK_ITEM_TYPE, normalizeView(viewName, WorkItemType.ACCOUNT_WORK_ITEM_TYPE));
	}
	/**
	 * Returns query info for the account list context.
	 * @param viewName is an optional view name.
	 * @return a response the query info for the account list context.
	 */
	@GET
	@Path(IConstants.QUERY_INFO_PATH)
	public Response getQueryInfo(@QueryParam(IConstants.VIEW_NAME) String viewName) {
		return getQueryInfo(getAccepts(), WorkItemType.ACCOUNT_WORK_ITEM_TYPE, normalizeView(viewName, WorkItemType.ACCOUNT_WORK_ITEM_TYPE));
	}
	
	/**
	 * Returns the filter options for the given filter name.
	 * @param filterName is the name of the filter.
	 * @param viewName is the name of the view.
	 * @return a response of all of the filter options for the given filter name.
	 */
	@GET
	@Path(IConstants.FILTER_PATH)
	public Response getFilterOptions(@PathParam(IConstants.FILTER_NAME) String filterName, @QueryParam(IConstants.VIEW_NAME) String viewName){
		return getFilterOptions(getAccepts(), filterName, normalizeView(viewName, WorkItemType.ACCOUNT_WORK_ITEM_TYPE));
	}
	/**
	 * Returns the view information for a given view. 
	 * @param viewName is the view name.
	 * @param savedSearchId is the saved search id.
	 * @return the view information for a given view.
	 */
	@GET
	@Path(IConstants.VIEW_PATH)
	public Response getView(@PathParam(IConstants.VIEW_NAME) String viewName, @QueryParam(IConstants.SAVED_SEARCH_ID) int savedSearchId) {
		return getView(getAccepts(), WorkItemType.ACCOUNT_WORK_ITEM_TYPE, viewName, savedSearchId);
	}
	/**
	 * Saves the view for the current user. 
	 * @param viewName is the view name.
	 * @param view is the view to save.
	 * @return a success response if the view was saved.
	 *  
	 */
	@PUT
	@Path(IConstants.VIEW_PATH)
	public Response saveView(@PathParam(IConstants.VIEW_NAME) String viewName, String view) {
		return saveView(getAccepts(), getContentType(), WorkItemType.ACCOUNT_WORK_ITEM_TYPE, viewName, view);
	}
	/**
	 * Deletes the search info for the current user. 
	 * @param viewName is the view name.
	 * @param savedSearchId is the saved search id.
	 * @return a success response if the delete operation was saved.
	 */
	@DELETE
	@Path(IConstants.VIEW_PATH)
	public Response deleteView(@PathParam(IConstants.VIEW_NAME) String viewName, @QueryParam(IConstants.SAVED_SEARCH_ID) int savedSearchId) {
		return deleteView(getAccepts(), WorkItemType.ACCOUNT_WORK_ITEM_TYPE, viewName, savedSearchId);
	}
	/**
	 * Returns the operators meta data information. 
	 * @return the operators meta data information
	 */
	@GET
	@Path(IConstants.OPERATORS_INFO_PATH)
	@Produces({MediaType.APPLICATION_JSON})
	public Response getOperators() {
		return getOperators(getAccepts(), WorkItemType.ACCOUNT_WORK_ITEM_TYPE);
	}
	
	/**
	 * Sends a query to the account index.
	 * @param viewName is the view name.
	 * @param view is the view that contains all of the filtering, sorting and query information.
	 * @return the results of the query.
	 */
	@POST
	@Path(IConstants.VIEW_PATH)
	public Response query(@PathParam(IConstants.VIEW_NAME) String viewName, String view){
		return query(getAccepts(), getContentType(), WorkItemType.ACCOUNT_WORK_ITEM_TYPE, viewName, view);
	}
	/**
	 * Retrieves the saved searches for a given view name for a given user.
	 * @param viewName is the view name.
	 * @return a response with the list of saved search items.
	 */
	@GET
	@Path(IConstants.SAVED_SEARCHES_PATH)
	public Response getSavedSearches(@QueryParam(IConstants.VIEW_NAME) String viewName){
		return getSavedSearches(getAccepts(), WorkItemType.ACCOUNT_WORK_ITEM_TYPE, viewName);
	}
}
