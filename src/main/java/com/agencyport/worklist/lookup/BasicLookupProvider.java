/*
 * Created on Feb 23, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.lookup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.ws.rs.core.MultivaluedMap;

import org.jdom2.Element;

import com.agencyport.account.model.IAccount.AccountType;
import com.agencyport.api.worklist.pojo.FilterType;
import com.agencyport.domXML.widgets.LOBCode;
import com.agencyport.locale.ILocaleConstants;
import com.agencyport.locale.IResourceBundle;
import com.agencyport.locale.ResourceBundleManager;
import com.agencyport.logging.LoggingManager;
import com.agencyport.resource.RestResource;
import com.agencyport.resource.RestResponse;
import com.agencyport.rest.AtomLink;
import com.agencyport.rest.IConstants;
import com.agencyport.security.model.IPrincipal;
import com.agencyport.security.model.IRoles;
import com.agencyport.security.profile.ISecurityProfile;
import com.agencyport.security.profile.impl.SecurityProfileManager;
import com.agencyport.shared.APException;
import com.agencyport.trandef.Transaction;
import com.agencyport.utils.AppProperties;
import com.agencyport.utils.PerfObject;
import com.agencyport.utils.PerfObjectCollector;
import com.agencyport.utils.text.StringUtilities;
import com.agencyport.webshared.IWebsharedConstants;
import com.agencyport.webshared.URLBuilder;
import com.agencyport.workitem.impl.WorkItemAction;
import com.agencyport.workitem.model.IWorkListConstants;
import com.agencyport.workitem.model.WorkItemType;
import com.agencyport.worklist.lookup.pojo.Code;
import com.agencyport.worklist.lookup.pojo.CodeList;

/**
 * The BasicLookupProvider class is the basic default lookup provider. It serves up code lists in the basic realm of lookup code lists 
 * relating to work list rendering. 
 * @since 5.1
 */
public class BasicLookupProvider implements ILookupProvider {
	
	/**
	 * The <code>LOGGER</code> is a copy of our logger.
	 */
	private static final Logger LOGGER = LoggingManager.getLogger(BasicLookupProvider.class.getPackage().getName());
	
	/**
	 * The <code>BASIC_REALM_PROVIDERS</code> is the map of private "basic" lookup providers that actually have the knowledge
	 * on how to build code list instances for a given name.
	 */
	private static final Map<String, ILookupProvider> BASIC_REALM_PROVIDERS = loadBasicLookupProviders();
	
	
	/**
	 * Initializes all of the internal lookup providers and loads them into a map keyed by their respective,
	 * unique name.
	 * @return a map of lookup providers keyed by their name.
	 */
	private static Map<String, ILookupProvider> loadBasicLookupProviders(){
		Map<String, ILookupProvider> map = new HashMap<>();
		map.put(WorkItemStatusLookupProvider.NAME, new WorkItemStatusLookupProvider());
		map.put(LOBLookupProvider.NAME, new LOBLookupProvider());
		map.put(TransactionTypeLookupProvider.NAME, new TransactionTypeLookupProvider());
		map.put(AccountTypeLookupProvider.NAME, new AccountTypeLookupProvider());
		map.put(NewWorkItemLinkLookupProvider.NAME, new NewWorkItemLinkLookupProvider());
		return map;
	}
	
	/**
	 * The WorkItemStatusLookupProvider class serves up the work item status lookup code list.
	 */
	private static final class WorkItemStatusLookupProvider implements ILookupProvider {
		/**
		 * The <code>NAME</code> is this lookup provider's name.
		 */
		private static final String NAME = FilterType.STATUS.name();

		/** 
		 * {@inheritDoc}
		 */ 
		@Override
		public CodeList get(LookupRealm realm, String name, MultivaluedMap<String, String> queryParams, ISecurityProfile securityProfile) throws APException{
		    PerfObject perfObject = PerfObjectCollector.startNew(this.getClass().getName() + ".get" );
		    try {
    			CodeList codeList = CodeList.create(realm, name);
    			List<Code> listCodes = codeList.getCodes();
    			
    			String workflowEndpoint = AppProperties.getAppProperties().getStringProperty("workflow_svc_url");
    			
    			RestResource res = new RestResource();
    			
    			res.initialize(workflowEndpoint + "/lobwistatus");
    			RestResponse response = res.read();
    			
    			for (Element status : response.getResults().getObject().getChildren("status")){
    				listCodes.add(new Code(status.getChildText("mnemomic"), status.getChildText("afterChangeStatusTitle")));
    			}
    			
    			return codeList;
		    }finally {
		        perfObject.stop();
		    }
		}

		/** 
		 * {@inheritDoc}
		 */ 
		@Override
		public void addLinks(List<AtomLink> atomLinks, LookupRealm realm) throws APException{
			atomLinks.add(CodeList.createAtomLink(true, realm, NAME));
		}

	}
	
	
	/**
	 * The LOBLookupProvider class serves up the LOB lookup code list.
	 */
	private static final class LOBLookupProvider implements ILookupProvider {
		/**
		 * The <code>NAME</code> is this lookup provider's name.
		 */
		private static final String NAME = FilterType.LOB.name();

		/** 
		 * {@inheritDoc}
		 */ 
		@Override
		public CodeList get(LookupRealm realm, String name, MultivaluedMap<String, String> queryParams, ISecurityProfile securityProfile) throws APException{
			Set<LOBCode> lobCodes = new HashSet<>();
			IRoles roles = securityProfile.getRoles();
			IPrincipal principal = securityProfile.getSubject().getPrincipal();
				
			//TODO this wont work, we need to call the configuration service here
			lobCodes.add(LOBCode.getLOBType("WORK"));
//			for(Transaction transaction : TransactionDefinitionManager.getTransactions(false)) {
//				String permissionRequired = "access" + transaction.getLobCode().getCode();
//				if (roles.checkPermission(principal, permissionRequired)){
//					lobCodes.add(transaction.getLobCode());
//				} else {
//					LOGGER.info(String.format("Current user is not authorized to access work items with LOB code: %s", transaction.getLobCode()));
//				}
//			}
			CodeList codeList = CodeList.create(realm, name);
			List<Code> listCodes = codeList.getCodes();
			for (LOBCode lobCode : lobCodes){
				listCodes.add(new Code(lobCode.getCode(), lobCode.getDescription()));
			}
			return codeList;
		}

		/** 
		 * {@inheritDoc}
		 */ 
		@Override
		public void addLinks(List<AtomLink> atomLinks, LookupRealm realm) throws APException{
			atomLinks.add(CodeList.createAtomLink(true, realm, NAME));
		}
	}
	
	/**
	 * The TransactionTypeLookupProvider class serves up the transaction type code list.
	 */
	private static final class TransactionTypeLookupProvider implements ILookupProvider {
		/**
		 * The <code>NAME</code> is this lookup provider's name.
		 */
		private static final String NAME = FilterType.TRANSACTION_TYPE.name();

		/** 
		 * {@inheritDoc}
		 */ 
		@Override
		public CodeList get(LookupRealm realm, String name, MultivaluedMap<String, String> queryParams, ISecurityProfile securityProfile) throws APException {
			Map<String, Transaction> transactionsByType = new HashMap<>();
			//TODO this wont work, we need to call the configuration service here

//			for (Transaction transaction : TransactionDefinitionManager.getTransactions(false)) {
//				transactionsByType.put(transaction.getType(), transaction);
//			}
			CodeList codeList = CodeList.create(realm, name);
			List<Code> listCodes = codeList.getCodes();
			for (Transaction transaction : transactionsByType.values()){
				listCodes.add(new Code(transaction.getType(), transaction.getLocalizedType()));
			}
			//TODO: remove this workaround
			listCodes.add(new Code("NEW_BUSINESS", "New Business"));
			return codeList;
		}

		/** 
		 * {@inheritDoc}
		 */ 
		@Override
		public void addLinks(List<AtomLink> atomLinks, LookupRealm realm) throws APException{
			atomLinks.add(CodeList.createAtomLink(true, realm, NAME));
		}
	}
	/**
	 * The AccountTypeLookupProvider class serves up the account type code list.
	 */
	private static final class AccountTypeLookupProvider implements ILookupProvider {
		/**
		 * The <code>NAME</code> is this lookup provider's name.
		 */
		private static final String NAME = FilterType.ACCOUNT_TYPE.name();

		/** 
		 * {@inheritDoc}
		 */ 
		@Override
		public CodeList get(LookupRealm realm, String name, MultivaluedMap<String, String> queryParams, ISecurityProfile securityProfile) throws APException {
			IResourceBundle rb = ResourceBundleManager.get().getBundle(ILocaleConstants.ACCOUNT_MANAGEMENT_BUNDLE);
			CodeList codeList = CodeList.create(realm, name);
			List<Code> listCodes = codeList.getCodes();
			listCodes.add(new Code(AccountType.C.name(), rb.getString("account.management.label.company", "Commercial")));
			listCodes.add(new Code(AccountType.P.name(), rb.getString("account.management.label.person", "Personal")));
			listCodes.add(new Code(AccountType.U.name(), rb.getString("account.management.label.unknown", "Unknown")));
			return codeList;
		}

		/** 
		 * {@inheritDoc}
		 */ 
		@Override
		public void addLinks(List<AtomLink> atomLinks, LookupRealm realm) throws APException{
			atomLinks.add(CodeList.createAtomLink(true, realm, NAME));
		}
	}
	
	/**
	 * The NewWorkItemLinkLookupProvider class serves up the .
	 */
	private static final class NewWorkItemLinkLookupProvider implements ILookupProvider {
		/**
		 * The <code>COPY_ACTION</code> is a constant for Copy action
		 */
		private static final String COPY_ACTION = "Copy";
		/**
		 * The <code>WORKITEMS_TYPE</code> is a constant for the workitems
		 */
		private static final String WORKITEMS_TYPE = "workitems";
		/**
		 * The <code>ACCOUNTS_TYPE</code> is a constant for the accounts
		 */
		private static final String ACCOUNTS_TYPE = "accounts";
		/**
		 * The <code>NAME</code> is this lookup provider's name.
		 */
		private static final String NAME = "NewWorkItemLink";

		/** 
		 * {@inheritDoc}
		 */ 
		@Override
		public CodeList get(LookupRealm realm, String name, MultivaluedMap<String, String> queryParams, ISecurityProfile securityProfile) throws APException{
			if(securityProfile == null){
				securityProfile = SecurityProfileManager.get().acquire();
			}
			IRoles roles = securityProfile.getRoles();
			IPrincipal principal = securityProfile.getSubject().getPrincipal();
			CodeList codeList = CodeList.create(realm, name);
			List<Code> listCodes = codeList.getCodes();
			WorkItemType workItemType = WorkItemType.REGULAR_LOB_WORK_ITEM_TYPE;
			String worklistType = IWorkListConstants.DEFAULT_WORK_LIST_TYPE;
			if(queryParams != null){
				String typeParam = queryParams.getFirst(IConstants.WORK_LIST_TYPE);
				if(typeParam.equals(WORKITEMS_TYPE)){
					workItemType = WorkItemType.REGULAR_LOB_WORK_ITEM_TYPE;
				}else if(typeParam.equals(ACCOUNTS_TYPE)){
					workItemType = WorkItemType.ACCOUNT_WORK_ITEM_TYPE;
				}else{
					throw new APException("Invalid argument for " + IConstants.WORK_LIST_TYPE);
				}
				if(!StringUtilities.isEmpty(queryParams.getFirst("listId"))){
					worklistType = queryParams.getFirst("listId");
				}
			}
			//TODO - this won't work, need to call the config/products api here
			Transaction[] allTransactions = {};//TransactionDefinitionManager.getAllTransactions();

			for (int ix = 0; ix < allTransactions.length; ix++) {
				Transaction tran = allTransactions[ix];
				// avoid displaying account in the new work creation list
				if (!tran.getWorkItemType().equals(workItemType)) {
					continue;
				} else if (tran.supportsEmptyInitialWorkItems()) {
					String lob = tran.getLob();
					if (roles.checkPermission(principal, "access" + lob)) {
						String url = "";
						if(worklistType.equals(IWorkListConstants.ACCOUNT_DETAILS_WORK_LIST_TYPE)){
							//ICSRFGuard guard = CSRFHelper.getCSRFGuard();
							url = URLBuilder.buildURL(IWebsharedConstants.WORK_ITEM_ACTION_URI,
					    			URLBuilder.parm(WorkItemAction.ACTION, COPY_ACTION),
					    			URLBuilder.parm("targetTransaction", tran.getId()),
					    			URLBuilder.parm("sourceWorkItemType", WorkItemType.ACCOUNT_WORK_ITEM_TYPE.intValue()),
					    			URLBuilder.parm(IWorkListConstants.WORK_LIST_TYPE, IWorkListConstants.ACCOUNT_DETAILS_WORK_LIST_TYPE),
					    			URLBuilder.parm(IWebsharedConstants.WORKITEMID, queryParams.getFirst(IWebsharedConstants.WORKITEMID))
					    			//URLBuilder.parm(guard.getCSRFTokenTagName(),guard.getCurrentCSRFToken())
					    			);
						}else{
							url = URLBuilder.buildFrontServletURL(-1,
								tran.getFirstPageName(), "true",
								tran.getId(), URLBuilder.DISPLAY_METHOD,
								false);
						}
						listCodes.add(new Code(url, tran.getTitle()));
					}
				}
			}			
			
			return codeList;
		}

		/** 
		 * {@inheritDoc}
		 */ 
		@Override
		public void addLinks(List<AtomLink> atomLinks, LookupRealm realm) throws APException{
			List<String> queryParams = new ArrayList<>();
			queryParams.add(IConstants.WORK_LIST_TYPE);
			atomLinks.add(CodeList.createAtomLink(true, realm, NAME,queryParams));
		}
	}	
	/**
	 * Creates an instance.
	 */
	public BasicLookupProvider() {
	}

	/** 
	 * {@inheritDoc}
	 */

	@Override
	public CodeList get(LookupRealm realm, String name, MultivaluedMap<String, String> queryParams, ISecurityProfile securityProfile) throws APException {
		ILookupProvider lookupProvider = BASIC_REALM_PROVIDERS.get(name);
		if (lookupProvider != null){
			LOGGER.info(String.format("Retrieving code list for realm '%s' and name '%s'.", realm, name));
			return lookupProvider.get(realm, name, queryParams, securityProfile);
		} else {
			throw new APException(String.format("Unable to create lookup provider for realm: '%s' and name: '%s'", realm, name));
		}
	}

	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public void addLinks(List<AtomLink> atomLinks, LookupRealm realm) throws APException{
		if (LookupRealm.BASIC.equals(realm)){
			for (ILookupProvider lookupProvider : BASIC_REALM_PROVIDERS.values()){
				lookupProvider.addLinks(atomLinks, realm);
			}
		} else {
			throw new APException(String.format("Unrecognized realm value: '%s'. Unable to render lookup atom links for this realm.", realm));
		}
	}

}
