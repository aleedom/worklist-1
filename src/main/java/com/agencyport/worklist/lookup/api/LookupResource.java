/*
 * Created on Feb 23, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.lookup.api;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.agencyport.api.BaseResource;
import com.agencyport.api.outputter.PojoContainerOutputter;
import com.agencyport.api.outputter.pojo.PojoCollectionContainer;
import com.agencyport.api.outputter.pojo.PojoContainer;
import com.agencyport.api.outputter.pojo.PojoObjectContainer;
import com.agencyport.rest.AtomLink;
import com.agencyport.rest.IConstants;
import com.agencyport.rest.IResourceConstants;
import com.agencyport.rest.Status;
import com.agencyport.security.profile.ISecurityProfile;
import com.agencyport.utils.PerfObject;
import com.agencyport.worklist.lookup.ILookupProvider;
import com.agencyport.worklist.lookup.LookupProviderFactory;
import com.agencyport.worklist.lookup.LookupRealm;
import com.agencyport.worklist.lookup.pojo.CodeList;

/**
 * The LookupResource class provides the REST API for lookup code list values.
 */
@Path(IResourceConstants.LOOKUP_API_PATH)
public class LookupResource extends BaseResource {

	/**
	 * Constructs an instance.
	 */
	public LookupResource() {
	}
	
	/**
	 * Gets a code list for a given lookup realm and name.
	 * @param realm is the realm of the lookup.
	 * @param name is the name of the lookup.
	 * @param urlInfo is the URL information from which query parameters are sourced.
	 * @return a code list for a given lookup realm and name.
	 */
	@Path(IConstants.LOOKUP_CODELIST_PATH)
	@GET
	public Response getLookupList(@PathParam(IConstants.LOOKUP_REALM) String realm, 
				@PathParam(IConstants.LOOKUP_NAME) String name, @Context UriInfo urlInfo) {
		MultivaluedMap<String, String> queryParams = urlInfo.getQueryParameters();
		return getLookupList(getAccepts(), realm, name, queryParams);
	}
	
	/**
	 * Gets a code list for a given lookup realm and name.
	 * @param acceptMediaType is the media type to render.
	 * @param realm is the realm of the lookup.
	 * @param name is the name of the lookup.
	 * @param queryParams is the instance of MultivaluesMap that consists of query params and their values 
	 * @return a code list for a given lookup realm and name.
	 */
	public Response getLookupList(MediaType acceptMediaType, String realm, String name, MultivaluedMap<String, String> queryParams){
		final String functionName = "getLookupList";
		PerfObject perfObject = startPerfObject(functionName);
		try {
			ILookupProvider lookupProvider = LookupProviderFactory.get().getLookupProvider(LookupRealm.valueOf(realm));
			ISecurityProfile securityProfile = getSecurityProfile();
			CodeList codeList = lookupProvider.get(LookupRealm.valueOf(realm), name, queryParams, securityProfile);
			PojoContainer pojoContainer = new PojoObjectContainer("codeList", codeList);
			return generateResponse(acceptMediaType, new PojoContainerOutputter(acceptMediaType, pojoContainer));
		} catch (Exception exception){
			return generateExceptionResponse(acceptMediaType, exception, Status.LOOKUP_ACCESS_ISSUE, functionName);
		} finally {
			perfObject.stop();
		}
	}
	
	/**
	 * Returns all of the atom links for a given realm.
	 * @param realm is the realm of the lookup.
	 * @param urlInfo is the URL information from which query parameters are sourced.
	 * @return all of the atom links for a given realm.
	 */
	@Path(IConstants.LOOKUP_REALM_PATH)
	@GET
	public Response getLookupLinks(@PathParam(IConstants.LOOKUP_REALM) String realm, @Context UriInfo urlInfo){
		MultivaluedMap<String, String> queryParams = urlInfo.getQueryParameters();
		return getLookupLinks(getAccepts(), realm, queryParams);
	}
	
	/**
	 * Returns all of the atom links for a given realm.
	 * @param acceptMediaType is the media type to render.
	 * @param realm is the realm of the lookup.
	 * @param queryParams are the query parameters.
	 * @return all of the atom links for a given realm.
	 */
	public Response getLookupLinks(MediaType acceptMediaType, String realm, MultivaluedMap<String, String> queryParams){
		final String functionName = "getLookupLinks";
		PerfObject perfObject = startPerfObject(functionName);
		try {
			ILookupProvider lookupProvider = LookupProviderFactory.get().getLookupProvider(LookupRealm.valueOf(realm));
			List<AtomLink> lookupLinksForRealm = new ArrayList<>();
			lookupProvider.addLinks(lookupLinksForRealm, LookupRealm.valueOf(realm));
			PojoContainer pojoContainer = new PojoCollectionContainer("lookupLinks", AtomLink.class, lookupLinksForRealm);
			return generateResponse(acceptMediaType, new PojoContainerOutputter(acceptMediaType, pojoContainer));
		} catch (Exception exception){
			return generateExceptionResponse(acceptMediaType, exception, Status.LOOKUP_ACCESS_ISSUE, functionName);
		} finally {
			perfObject.stop();
		}
	}

}
