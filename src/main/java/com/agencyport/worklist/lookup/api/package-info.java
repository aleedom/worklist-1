/**
 * REST API supporting lookup services such as code lookups for work item statuses, transaction types, etc.
 * @since 5.1   
 */
package com.agencyport.worklist.lookup.api;