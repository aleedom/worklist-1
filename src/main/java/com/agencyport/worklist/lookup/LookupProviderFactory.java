/*
 * Created on Feb 23, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.lookup;

import com.agencyport.factory.GenericFactory;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.shared.APException;
import com.agencyport.worklist.searchindex.SearchIndexFactory;

/**
 * The LookupProviderFactory class provides a factory mechanism for dispensing lookup provider instances. Applications can register their
 * own factory class extension of this class under the application property <b>application.lookup_provider_factory_classname</b>.
 * @since 5.1
 */
public class LookupProviderFactory {
	/**
	 * The <code>SINGLETON</code> is the single instance of this class.
	 */
	private static final LookupProviderFactory SINGLETON = createLookupProviderFactory();
	
	/**
	 * Creates an instance of the search index factory.
	 * @return an instance of the search index factory.
	 */
	private static LookupProviderFactory createLookupProviderFactory(){
		try {
			return GenericFactory.create("application.lookup_provider_factory_classname", LookupProviderFactory.class.getName());
		} catch (APException apException){
			ExceptionLogger.log(apException, SearchIndexFactory.class, "createLookupProviderFactory");
			return new LookupProviderFactory();
		}
	}

	/**
	 * Returns the single instance of this class.
	 * @return the single instance of this class.
	 */
	public static LookupProviderFactory get(){
		return SINGLETON;
	}

	/**
	 * Creates an instance.
	 */
	public LookupProviderFactory() {
	}
	
	/**
	 * Gets a lookup provider for a given realm.
	 * @param realm is used to narrow the focus to a specific lookup provider. Think of realm as category or class.
	 * @return a lookup provider for a given realm
	 * @throws APException propagated from {@link GenericFactory#create(String, String, Object...)}
	 */
	public ILookupProvider getLookupProvider(LookupRealm realm) throws APException {
		String property = String.format("application.%s.lookup_provider_classname", realm.name());
		return GenericFactory.create(property, BasicLookupProvider.class.getName());
	}

}
