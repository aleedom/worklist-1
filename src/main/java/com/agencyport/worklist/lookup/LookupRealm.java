/*
 * Created on Feb 23, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.lookup;

import com.agencyport.type.ExtensibleEnum;

/**
 * The LookupRealm class models the idea of a lookup realm. A look up realm is the class or category of lookup services. Currently
 * the product only supports the "basic" realm. Custom realms can be added by application project teams. A good best practice is to
 * create your own realm for any custom lookups.
 * @since 5.1 
 */
public class LookupRealm extends ExtensibleEnum<LookupRealm>{
	/**
	 * The <code>BASIC</code> is the basic realm. The basic realm defines lookups that the product
	 * can support natively.
	 */
	public static final LookupRealm BASIC = new LookupRealm("basic");

	/**
	 * Constructs an instance.
	 * @param name is the name value of this policy type. 
	 */
	public LookupRealm(String name) {
		super(name, LookupRealm.class);
	}
	/**
	 * Returns the enumeration instance for the given name.
	 * @param name is the value of the enumeration.
	 * @return the enumeration instance for the given name.
	 */
	public static LookupRealm valueOf(String name){
		return valueOf(name, LookupRealm.class);
	}

}
