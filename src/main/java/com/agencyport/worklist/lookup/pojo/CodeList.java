/*
 * Created on Feb 23, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.lookup.pojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

import com.agencyport.api.URIBuilder;
import com.agencyport.worklist.lookup.api.LookupResource;
import com.agencyport.api.pojo.ItemLink;
import com.agencyport.worklist.lookup.LookupRealm;
import com.agencyport.rest.AtomLink;
import com.agencyport.rest.IConstants;
import com.agencyport.utils.EqualityChecker;
import com.agencyport.utils.HashCodeCalculator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Codes class is a container of lookup codes.
 * @since 5.1
 */
@XmlRootElement
public class CodeList extends ItemLink {
	/**
	 * The <code>codes</code> is the list of codes.
	 */
	private List<Code> codes = new ArrayList<>();
	
	/**
	 * Constructs an instance.
	 */
	public CodeList() {
	}

	/**
	 * Constructs an instance.
	 * @param name is the name of the code list.
	 * @param link
	 */
	public CodeList(String name, AtomLink link) {
		super(name, name + " Code list", link);
	}
	
	/**
	 * Creates an atom link.
	 * @param generateRelationship signifies whether or not to generate a relationship attribute on the atom link from the realm and name.
	 * @param realm is the realm of the lookup code list.
	 * @param name is the name of the lookup code list.
	 * @return an atom link.
	 */
	public static AtomLink createAtomLink(boolean generateRelationship, LookupRealm realm, String name){
		Map<String, String> pathParameters = new HashMap<>();
		pathParameters.put(IConstants.LOOKUP_REALM, realm.name());
		pathParameters.put(IConstants.LOOKUP_NAME, name);
		return createAtomLink(generateRelationship, realm, name, pathParameters, null);
	}

	/**
	 * Creates an atom link.
	 * @param generateRelationship signifies whether or not to generate a relationship attribute on the atom link from the realm and name.
	 * @param realm is the realm of the lookup code list.
	 * @param name is the name of the lookup code list.
	 * @param queryParameters is the map of query parameters
	 * @return an atom link.
	 */
	public static AtomLink createAtomLink(boolean generateRelationship, LookupRealm realm, String name, List<String> queryParameters){
		Map<String, String> pathParameters = new HashMap<>();
		pathParameters.put(IConstants.LOOKUP_REALM, realm.name());
		pathParameters.put(IConstants.LOOKUP_NAME, name);
		return createAtomLink(generateRelationship, realm, name, pathParameters, queryParameters);
	}
	
	/**
	 * Creates an atom link.
	 * @param generateRelationship signifies whether or not to generate a relationship attribute on the atom link from the realm and name.
	 * @param realm is the realm of the lookup code list.
	 * @param name is the name of the lookup code list.
	 * @param pathParameters is the map of path parameters.
	 * @param queryParameters is the list of query parameters. 
	 * @return an atom link.
	 */
	public static AtomLink createAtomLink(boolean generateRelationship, LookupRealm realm, String name, Map<String, String> pathParameters, List<String> queryParameters){
		if (generateRelationship){
			return new AtomLink("lookups." + realm.name() + "." + name, URIBuilder.createURI(LookupResource.class, IConstants.LOOKUP_CODELIST_PATH, pathParameters, null, false),queryParameters);
		} else {
			return new AtomLink(URIBuilder.createURI(LookupResource.class, IConstants.LOOKUP_CODELIST_PATH, pathParameters),queryParameters);
		}
	}
	
	/**
	 * Creates an instance.
	 * @param realm is the lookup realm.
	 * @param name is the lookup name.
	 * @return a Codes instance.
	 */
	public static CodeList create(LookupRealm realm, String name){
		return new CodeList(name, createAtomLink(false, realm, name));
	}
	/**
	 * @return the code list.
	 */
	@XmlElementWrapper(name="codes")
	@XmlElements(@XmlElement(name="code", type=Code.class))
	@JsonProperty("codes")
	public List<Code> getCodes() {
		return codes;
	}
	
	/**
	 * Sets the codes list.
	 * @param codes list.
	 */
	public void setCodes(List<Code> codes){
		this.codes = codes;
	}
	
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public int hashCode(){
		return super.hashCode() + HashCodeCalculator.calculate(this.codes);
	}
	
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public boolean equals(Object other){
		if (other instanceof CodeList){
			CodeList codeList = (CodeList) other;
			return super.equals(other) && EqualityChecker.areEqual(this.codes, codeList.codes);
		} else {
			return false;
		}
	}

	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public String toString() {
		return super.toString() + " [codes=" + codes + "]";
	}

}
