/*
 * Created on Jan 20, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.verfication;

import com.agencyport.api.BaseResource;
import com.agencyport.api.verification.Verifier;
import com.agencyport.resource.ResourceException;
import com.agencyport.rest.RestValidationException;
import com.agencyport.security.exception.SecurityException;
import com.agencyport.security.profile.ISecurityProfile;
import com.agencyport.worklist.view.CompiledWorkListView;
import com.agencyport.worklist.view.WorkListViewResourceProvider;

/**
 * The WorkListViewNameVerifier class validates the view name.
 * @since 5.1
 */
public class WorkListViewNameVerifier extends Verifier {
	/**
	 * The <code>viewName</code> is the view name.
	 */
	private final String viewName;
	/**
	 * The <code>workListViewResource</code> is the compiled resource related to the incoming view. If this is null after
	 * construction then the name on the incoming view was corrupted.
	 */
	protected final CompiledWorkListView workListViewResource;

	/**
	 * Constructs an instance.
	 * @param viewName is the view name.
	 * @param viewResource is the CompiledWorkListView from the configuration service
	 * @throws ResourceException propagated from  {@link WorkListViewResourceProvider#getWorkListViewResource(String)}
	 */
	public WorkListViewNameVerifier(String viewName, CompiledWorkListView viewResource) throws ResourceException {
		this.viewName = viewName;
		this.workListViewResource = viewResource;
	}

	/** 
	 * {@inheritDoc}
	 */
	@Override
	public void validateParameters(BaseResource baseResource) throws RestValidationException {
		if (workListViewResource == null){
			throw new RestValidationException(String.format("Unrecognized view name '%s'", viewName));
		}

	}

	/** 
	 * {@inheritDoc}
	 */
	@Override
	public void checkAuthorization(BaseResource baseResource, ISecurityProfile securityProfile) throws SecurityException {
		// no authorization
	}

}
