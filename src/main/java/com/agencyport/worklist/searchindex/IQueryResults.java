/*
 * Created on Feb 2, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.searchindex;

import java.util.Map;

/**
 * The IQueryResults interface provides a generate interface on a set of query results served
 * up from various types of search indices.
 * @since 5.1
 */
public interface IQueryResults {
	/**
	 * The IQueryResultRecord interface defines a query result record or row.
	 */
	interface IQueryResultRecord extends Iterable<Map.Entry<String, Object>>{
		// iteration is the only required interaction.
	}
	
	/**
	 * Returns a query result instance for the given index.
	 * @param index is the query result index. These are zero based.
	 * @return a query result instance for the given index.
	 */
	IQueryResultRecord getQueryResult(int index);
	/**
	 * Returns the total number of hits in the search index for this query. This
	 * can be more than what is returned by {@link #getNumberOfResults()}
	 * @return  the total number of hits in the search index for this query.
	 */
	long getNumberOfHits();
	
	/**
	 * Returns the starting record number represented in this query. This is typically
	 * the same value echoed back as what is passed as {@link com.agencyport.api.worklist.pojo.ListView#setStartRowNumber(int)}
	 * @return the starting record number represented in this query.
	 */
	long getStartingRecordNumber();
	
	/**
	 * Returns the number of result records in this query result instance.
	 * @return the number of result records in this query result instance.
	 */
	int getNumberOfResults();
	
}
