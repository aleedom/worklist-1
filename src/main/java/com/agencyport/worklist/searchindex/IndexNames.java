/*
 * Created on Jan 28, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.searchindex;


/**
 * The SolrIndex class contains the names of the SOLR indices.
 * @since 5.1
 */
public final class IndexNames {
	/**
	 * Prevent construction.
	 */
	private IndexNames(){
		
	}
	/**
	 * The <code>ACCOUNT_SOLR_INDEX_NAME</code> is the name of the account's
	 * SOLR index.
	 */
	public static final String ACCOUNT_SOLR_INDEX_NAME = "account";
	/**
	 * The <code>WORKITEM_SOLR_INDEX_NAME</code> is the name of the work item's
	 * SOLR index.
	 */
	public static final String WORKITEM_SOLR_INDEX_NAME = "worklist";
	
}
