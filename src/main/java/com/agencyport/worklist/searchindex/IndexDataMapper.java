/*
 * Created on Mar 10, 2016 by ldeane AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.searchindex;

import com.agencyport.resource.ResourceException;
import com.agencyport.shared.APException;

/**
 * The IndexDataMapper class maps data values served up by the data reader into an index specific input document.
 * @since 5.1
 */
public abstract class IndexDataMapper {
	/**
	 * The <code>indexMap</code> is the index map.
	 */
	private final ISearchIndexMapping indexMap;
	
	/**
	 * The <code>reader</code> is the data reader.
	 */
	private final IDataReader reader;
	/**
	 * Constructs an instance.
	 * @param index is the name of the index.
	 * @param reader is the data reader.
	 * @throws ResourceException propagated from  {@link CompiledIndexMapping#get(String)}
	 */
	public IndexDataMapper(String index, IDataReader reader) throws ResourceException{
		indexMap = CompiledIndexMapping.get(index);
		this.reader = reader;
	}
	
	/**
	 * Creates a input document replete with all of the field values from the data source.
	 * @param dataSource is the data source. These are specific to the data reader type being used.
	 * @return an index input document replete with all of the field values from the data source. The type of the
	 * return object is governed by the back end search index specifics.
	 * @throws APException if the method ran into an unrecoverable issue.
	 */
	public abstract Object createIndexDocument(Object dataSource) throws APException;
	

	/**
	 * @return the indexMap
	 */
	protected ISearchIndexMapping getIndexMap() {
		return indexMap;
	}

	/**
	 * @return the reader
	 */
	protected IDataReader getReader() {
		return reader;
	}
}
