/*
 * Created on Jan 9, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.searchindex;

import javax.ws.rs.core.MediaType;

import com.agencyport.worklist.pojo.WorkListView;
import com.agencyport.worklist.view.CompiledWorkListView;
import com.agencyport.security.profile.ISecurityProfile;
import com.agencyport.shared.APException;
import com.agencyport.workitem.model.WorkItemType;

/**
 * The IWorkListSearchProvider interface establishes the API contract for SOLR based worklist searching. WorkListSearchProvider instances
 * are dispensed out with a new instance per request and never shared across threads. Therefore it is okay to store request level state on these
 * instances.
 * @since 5.1
 */
public interface IWorkListSearchProvider {
	/**
	 * Initializes the state on the worklist search provider. This is called before and in preparation of the {@link #search()} method.
	 * @param returnMediaType designates the media type of the results object.
	 * @param workItemType is the work item type for the selected view.
	 * @param workListView is the view which contains the various filters, sort information and query information.
	 * @param securityProfile is the security profile for the current user.
	 * @throws APException if the worklist resource artifact could not be accesses.
	 */
	void initialize(MediaType returnMediaType, WorkItemType workItemType, WorkListView workListView, ISecurityProfile securityProfile) throws APException;
	/**
	 * Searches an index based on the filters, sort information and query information initialized from previously calling the {@link #initialize(MediaType, WorkItemType, WorkListView, ISecurityProfile)}
	 * mtehod.
	 * @return the results object.
	 * @throws APException if the index could not be accessed or if the search encountered an error.
	 */
	IQueryResults search() throws APException;
	
	/**
	 * Returns the CompiledWorkListView as obtained from the initialize method
	 * @return the work list view artifact.
	 */
	CompiledWorkListView getWorkListViewResource();
}
