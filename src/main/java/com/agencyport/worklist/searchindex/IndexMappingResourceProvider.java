/*
 * Created on Jan 15, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.searchindex;

import org.jdom2.Document;
import org.jdom2.Element;

import com.agencyport.resource.Resource;
import com.agencyport.resource.ResourceEntityKey;
import com.agencyport.resource.ResourceException;
import com.agencyport.resource.ResourceType;
import com.agencyport.resource.SimpleXMLResourceProvider;
import com.agencyport.resource.XMLCompiledResourceEntity;

/**
 * The IndexMappingResourceProvider class provides access to index mapping resources.
 * @since 5.1
 */
public class IndexMappingResourceProvider extends SimpleXMLResourceProvider {

	/**
	 * Constructs an instance.
	 */
	public IndexMappingResourceProvider() {
		super(ResourceType.INDEX_MAPPING_DEFINITION);
	}
	
	/** 
	 * {@inheritDoc}
	 */
	@Override
	protected String readKey(Document document){
        Element rootElement = document.getRootElement();
        return rootElement.getAttributeValue("index");
	}

	/**
	 * Fetches the compiled index mapping for the given index name.
	 * @param index is the index name.
	 * @return the compiled index mapping for the given index name.
	 * @throws ResourceException if the compiled item could not be found. 
	 */
	public ISearchIndexMapping getIndexMappingResource(String index) throws ResourceException {
		ResourceEntityKey key = ResourceEntityKey.create(index);
		return (ISearchIndexMapping) this.getCompiledEntity(key);
	}

	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	protected XMLCompiledResourceEntity createCompileResourceEntity(Resource resource, Document resourceContent) throws ResourceException {
		return new CompiledIndexMapping(resource, resourceContent);
	}

}
