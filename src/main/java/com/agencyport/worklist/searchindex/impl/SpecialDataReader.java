/*
 * Created on Jan 28, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.searchindex.impl;

import com.agencyport.account.model.IAccount.AccountType;
import com.agencyport.api.worklist.pojo.Field;
import com.agencyport.shared.APException;
import com.agencyport.worklist.searchindex.IDataReader;
import com.agencyport.worklist.searchindex.ISearchIndexMapping;

/**
 * The SpecialDataReader class aggregates a field name with an account type value and how their data values should
 * be served up. Special processing is triggered when the incoming account type and the field name match.
 * @since 5.1
 */
public final class SpecialDataReader {
	/**
	 * The <code>accountType</code> is the account type.
	 */
	private final AccountType accountType;
	/**
	 * The <code>fieldName</code> is the field name.
	 */
	private final String fieldName;
	
	/**
	 * The <code>basedOn</code> contains 0 or more field names. These names should
	 * match the {@link Field#getName()} property. 
	 */
	private final String[] basedOn;
	/**
	 * Constructs an instance.
	 * @param accountType see {@link #accountType}
	 * @param fieldName see {@link #fieldName}
	 * @param basedOn see {@link #basedOn}
	 */
	public SpecialDataReader(AccountType accountType, String fieldName, String... basedOn){
		this.accountType = accountType;
		this.fieldName = fieldName;
		this.basedOn = basedOn;
	}
	
	/**
	 * Retrieves the value for the specified field.
	 * @param fieldId is the field id. 
	 * @param parentReader is the governing data reader.
	 * @param indexMap is the index map.
	 * @return the value for the specified field.
	 * @throws APException propagated from {@link IDataReader#fetchValue(Field)}
	 */
	private Object getValue(String fieldId, IDataReader parentReader, ISearchIndexMapping indexMap) throws APException {
		Field field = indexMap.getField(fieldId);
		return parentReader.fetchValue(field);
	}
	
	/**
	 * Performs the special handling necessary for the field. 
	 * @param parentReader is the parent data reader.
	 * @param solrIndexMap is a reference to the SOLR index map instance.
	 * @return the value resulting from the special handling performed.
	 * @throws APException if the wrong kind of data source is passed or propagated
	 * from a SQL exception from accessing the result set.
	 */
	public Object read(IDataReader parentReader, ISearchIndexMapping solrIndexMap) throws APException {
		if (basedOn.length == 0){
			return null;
		} else if (basedOn.length == 1){
			return getValue(basedOn[0], parentReader, solrIndexMap);
		} else {
			// Assume a concatenation
			StringBuilder value = new StringBuilder();
			int count = 0;
			for (String otherFieldName : this.basedOn){
				if (count > 0){
					value.append(' ');
				}
				Object atomicValue = getValue(otherFieldName, parentReader, solrIndexMap);
				if (atomicValue != null){
					value.append(atomicValue);
					count++;
				}
			}
			return value;
		}
	}
	/**
	 * @return the accountType
	 */
	public AccountType getAccountType() {
		return accountType;
	}
	/**
	 * @return the fieldName
	 */
	public String getFieldName() {
		return fieldName;
	}
}