/*
 * Created on Jan 28, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.searchindex.impl;

import com.agencyport.account.model.IAccount.AccountType;
import com.agencyport.api.worklist.pojo.Field;
import com.agencyport.resource.ResourceException;
import com.agencyport.shared.APException;
import com.agencyport.worklist.searchindex.CompiledIndexMapping;
import com.agencyport.worklist.searchindex.IDataReader;
import com.agencyport.worklist.searchindex.ISearchIndexMapping;
import com.agencyport.worklist.searchindex.IndexNames;

/**
 * The AccountDataReader class provides some special processing when reading account instances mostly
 * in the area of the account's name. 
 * @since 5.1 
 */
public class AccountDataReader implements IDataReader {
	/**
	 * The <code>indexMap</code> is the SOLR index map.
	 */
	private final ISearchIndexMapping indexMap;
	/**
	 * The <code>accountType</code> reflects the account type currently being processed.
	 */
	private AccountType accountType;
	/**
	 * Constructs an instance.
	 */
	public AccountDataReader() {
		try {
			indexMap = CompiledIndexMapping.get(IndexNames.ACCOUNT_SOLR_INDEX_NAME);
		} catch (ResourceException e) {
			throw new IllegalStateException(e);
		}
	}
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public void setDataSource(Object dataSource) throws APException{
		//super.setDataSource(dataSource);
		//IAccount account = getDataSourceAsAccount();
		//accountType = account.getAccountType();
	}
	
	/**
	 * Returns the data source downcast as an IAccount.
	 * @return the data source downcast as an IAccount.
	 */
//	private IAccount getDataSourceAsAccount(){
//		//IWorkItem dataSource = this.getDataSource();
////		if (dataSource instanceof IAccount){
////			return (IAccount) dataSource;
////		} else {
////			throw new IllegalStateException(String.format("Expected account instance data source with class name of '%s' or derivative. Instead encountered data source with class name of '%s'", IAccount.class.getName(), dataSource.getClass().getName()));
////		}
//	}

	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public Object read(Field field) throws APException {
		SpecialDataReader spcField = SpecialDataReaderStore.getSpecialDataReader(accountType, field);
		if (spcField != null){
			return spcField.read(this, indexMap);
		} else {
			//return super.read(field);
		}
		
		return null;
	}
	@Override
	public Object fetchValue(Field field) throws APException {
		// TODO Auto-generated method stub
		return null;
	}
}
