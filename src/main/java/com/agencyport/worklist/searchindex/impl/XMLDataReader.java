/*
 * Created on Mar 14, 2016 by ldeane AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.searchindex.impl;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;

import com.agencyport.api.pojo.DataType;
import com.agencyport.api.worklist.pojo.Field;
import com.agencyport.rest.Serializer;
import com.agencyport.service.ServiceException;
import com.agencyport.service.xml.DOMUtils;
import com.agencyport.shared.APException;
import com.agencyport.utils.APDate;
import com.agencyport.utils.AppProperties;
import com.agencyport.utils.text.StringUtilities;
import com.agencyport.worklist.pojo.Property;
import com.agencyport.worklist.pojo.WorkItemContent;
import com.agencyport.worklist.searchindex.IDataReader;
import com.agencyport.worklist.solr.manager.SolrManager;

/**
 * 
 * The XMLDataReader class provides the mechanism for for reading values from a work item or account objects 
 * for the purposes of updating a search index. 
 * 
 * This reader expects XML as the dataSource - specifically a jdom2 Document that it can parse through the use of xpath.
 * Expected format is as follows:
 * 
 * <workItem>
 * 		<property>
 * 			<name>ENTITY_NAME</name>
 * 			<value>Best Value Construction</value>
 * 			<type>STRING</type>
 * 		</property>
 * 			...
 * 		<property>
 * 			<name>WORKITEM_STATUS</name>
 * 			<value>15</value>
 * 			<type>STATUS</type>
 * 		</property>
 * </workItem>
 * 
 */
public class XMLDataReader implements IDataReader {
	
	/**
	 * The <code>dataSource</code> is the data source for this reader.
	 */
	private WorkItemContent dataSource;

	@Override
	public void setDataSource(Object dataSource) throws APException {
		try {
			if (dataSource instanceof Document){			
				this.dataSource = Serializer.get(MediaType.APPLICATION_XML_TYPE).deserialize(DOMUtils.asString((Document) dataSource), WorkItemContent.obtainClassInfo());
			} else {
				throw new APException(String.format("Unacceptable data source type '%s' passed. Expected type '%s'", dataSource != null ? dataSource.getClass() : "null", Document.class.getName()));
			}
		} catch (IOException e) {
			throw new APException(e.getMessage(), e.getCause());
		}
	}

	@Override
	public Object read(Field field) throws APException {
		return fetchValue(field);
	}

	@Override
	public Object fetchValue(Field field) throws APException {
		try {
			Property value = getPropertyValue(field.getWorkItemPropertyName());
			DataType dataType = field.getType(); 
			if (value != null && !StringUtilities.isEmptyOnTrim(value.getValue())){		
				switch (dataType){
				case DATE:
					APDate date = new APDate(value.getValue());
					return SolrManager.formatDate(date.getUtilDate());
				case TIME:
					Timestamp time = (Timestamp) new APDate(value.getValue(), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S")).getTimestamp();
					return SolrManager.formatDate(time);
				case STATUS:
					return value.getValue();
				case STRING:
					/*
					 * TODO: Field encryption bug logged: DP-736
					 */
					//return field.getEncrypted() ? Field.encryptValue(value.getValue()) : value.getValue();
					return value.getValue();
				default:
					return interpretValueForField(value);
				}
			} 
			return null;
		} catch (ParseException | XPathExpressionException | ServiceException | IOException e) {
			throw new APException(e.getMessage(), e.getCause());
		}
	}
	
	/**
	 * Obtains the Work Item Property given the field name
	 * @param propertyName is the name of the property we desire
	 * @return the Work Item Property for the provided field name
	 * @throws XPathExpressionException 
	 * @throws ServiceException 
	 * @throws IOException 
	 */
	protected Property getPropertyValue(String propertyName) throws XPathExpressionException, ServiceException, IOException {
		if (dataSource == null) {
			return null;
		}
		
		List<Property> properties = dataSource.getProperties();
		for (Property property : properties) {
			if (property.getName().equalsIgnoreCase(propertyName)) {
				return property;
			}
		}
		
		return null;
	}
	
	/**
	 * Interprets a value for a field.
	 * @param value is the value to interpret.
	 * @return a the interpreted value.
	 */
	private Object interpretValueForField(Property value) {
		if (value.getType().equalsIgnoreCase("Boolean")){
			// We want a 1 or 0 not a true / false
			Boolean boolValue = AppProperties.determineTrueFalseValue(value.getValue(), false);
			return boolValue ? 1 : 0;
		} else {
			return value.getValue();
		}
	}

}
