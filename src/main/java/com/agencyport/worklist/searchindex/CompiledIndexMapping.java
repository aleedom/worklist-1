/*
 * Created on Jan 2, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.searchindex;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.jdom2.Document;
import org.jdom2.Element;

import com.agencyport.api.pojo.DataType;
import com.agencyport.api.pojo.Item;
import com.agencyport.api.worklist.pojo.Field;
import com.agencyport.domXML.APDataCollection;
import com.agencyport.id.Id;
import com.agencyport.locale.Locales;
import com.agencyport.resource.Resource;
import com.agencyport.resource.ResourceException;
import com.agencyport.resource.ResourceType;
import com.agencyport.resource.RestResource;
import com.agencyport.resource.RestResponse;
import com.agencyport.resource.XMLCompiledResourceEntity;
import com.agencyport.utils.AppProperties;
import com.agencyport.utils.ArrayHelper;
import com.agencyport.utils.EqualityChecker;
import com.agencyport.utils.HashCodeCalculator;
import com.agencyport.utils.PerfObject;
import com.agencyport.utils.PerfObjectCollector;
import com.agencyport.utils.text.StringUtilities;
import com.agencyport.worklist.view.IConstants;
import com.agencyport.worklist.view.LocalizedContent;

/**
 * The CompiledSolrIndexMapping class is the compilation of a index mapping artifact.
 * @since 5.1
 */
public class CompiledIndexMapping extends XMLCompiledResourceEntity implements ISearchIndexMapping{
	
	/**
	 * The <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = 2114687973474771082L;
	/**
	 * The <code>indexMappingData</code> is the index mapping data collection.
	 */
	private final APDataCollection indexMappingData;
	
	/**
	 * The FieldCacheKey class aggregates the key values used for the field instance map.
	 */
	private static final class FieldCacheKey {
		/**
		 * The <code>name</code> can be the id, name, index field name or work item property name.
		 */
		private final String name;
		/**
		 * The <code>localeId</code> is the locale id.
		 */
		private final Id localeId;
		/**
		 * Creates an instance. 
		 * @param name see {@link #name}
		 * @param localeId see {@link #localeId}
		 */
		private FieldCacheKey(String name, Id localeId){
			this.name = name;
			this.localeId = localeId;
		}
		/** 
		 * {@inheritDoc}
		 */ 
		
		@Override
		public String toString() {
			return "FieldCacheKey [localeId=" + localeId
					+ ", name=" + name + "]";
		}
		/** 
		 * {@inheritDoc}
		 */ 
		
		@Override
		public int hashCode() {
			return HashCodeCalculator.calculate(localeId, name);
		}
		/** 
		 * {@inheritDoc}
		 */ 
		
		@Override
		public boolean equals(Object obj) {
			if (obj instanceof FieldCacheKey){
				if (this == obj) {
					return true;
				}
				FieldCacheKey other = (FieldCacheKey) obj;
				return EqualityChecker.areEqual(name, other.name,
						localeId, other.localeId);
			} else {
				return false;
			}
		}

	}
	
	/**
	 * The <code>fieldInstanceMap</code> is a cache of all the fields for all registered locales.
	 */
	private transient Map<FieldCacheKey, Field> fieldInstanceMap;
	
	/**
	 * The <code>uniqueFields</code> is a cache of all the fields with no regard for locale.
	 */
	private transient Collection<Field> uniqueFields;
	
	 /**
     * The <code>INDEXMAPPING_URL</code> is the partial URL for configuration service.
     */
    private static final String INDEXMAPPING_URL = AppProperties.getAppProperties().getStringProperty("indexmapping_resource", null);
	
	/**
	 * Constructs an instance. 
	 * @param resource is the associated resource. 
	 * @param document is the XML contents for this resource.
	 */
	public CompiledIndexMapping(Resource resource, Document document) {
		super(resource, document);
		indexMappingData = new APDataCollection(ResourceType.INDEX_MAPPING_DEFINITION.getSchemaName());
		indexMappingData.setDocument(document);
		fieldInstanceMap = createFieldInstanceMap(indexMappingData);
		uniqueFields = asUniqueFieldSet(fieldInstanceMap);
	}
	
	/**
	 * Creates a map of all of the field instances from the given resource for all registered locales.
	 * @param indexMappingData is the data collection to query.
	 * @return  a map of all of the field instances from the given resource for all registered locales.
	 */
	private static Map<FieldCacheKey, Field> createFieldInstanceMap(APDataCollection indexMappingData){
		Map<FieldCacheKey, Field> fieldInstanceMap = new HashMap<>();
		for (int[] fieldIndex : indexMappingData.createIndexTraversalBasis(IConstants.FIELD_XPATH)){
			int contentElementCount = indexMappingData.getCount(IConstants.FIELD_CONTENT_XPATH, fieldIndex);
			String id = indexMappingData.getAttributeText(IConstants.FIELD_XPATH, fieldIndex, "id");
			String name = indexMappingData.getAttributeText(IConstants.FIELD_XPATH, fieldIndex, IConstants.NAME);
			if (StringUtilities.isEmpty(name)){
				name = id;
			}
			String indexFieldName = indexMappingData.getAttributeText(IConstants.FIELD_XPATH, fieldIndex, "indexFieldName");
			if (StringUtilities.isEmpty(indexFieldName)){
				indexFieldName = name;
			}
			String indexSearchFieldName = indexMappingData.getAttributeText(IConstants.FIELD_XPATH, fieldIndex, "indexSearchFieldName");
			String dataType = indexMappingData.getAttributeText(IConstants.FIELD_XPATH, fieldIndex, IConstants.TYPE);
			String workItemPropertyName = indexMappingData.getAttributeText(IConstants.FIELD_XPATH, fieldIndex, "workItemPropertyName");
			if (StringUtilities.isEmpty(workItemPropertyName)){
				workItemPropertyName = name.toUpperCase();
			}
			String isSearchableValue = indexMappingData.getAttributeText(IConstants.FIELD_XPATH, fieldIndex, "isSearchable");
			boolean isSearchable = StringUtilities.isEmpty(isSearchableValue) ? true : Boolean.valueOf(isSearchableValue);
			for (int contentIndex = 0; contentIndex < contentElementCount; contentIndex++){
				int[] combinedIndex = ArrayHelper.append(fieldIndex, contentIndex);
				LocalizedContent content = LocalizedContent.create(indexMappingData, IConstants.FIELD_CONTENT_XPATH, combinedIndex);
				String title = name;
				String styleClass = null;
				String format = null;
				if (content != null){
					title = content.getTitle();
					styleClass = content.getStyleClass();
					format = content.getFormat();
				}
				Field field = new Field(name, title);
				field.setId(id);
				field.setSearchable(isSearchable);
				field.setFormat(format);
				field.setStyleClass(styleClass);
				field.setIndexFieldName(indexFieldName);
				field.setIndexSearchFieldName(indexSearchFieldName);
				field.setType(DataType.valueOf(dataType));
				field.setWorkItemPropertyName(workItemPropertyName);
				// Make immutable
				field.setImmutable(true); 
				// Update map for all potentially used keys
				updateMap(fieldInstanceMap, field, content.getLocaleId());
			}
		}
		return Collections.unmodifiableMap(fieldInstanceMap);
	}
	
	/**
	 * Updates the map with various key fields drawn from the field instance.
	 * @param fieldInstanceMap is the field instance map.
	 * @param field is the field under consideration.
	 * @param localeId is the locale id.
	 */
	private static void updateMap(Map<FieldCacheKey, Field> fieldInstanceMap, Field field, Id localeId){
		FieldCacheKey fieldCacheKey = new FieldCacheKey(field.getId(), localeId);
		fieldInstanceMap.put(fieldCacheKey, field);
		fieldCacheKey = new FieldCacheKey(field.getId(), localeId);
		fieldInstanceMap.put(fieldCacheKey, field);
		fieldCacheKey = new FieldCacheKey(field.getIndexFieldName(), localeId);
		fieldInstanceMap.put(fieldCacheKey, field);
		fieldCacheKey = new FieldCacheKey(field.getWorkItemPropertyName(), localeId);
		fieldInstanceMap.put(fieldCacheKey, field);
		if (!StringUtilities.isEmpty(field.getIndexSearchFieldName())){
			fieldCacheKey = new FieldCacheKey(field.getIndexSearchFieldName(), localeId);
			fieldInstanceMap.put(fieldCacheKey, field);
		}
		
	}
	
	/**
	 * Creates a locale unaware set of unique fields from the field instance map.
	 * @param fieldInstanceMap is the field instance map.
	 * @return a locale unaware set of unique fields from the field instance map.
	 */
	@SuppressWarnings("unchecked")
	private static Collection<Field> asUniqueFieldSet(Map<FieldCacheKey, Field> fieldInstanceMap){
		Map<String, Field> uniqueFields = (Map<String, Field>) Item.makeMap(fieldInstanceMap.values());
		return Collections.unmodifiableList(new ArrayList<>(uniqueFields.values()));
	}
	
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public Field getField(String id){
		Field field = getField(id, Locales.get().getLocaleId());
		if (field == null){
			field = getField(id, Locales.get().getLocaleId(Locale.getDefault()));
		}
		if (field == null){
			String error = String.format("Referential integrity issue with resource '%s'. Attempting to access field with id or name or index field name or work item property name value of '%s' but no value was found. Please check product artifact for data integrity.", 
					this.getResource().getResourceId(), id);
			throw new IllegalStateException(error);
		}
		return field;
	}
	
	/**
	 * Gets the field instance for a given name and locale id.
	 * @param id is the field id or name.
	 * @param localeId is the locale id.
	 * @return the Field instance for the given locale id. 
	 */
	private Field getField(String id, Id localeId){
		FieldCacheKey key = new FieldCacheKey(id, localeId);
		return fieldInstanceMap.get(key);
	}
	
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public LocalizedContent getLocalizedContent(String fieldId){
		Field field = getField(fieldId);
		return new LocalizedContent(Locales.get().getLocaleId(), field.getTitle(), field.getFormat(), field.getStyleClass()); 
	}
	
	/**
	 * Override the default de-serialization to inflate an instance.
	 * @param is is the input stream from which this instance is being based on.
	 * @throws IOException is propagated from the {@link ObjectInputStream#readObject()} method.
	 * @throws ClassNotFoundException is propagated from the {@link ObjectInputStream#readObject()} method.
	 */
	private void readObject(ObjectInputStream is) throws IOException, ClassNotFoundException {
		is.defaultReadObject();
		fieldInstanceMap = createFieldInstanceMap(indexMappingData);
		uniqueFields = asUniqueFieldSet(fieldInstanceMap);
	}
	
	/**
	 * Fetches the compiled index mapping for the given SOLR index name.
	 * @param index is the index name.
	 * @return the compiled index mapping for the given SOLR index name.
	 * @throws ResourceException if the compiled item could not be found. 
	 */
	public static ISearchIndexMapping get(String index) throws ResourceException {
	    PerfObject perfObject = PerfObjectCollector.startNew(ISearchIndexMapping.class.getName() + ".get()"+index );
	    try {
    		RestResource resource = new RestResource();
    		StringBuffer buffer = new StringBuffer();
    		buffer.append(INDEXMAPPING_URL).append("/").append(index);
    		resource.initialize(buffer.toString());
    		RestResponse response = resource.read();
    		Element searchIndexMapping =  response.getResults().getObject().getChild("indexMapping").detach();
    		CompiledIndexMapping compiledSolrWorkListView = new CompiledIndexMapping(resource, new Document(searchIndexMapping));
    		return compiledSolrWorkListView;
	    }finally {
	        perfObject.stop();
	    }
	}

	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public Iterator<Field> iterator() {
		List<Field> candidates = new ArrayList<>();
		for (Field candidate : uniqueFields){
			if (candidate.isSearchable()){
				candidates.add(candidate);
			}
		}
		return Collections.unmodifiableList(candidates).iterator();
	}

	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public String toString() {
		return super.toString() + ", " + "CompiledIndexMapping [uniqueFields=" + uniqueFields + "]";
	}
	
}
