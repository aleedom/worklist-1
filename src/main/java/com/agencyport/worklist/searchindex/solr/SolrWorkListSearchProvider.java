/*
 * Created on Jan 9, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.searchindex.solr;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.core.MediaType;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.util.ClientUtils;

import com.agencyport.account.AccountManagementFeature;
import com.agencyport.api.pojo.DataType;
import com.agencyport.api.worklist.pojo.Field;
import com.agencyport.domXML.APDataCollection;
import com.agencyport.id.Id;
import com.agencyport.rest.RestValidationException;
import com.agencyport.security.model.IUserGroups;
import com.agencyport.security.profile.ISecurityProfile;
import com.agencyport.shared.APException;
import com.agencyport.utils.APDate;
import com.agencyport.utils.AppProperties;
import com.agencyport.utils.text.CoreVariableSubstitutor;
import com.agencyport.utils.text.StringUtilities;
import com.agencyport.utils.text.VariableSubstitutor;
import com.agencyport.webshared.IWebsharedConstants;
import com.agencyport.workitem.model.WorkItemType;
import com.agencyport.worklist.pojo.Filter;
import com.agencyport.worklist.pojo.FilterOption;
import com.agencyport.worklist.pojo.ListView;
import com.agencyport.worklist.pojo.OpCode;
import com.agencyport.worklist.pojo.QueryField;
import com.agencyport.worklist.pojo.SortOption;
import com.agencyport.worklist.pojo.WorkListView;
import com.agencyport.worklist.searchindex.IQueryResults;
import com.agencyport.worklist.searchindex.ISearchIndexMapping;
import com.agencyport.worklist.searchindex.IWorkListSearchProvider;
import com.agencyport.worklist.solr.manager.ISolrManager;
import com.agencyport.worklist.solr.manager.SolrManager;
import com.agencyport.worklist.view.CompiledWorkListView;
import com.agencyport.worklist.view.IConstants;

/**
 * The SolrWorkListSearchProvider class is the default implementation of the worklist search provider
 * interface specific to SOLR work list searching.
 * @since 5.1
 */
public class SolrWorkListSearchProvider implements IWorkListSearchProvider {

	/**
	 * The <code>ASTERISK</code> is a constant for an asterisk string.
	 */
	private static final String ASTERISK = "*";
	
	/**
	 * The <code>INLCUDE_ALL</code> is a constant for inclusion of all rows.
	 */
	private static final String INLCUDE_ALL = "*:*";
	
	/**
	 * The <code>LEFT_PAREN</code> is a constant for the '(' character.
	 */
	private static final char LEFT_PAREN = '(';
	/**
	 * The <code>RIGHT_PAREN</code> is a constant for the ')' character.
	 */
	private static final char RIGHT_PAREN = ')';
	/**
	 * The <code>LEFT_BRACKET</code> is a constant for the '[' character.
	 */
	private static final char LEFT_BRACKET = '[';
	/**
	 * The <code>RIGHT_BRACKET</code>is a constant for the ']' character.
	 */
	private static final char RIGHT_BRACKET = ']';
	/**
	 * The <code>LEFT_BRACE</code> is a constant for the '{' character.
	 */
	private static final char LEFT_BRACE = '{';
	/**
	 * The <code>RIGHT_BRACE</code>is a constant for the '}' character.
	 */
	private static final char RIGHT_BRACE = '}';
	/**
	 * The <code>COLON</code>is a constant for the ':' character.
	 */
	private static final char COLON = ':';
	/**
	 * The <code>MINUS</code>is a constant for the '-' character.
	 */
	private static final char MINUS = '-';
	/**
	 * The <code>AND</code> is a constant for the string: "AND"
	 */
	private static final String AND = "AND";
	/**
	 * The <code>OR</code> is a constant for the string: "OR"
	 */
	private static final String OR = "OR";
	/**
	 * The <code>NOT</code> is a constant for the string: "NOT"
	 */
	private static final String NOT = "NOT";

	/**
	 * The <code>MATCH_SPECIAL_WORD_REGEX</code> identifies special words within
	 * a Solr operand (a.k.a. the search term). This logically identifies "AND",
	 * "OR", and "NOT" as special words, only if they are whole words:
	 * "\\b(AND|OR|NOT)\\b"
	 */
	private static final Pattern MATCH_SPECIAL_WORD_REGEX = Pattern.compile("\\b(" + AND + "|" + OR + "|" + NOT + ")\\b");

	/**
	 * The <code>AND</code> is a constant for the string: " AND "
	 */
	private static final String PADDED_AND = " " + AND + " ";
	/**
	 * The <code>OR</code> is a constant for the string: " OR "
	 */
	private static final String PADDED_OR = " " + OR + " ";
	/**
	 * The <code>TO</code> is a constant for the string: " TO "
	 */
	private static final String TO = " TO ";

	/**
	 * <USERGROUP_IDS> is a string constant for user group ids 
	 */
	public static final String USERGROUP_IDS = "USERGROUP_IDS";
	
	/**
	 * The <MEDIA_TYPE> is a string constant for MEDIA_TYPE
	 */
	public static final String MEDIA_TYPE = "MEDIA_TYPE";
	
	/**
	 * The <ROLES> is a string constant for ROLES
	 */
	public static final String ROLES = "ROLES";
	
	/**
	 * The <code>workListViewResource</code> is a reference to the worklist view compiled resource.
	 */
	private CompiledWorkListView workListViewResource;
	
	/**
	 * The <code>solrIndexMap</code> is a reference to the compiled SOLR index mapping resource.
	 */
	private ISearchIndexMapping indexMap;
	/**
	 * The <code>workListViewResourceData</code> is a reference to the underlying XML for the compiled resource
	 */
	private APDataCollection workListViewResourceData;
	/**
	 * The <code>solrWorkListView</code> is the incoming view which contains the filtering, sorting, filtering information.
	 */
	private WorkListView solrWorkListView;
	/**
	 * The <code>securityProfile</code> is a reference to the user's security profile.
	 */
	private ISecurityProfile securityProfile;
	/**
	 * The <code>workItemType</code> is the associated work item type.
	 */
	private WorkItemType workItemType;
	/**
	 * The <code>returnMediaType</code> is the media type requested by the caller.
	 */
	private MediaType returnMediaType;

	/**
	 * The <code>builtInVariables</code> contains the built in variables such as the user's login id and user groups.
	 */
	protected final Map<String,String> builtInVariables = new HashMap<String,String>();
	/**
	 * Constructs an instance.
	 */
	public SolrWorkListSearchProvider(){
	}
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public void initialize(MediaType returnMediaType, WorkItemType workItemType, WorkListView solrWorkListView, ISecurityProfile securityProfile) throws APException {
		this.solrWorkListView = solrWorkListView;
		this.returnMediaType = returnMediaType;
		this.securityProfile = securityProfile;
		workListViewResource = CompiledWorkListView.get(solrWorkListView.getName());
		indexMap = workListViewResource.getIndexMap();  
		workListViewResourceData = workListViewResource.getResourceAsDataCollection();
		initializeBuiltInVariables();
	}

	/** 
	 * {@inheritDoc}
	 */

	@Override
	public final IQueryResults search() throws APException {
		SolrQuery solrQuery = createAndInitializeSolrQuery(); 
		ISolrManager solrManager = SolrManager.getSolrManager();
		HttpSolrServer httpSolrServer = solrManager.reserveSolrServerInstance();
		try {
			httpSolrServer.setBaseURL(solrManager.getUrlResolver().resolveUrl(solrWorkListView.getIndex()));
			solrQuery.setRequestHandler("/search");
			QueryResponse queryResponse = httpSolrServer.query(solrQuery);
			return new SolrQueryResults(queryResponse.getResults());
		} catch (SolrServerException | IOException e) {
			throw new APException(e);
		} finally {
			solrManager.unreserveSolrServerInstance(httpSolrServer);
		}
	}
	
	/**
	 * Creates a SOLR query instance fully initialized for query purposes.
	 * @return a SOLR query instance fully initialized for query purposes. 
	 * @throws APException propagated from {@link #setQuery(SolrQuery)}
	 */
	protected SolrQuery createAndInitializeSolrQuery() throws APException {
		SolrQuery solrQuery = new SolrQuery();
		ListView selectedListView = getSelectedListView();
		setBounds(solrQuery, selectedListView);
		setSortClause(solrQuery);
		setFields(solrQuery, selectedListView);
		setQuery(solrQuery);
		return solrQuery;
	}
	/**
	 * Sets the bounds of the query as in the start row and number of rows to return.
	 * @param solrQuery is the SOLR query instance to update.
	 * @param selectedListView is the list view that is selected.
	 */
	protected void setBounds(SolrQuery solrQuery, ListView selectedListView) {
		if (selectedListView != null){
			solrQuery.setStart(selectedListView.getStartRowNumber());
			solrQuery.setRows(selectedListView.getFetchSize());
		}
	}
	/**
	 * Method delegated to for the purposes of getting the selected list view. 
	 * @return the selected list view.
	 */
	protected ListView getSelectedListView(){
		return solrWorkListView.getSelectedListView();
	}
	
	/**
	 * Sets the sort option on the SOLR query instance.
	 * @param solrQuery is the SOLR query instance to update.
	 */
	protected void setSortClause(SolrQuery solrQuery) {
		for (SortOption sortOption : solrWorkListView.getSelectedSortOptions()){
			Field field = indexMap.getField(sortOption.getKey());
			String sortField = field.getIndexFieldName(); 
			solrQuery.addSort(sortField, sortOption.getAscending() ? SolrQuery.ORDER.asc : SolrQuery.ORDER.desc);
		}
	}
	
	/**
	 * Sets the field list to return from the SOLR query.
	 * @param solrQuery is the SOLR query instance to update.
	 * @param selectedListView is the list view that is selected.
	 */
	protected void setFields(SolrQuery solrQuery, ListView selectedListView){
		if (selectedListView == null){
			return;
		}
		for (Field field : selectedListView.getFields()){
			Field realField = indexMap.getField(field.getId());
			solrQuery.addField(realField.getIndexFieldName());
		}
	}
	/**
	 * Sets the query parameter on the SOLR query instance. 
	 * @param solrQuery is the SOLR query instance to update.
	 * @throws APException propagated from {@link #applyFilters(StringBuilder, int)}
	 */
	protected void setQuery(SolrQuery solrQuery) throws APException {
		StringBuilder queryString = new StringBuilder();
		int subQueries = applyQueryFields(queryString, 0); 
		subQueries = applyFilters(queryString, subQueries);
		applySecurityFilter(queryString, subQueries);
		applyTenancyFilter(queryString, subQueries);
		solrQuery.setQuery(queryString.toString());
		
	}
	/**
	 * Applies the filters to the query buffer.  
	 * @param queryString is the query string buffer that will be updated.
	 * @param subQueries is the count of sub query statements currently set on the query string buffer.
	 * @return the total number of sub query statements written including the ones that were written before
	 * this method was called.
	 */
	protected int applyFilters(StringBuilder queryString, int subQueries){
		Map<String, int[]> filterMap = workListViewResource.getFilterMap();
		int subQueryCount = subQueries;
		for (Filter filter : solrWorkListView.getSelectedFilters()){
			if (subQueryCount > 0){
				queryString.append(PADDED_AND);
			}
			// AND (lob:CFRM OR lob:BOP)
			queryString.append(LEFT_PAREN);
			int[] filterIndex = filterMap.get(filter.getName());
			String fieldRefId = workListViewResourceData.getAttributeText(IConstants.FILTERS_FILTER_XPATH, filterIndex, IConstants.FIELD_REF_ID);  
			Field field = indexMap.getField(fieldRefId);
			String solrName = field.getIndexFieldName();
			int filterOptions = 0;
			for (FilterOption filterOption : filter.getFilterOptions()){
				if (filterOptions > 0){
					queryString.append(PADDED_OR);
				}
				queryString.append(solrName);
				queryString.append(COLON);
				queryString.append(filterOption.getValue());
				filterOptions++;
			}
			queryString.append(RIGHT_PAREN);
			subQueryCount++;
		}
		return subQueryCount;
	}
	/**
	 * Applies the query fields to the query buffer.  
	 * @param queryString is the query string buffer that will be updated.
	 * @param subQueries is the count of sub query statements currently set on the query string buffer.
	 * @return the total number of sub query statements written including the ones that were written before
	 * this method was called.
	 * @throws APException if a date field conversion failed.
	 */
	protected int applyQueryFields(StringBuilder queryString, int subQueries) throws APException {
		int subQueryCount = subQueries;
		for ( QueryField queryField : normalizeQueryFields(solrWorkListView.getSelectedQueries().getQueryFields())){
			OpCode opCode = queryField.getOpCode();
			if (opCode == null){
				continue;
			}
			if (subQueryCount > 0){
				queryString.append(PADDED_AND);
			}
			boolean needParens = opCode.equals(OpCode.ONE_OF);  
			if (needParens){
				queryString.append(LEFT_PAREN);
			}
			if (opCode.equals(OpCode.NOT_EQUAL)){
				queryString.append(MINUS);
			}
			Field field = indexMap.getField(queryField.getRelatedFieldId());
			String solrName = field.getIndexFieldName();
			String solrSearchName = field.getIndexSearchFieldName();
			if (opCode.getNeedsTwoOperands()){
				queryString.append(solrName);
				queryString.append(COLON);
				
				if(opCode.equals(OpCode.LESS_THAN) || opCode.equals(OpCode.GREATER_THAN)){
					queryString.append(LEFT_BRACE);
				}else{
					queryString.append(LEFT_BRACKET);
				}
			}
			int numOperandsProcessed = 0;
			
			for (String operand : queryField.getOperands()){
				String normalizedValue = normalizeOperand(operand, field);
				if (numOperandsProcessed > 0){
					if (opCode.getNeedsTwoOperands()){
						queryString.append(TO);
					} else {
						queryString.append(PADDED_OR);
					}
				}
				if (!opCode.getNeedsTwoOperands()){
					if (opCode.equals(OpCode.CONTAINS) && !StringUtilities.isEmpty(solrSearchName)){
						if(!INLCUDE_ALL.equals(normalizedValue)){
							queryString.append(solrSearchName);
							queryString.append(COLON);
						}
					} else {
						queryString.append(solrName);
						queryString.append(COLON);
					}					
				} 
				queryString.append(normalizedValue);
				numOperandsProcessed++;
			}
			if (opCode.getNeedsTwoOperands()){
				if(opCode.equals(OpCode.LESS_THAN) || opCode.equals(OpCode.GREATER_THAN)){
					queryString.append(RIGHT_BRACE);
				}else{
					queryString.append(RIGHT_BRACKET);
				}
			}
			if (needParens){
				queryString.append(RIGHT_PAREN);
			}
			subQueryCount++;
		}
		return subQueryCount;
	}
	
	/**
	 * Normalizes an operand value. This implementation formats dates to the
	 * SOLR UTC format, surrounds string literals with {@link #QUOT} characters,
	 * encrypts search terms for hash to hash comparison, and applies variable
	 * substitution.
	 * 
	 * @param value
	 *            is the value to normalize.
	 * @param relatedField
	 *            is the field related to this operand.
	 * @return the normalized value.
	 * @throws RestValidationException
	 *             on data parsing issues.
	 */
	protected String normalizeOperand(String value, Field relatedField) throws RestValidationException {
		String normalizedValue = value;
		DataType dataType = relatedField.getType();
		try {
			if (VariableSubstitutor.containsVariables(value)) {
				CoreVariableSubstitutor vSub = CoreVariableSubstitutor.getStandardVariableSubstitor();
				normalizedValue = vSub.applyVariableSubstitutions(value, builtInVariables);
			}

			if (!ASTERISK.equals(normalizedValue)) {
				if (DataType.DATE.equals(dataType)) {
					// Please note that the following call does take locale into
					// account and the date display format.
					APDate date = new APDate(normalizedValue);
					normalizedValue = SolrManager.formatDate(date.getUtilDate());
				} else if (DataType.STRING.equals(dataType)) {
					// user entered upper case AND/OR substrings need to be
					// converted to lower case to escape them
					normalizedValue = escapeSpecialWords(normalizedValue);
				}				
				
				/**
				 * TODO: Field encryption bug logged: DP-736
				 */				
//				if (relatedField.getEncrypted()) {
//					normalizedValue = Field.encryptValue(normalizedValue);
//				}
				
				// Escape other special chars like ':'s by prepending with \\
				normalizedValue = ClientUtils.escapeQueryChars(normalizedValue);
			}else{
				//include empty records
				if (!DataType.DATE.equals(dataType)){
					normalizedValue= INLCUDE_ALL; 
				}
			}

		} catch (ParseException parseException){
			String invalidDateMsg = String.format("Invalid date value: '%s' found for field named: '%s'", value, relatedField.getName());
			throw new RestValidationException(invalidDateMsg, parseException);
		}
		return normalizedValue;
	}
	
	/**
	 * Logically breaks the given <code>operandValue</code> (a.k.a. the search
	 * term) into whole words and then escapes the "special words" (AND, OR, and
	 * NOT) by converting them to lower-case.
	 * 
	 * <p>
	 * See: <a href=
	 * "https://stackoverflow.com/questions/10337908/how-to-properly-escape-or-and-and-in-lucene-query/10757968#comment53655292_10757968">
	 * https://stackoverflow.com/questions/10337908/how-to-properly-escape-or-
	 * and-and-in-lucene-query/10757968#comment53655292_10757968</a>
	 * 
	 * 
	 * @param operandValue
	 *            the operand value which must correspond to a STRING field type
	 * @return the operandValue with the double-backslash escape prefix
	 *         prepended to any special words
	 */
	private String escapeSpecialWords(String operandValue) {

		Matcher m = MATCH_SPECIAL_WORD_REGEX.matcher(operandValue);
		StringBuffer sb = new StringBuffer(); // NOSONAR
		while (m.find()) {
			// The start(1) and end(1) invocations in the replacement string dynamically resolve
			// to the start and end indexes of the first group in a given match from our input 
			// string - in the case of our regex, the one and only group is the literal AND, OR, 
			//or NOT sequence.
			m.appendReplacement(sb, m.group(1).toLowerCase());
		}
		m.appendTail(sb);
		operandValue = sb.toString();
		return operandValue;
	}

	/**
	 * Initialize the built in variables that represent the transient state.
	 * These variable can be used in the worklist configuration using the syntax like ${USERID}
	 */
	protected void initializeBuiltInVariables(){
		builtInVariables.put(IWebsharedConstants.USERID, securityProfile.getSubject().getId().toString());
		
		Set<Id> ids = securityProfile.getUserGroups().getUserGroupIds(securityProfile.getSubject().getPrincipal());
		builtInVariables.put(USERGROUP_IDS, buildContainsOperand(new HashSet<Object>(ids)));
		
		Set<String> roleNames = securityProfile.getRoles().getRoleNames(securityProfile.getSubject().getPrincipal());
		builtInVariables.put(ROLES, buildContainsOperand(new HashSet<Object>(roleNames)));
		
		builtInVariables.put(MEDIA_TYPE, returnMediaType.toString());
	}
	/**
	 * Applies the security constraints to the query   
	 * @param queryString is the query string buffer that will be updated.
	 * @param subQueries is the count of sub query statements currently set on the query string buffer.
	 * @return the total number of sub query statements written including the ones that were written before
	 * this method was called.
	 */
	protected int applySecurityFilter(StringBuilder queryString, int subQueries){
		if(AccountManagementFeature.get().isSupportGroupAccess()){
			IUserGroups userGroups = securityProfile.getUserGroups();
			if(null != userGroups){
				Set<Id> ids = userGroups.getUserGroupIds(securityProfile.getSubject().getPrincipal());
				
				StringBuilder filter = new StringBuilder();

				filter.append(buildContainsOperand(new HashSet<Object>(ids)));
				
				if (subQueries > 0 ){
					queryString.append(PADDED_AND);
				}
				Field userGroupField = indexMap.getField("user_group_id");
				queryString.append(userGroupField.getIndexFieldName());
				queryString.append(COLON);
				queryString.append(filter);
			}
			return subQueries + 1;
		} else {
			return subQueries;
		}

	}
	
	/**
	 * Applies the tenancy constraints to the query   
	 * @param queryString is the query string buffer that will be updated.
	 * @param subQueries is the count of sub query statements currently set on the query string buffer.
	 * @return the total number of sub query statements written including the ones that were written before
	 * this method was called.
	 */
	protected int applyTenancyFilter(StringBuilder queryString, int subQueries){
		if(AppProperties.getAppProperties().getTrueFalseBooleanProperty("application.apply_tenancy_filter", true)){
			String tenant_id = securityProfile.getClient().getId().asString();
			if(null != tenant_id){				
				
				if (subQueries > 0 ){
					queryString.append(PADDED_AND);
				}
				Field tenantField = indexMap.getField("tenant_id");
				queryString.append(tenantField.getIndexFieldName());
				queryString.append(COLON);
				queryString.append(tenant_id);
			}
			return subQueries + 1;
		} else {
			return subQueries;
		}

	}
	
	/**
	 * Builds a contains expression.
	 * @param list is the list of operands.
	 * @return a contains expression.
	 */
	protected String buildContainsOperand(Collection<Object> list){
		StringBuilder operandString = new StringBuilder();
		
		operandString.append(LEFT_PAREN);
		Iterator<Object> it = list.iterator();
		while(it.hasNext()){
			operandString.append(it.next());
			if(it.hasNext()){
				operandString.append(PADDED_OR);
			}
		}
		operandString.append(RIGHT_PAREN);
		return operandString.toString();
	}
	
	/**
	 * Normalizes a list of query fields so that GREATER_THAN and LESS_THAN operators are normalized
	 * to a standardized BETWEEN operator pattern. 
	 * @param queryFields is the list of query fields to normalize.
	 * @return the normalized list of query fields.
	 * @throws APException if the normalization process failed.
	 */
	protected List<QueryField> normalizeQueryFields(List<QueryField> queryFields) throws APException {
		List<QueryField> normalizedQueryFields = new ArrayList<>();
		for (QueryField queryField : queryFields){
			OpCode opCode = queryField.getOpCode();
			if (OpCode.GREATER_THAN.equals(opCode) || OpCode.LESS_THAN.equals(opCode)){
				QueryField normalizedQueryField = cloneQueryField(queryField);
				List<String> operands = normalizedQueryField.getOperands();
				operands.clear();
				if (OpCode.GREATER_THAN.equals(opCode)){
					operands.addAll(queryField.getOperands());
					operands.add(ASTERISK);
				} else {
					operands.add(ASTERISK);
					operands.addAll(queryField.getOperands());
				}
				normalizedQueryFields.add(normalizedQueryField);
			} else {
				normalizedQueryFields.add(queryField);
			}
		}
		return normalizedQueryFields;
	}
	
	/**
	 * Simple wrapper for QueryField.clone().
	 * @param field is the query field to clone.
	 * @return the cloned query field.
	 * @throws APException propagated from {@link QueryField#clone()}
	 */
	private static QueryField cloneQueryField(QueryField field) throws APException {
		try {
			return (QueryField) field.clone();
		} catch (CloneNotSupportedException cnse){
			throw new APException(cnse);
		}
	}

	@Override
	public CompiledWorkListView getWorkListViewResource() {
		return workListViewResource;
	}
	/**
	 * @return the workListViewResourceData
	 */
	protected APDataCollection getWorkListViewResourceData() {
		return workListViewResourceData;
	}
	/**
	 * @return the solrWorkListView
	 */
	protected WorkListView getSolrWorkListView() {
		return solrWorkListView;
	}
	/**
	 * @return the securityProfile
	 */
	protected ISecurityProfile getSecurityProfile() {
		return securityProfile;
	}
	/**
	 * @return the workItemType
	 */
	protected WorkItemType getWorkItemType() {
		return workItemType;
	}
	/**
	 * @return the returnMediaType
	 */
	protected MediaType getReturnMediaType() {
		return returnMediaType;
	}


}
