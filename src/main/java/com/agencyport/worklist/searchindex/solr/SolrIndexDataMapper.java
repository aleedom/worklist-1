/*
 * Created on Feb 2, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.searchindex.solr;

import org.apache.solr.common.SolrInputDocument;

import com.agencyport.api.worklist.pojo.Field;
import com.agencyport.resource.ResourceException;
import com.agencyport.shared.APException;
import com.agencyport.worklist.searchindex.IDataReader;
import com.agencyport.worklist.searchindex.ISearchIndexMapping;
import com.agencyport.worklist.searchindex.IndexDataMapper;

/**
 * The SolrIndexDataMapper class provides the default implementation of the index mapper
 * abstraction specifically for SOLR index processing.
 * @since 5.1
 */
public class SolrIndexDataMapper extends IndexDataMapper {

	/** 
	 * Creates an instance.
	 * @param index is the name of the index.
	 * @param reader is the data reader.
	 * @throws ResourceException propagated from  {@link IndexDataMapper#IndexDataMapper(String, IDataReader)}
	 */
	public SolrIndexDataMapper(String index, IDataReader reader) throws ResourceException {
		super(index, reader);
	}

	/** 
	 * {@inheritDoc}
	 */

	@Override
	public Object createIndexDocument(Object dataSource) throws APException {
		SolrInputDocument doc = new SolrInputDocument();
		IDataReader reader = this.getReader();
		ISearchIndexMapping indexMap = this.getIndexMap();
		reader.setDataSource(dataSource);
		for (Field field : indexMap){
			Object value = reader.read(field);
			if (value != null){
				doc.addField(field.getIndexFieldName(), value);
			}
		}
		return doc;
	}
	
}
