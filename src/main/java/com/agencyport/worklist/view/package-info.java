/**
 * Supporting classes and interfaces for worklist view processing.
 * @since 5.1 
 */
package com.agencyport.worklist.view;