/*
 * Created on Jan 2, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.view;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Map;

import org.jdom2.Document;
import org.jdom2.Element;

import com.agencyport.api.worklist.pojo.FilterType;
import com.agencyport.domXML.APDataCollection;
import com.agencyport.resource.Resource;
import com.agencyport.resource.ResourceException;
import com.agencyport.resource.ResourceType;
import com.agencyport.resource.RestResource;
import com.agencyport.resource.RestResponse;
import com.agencyport.resource.XMLCompiledResourceEntity;
import com.agencyport.utils.AppProperties;
import com.agencyport.utils.ArrayHelper;
import com.agencyport.utils.PerfObject;
import com.agencyport.utils.PerfObjectCollector;
import com.agencyport.utils.text.StringUtilities;
import com.agencyport.workitem.model.WorkItemType;
import com.agencyport.worklist.searchindex.CompiledIndexMapping;
import com.agencyport.worklist.searchindex.ISearchIndexMapping;

/**
 * The CompiledSolrWorkListView class is the compilation of a work list view artifact.
 * @since 5.1
 */
public class CompiledWorkListView extends XMLCompiledResourceEntity {
	/**
	 * The <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = 1711511424679577029L;
	/**
	 * The <code>workListViewResourceData</code> is the data collection for the work list view XML document. 
	 */
	private final APDataCollection workListViewResourceData;
	
	/**
	 * The <code>filterNameToTypeMap</code> ties a filter name to its specific type.
	 */
	private transient Map<String, FilterType> filterNameToTypeMap;
	
	
	/**
	 * The ElementPathIndexMap class is a map of id attribute value keys with its numeric index value. This derivation
	 * overrides the {@link java.util.HashMap#get(Object)} method so as to provide a centralization of error detection. 
	 */
	private static final class ElementPathIndexMap extends HashMap<String, int[]> {

		/**
		 * The <code>serialVersionUID</code>
		 */
		private static final long serialVersionUID = -6446028477380324063L;
		
		/**
		 * The <code>name</code> is the element path of this map. 
		 */
		private final String elementPath;
		
		/**
		 * The <code>resource</code> is the associated resource.
		 */
		private final Resource resource;
		
		/**
		 * Constructs an instance.
		 * @param elementPath is the element path of this map.
		 * @param resource is the associated resource.
		 */
		public ElementPathIndexMap(String elementPath, Resource resource){
			this.elementPath = elementPath;
			this.resource = resource;
		}
		
		/** 
		 * {@inheritDoc}
		 */ 
		@Override
		public String toString(){
			StringBuilder stateAsString = new StringBuilder(ElementPathIndexMap.class.getSimpleName());
			stateAsString.append('[');
			stateAsString.append("name=");
			stateAsString.append(elementPath);
			stateAsString.append(", resource=");
			stateAsString.append(resource.getResourceId());
			stateAsString.append(", entries=");
			int count = 0;
			for (Map.Entry<String, int[]> entry : this.entrySet()){
				if (count > 0){
					stateAsString.append(", ");
				}
				stateAsString.append(entry.getKey());
				stateAsString.append("=");
				stateAsString.append(ArrayHelper.prettyPrint(entry.getValue()));
				count++;
			}
			stateAsString.append(']');
			return stateAsString.toString();
		}
		
		/** 
		 * {@inheritDoc}
		 */ 
		@Override
		public int[] get(Object key){
			int[] ix = super.get(key);
			if (ix == null){
				String error = String.format("Referential integrity issue with resource '%s'. Attempting to access path '%s[@id='%s'' but no value was found. Please check product artifact for data integrity.", 
						this.resource.getResourceId(), this.elementPath, key);
				throw new IllegalStateException(error);
			}
			return ix;
		}
		
	}
	/**
	 * The <code>filterMap</code> is the filter map.
	 */
	private final ElementPathIndexMap filterMap;
	/**
	 * The <code>filtersMap</code> is the filters map.
	 */
	private final ElementPathIndexMap filtersMap;
	/**
	 * The <code>sortInfoMap</code> is the sort info map.
	 */
	private final ElementPathIndexMap sortInfoMap;
	/**
	 * The <code>queryInfoMap</code> is the query info map.
	 */
	private final ElementPathIndexMap queryInfoMap;
	/**
	 * The <code>workItemType</code> is the related work item type. 
	 */
	private final WorkItemType workItemType;
	/**
	 * The <code>viewName</code> is the view name for this instance.
	 */
	private final String viewName;
	
	/**
     * The <code>WORKLISTVIEW_URL</code> is the partial URL for configuration service.
     */
    private static final String WORKLISTVIEW_URL = AppProperties.getAppProperties().getStringProperty("worklist_view_resource", null);
	
	
	/**
	 * The <code>index</code> is the index name.
	 */
	private final String index;
	
	/**
	 * Constructs an instance. 
	 * @param resource is the associated resource. 
	 * @param document is the XML contents for this resource.
	 */
	public CompiledWorkListView(Resource resource, Document document) {
		super(resource, document);
		workListViewResourceData = new APDataCollection(ResourceType.WORK_LIST_VIEW_DEFINITION.getSchemaName());
		workListViewResourceData.setDocument(this.getDocument());
		filterMap = getMap(workListViewResourceData, "filters.filter", resource); 
		filtersMap = getMap(workListViewResourceData, "filters", resource); 
		sortInfoMap = getMap(workListViewResourceData, "sortInfos.sortInfo", resource); 
		queryInfoMap = getMap(workListViewResourceData, "queryInfos.queryInfo", resource);
		String workItemTypeValue = workListViewResourceData.getAttributeText(null, "workItemType");
		if ("regular".equals(workItemTypeValue)){
			workItemType = WorkItemType.REGULAR_LOB_WORK_ITEM_TYPE;
		} else if ("account".equals(workItemTypeValue)){
			workItemType = WorkItemType.ACCOUNT_WORK_ITEM_TYPE;
		} else {
			throw new IllegalArgumentException(String.format("Unable to render WorkItemType value from : %s", workItemTypeValue));
		}
		viewName = workListViewResourceData.getAttributeText(null, "viewName");
		index = workListViewResourceData.getAttributeText(null, "index");
		filterNameToTypeMap = buildFilterNameToTypeMap(workListViewResourceData);
	}
	
	/**
	 * Builds the filter name to type map.
	 * @param workListViewResourceData is the data collection to query.
	 * @return the filter name to type map.
	 */
	private static Map<String, FilterType> buildFilterNameToTypeMap(APDataCollection workListViewResourceData){
		Map<String, FilterType> filterNameToTypeMap = new HashMap<>();
		for (int[] filterIndex : workListViewResourceData.createIndexTraversalBasis(IConstants.FILTERS_FILTER_XPATH)){
			String filterName = workListViewResourceData.getAttributeText(IConstants.FILTERS_FILTER_XPATH, filterIndex, IConstants.NAME);
			String type = workListViewResourceData.getAttributeText(IConstants.FILTERS_FILTER_XPATH, filterIndex, IConstants.TYPE);
			filterNameToTypeMap.put(filterName, FilterType.valueOf(type));
		}		
		return filterNameToTypeMap;
	}
	
	/**
	 * Builds a map of id and name (if the name exists) attribute key values with their corresponding index.
	 * @param workListViewResourceData is the data collection to query.
	 * @param elementPath is the element path to traverse.
	 * @param resource is the associated resource.
	 * @return a map of id attribute key values with their corresponding index.
	 */
	private static ElementPathIndexMap getMap(APDataCollection workListViewResourceData, String elementPath, Resource resource) {
		ElementPathIndexMap map = new ElementPathIndexMap(elementPath, resource);
		for ( int[] index : workListViewResourceData.createIndexTraversalBasis(elementPath)){
			String key = workListViewResourceData.getAttributeText(elementPath, index, "id");
			int[] clonedIndex = index.clone(); 
			map.put(key, clonedIndex);
			String name = workListViewResourceData.getAttributeText(elementPath, index, "name");
			if (!StringUtilities.isEmpty(name)){
				map.put(name, clonedIndex);
			}
			String solrName = workListViewResourceData.getAttributeText(elementPath, index, "solrName");
			if (!StringUtilities.isEmpty(solrName)){
				map.put(solrName, clonedIndex);
			}
		}
		return map;
	}
	
	/**
	 * Returns an APDataCollection for the underlying XML content of this work list view.
	 * @return an APDataCollection for the underlying XML content of this work list view.
	 */
	public APDataCollection getResourceAsDataCollection(){
		return (APDataCollection) this.workListViewResourceData.clone();
	}

	/**
	 * @return the filterMap
	 */
	public Map<String, int[]> getFilterMap() {
		return filterMap;
	}

	/**
	 * @return the filtersMap
	 */
	public Map<String, int[]> getFiltersMap() {
		return filtersMap;
	}

	/**
	 * @return the sortInfoMap
	 */
	public Map<String, int[]> getSortInfoMap() {
		return sortInfoMap;
	}

	/**
	 * @return the queryInfoMap
	 */
	public Map<String, int[]> getQueryInfoMap() {
		return queryInfoMap;
	}

	/**
	 * @return the workItemType
	 */
	public WorkItemType getWorkItemType() {
		return workItemType;
	}

	/**
	 * @return the viewName
	 */
	public String getViewName() {
		return viewName;
	}

	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public String toString() {
		return super.toString() + " CompiledSolrWorkListView [" + 
				"filterMap=" + filterMap + 
				", filtersMap=" + filtersMap + 
				", sortInfoMap=" + sortInfoMap + 
				", queryInfoMap=" + queryInfoMap + 
				", workItemType=" + workItemType + 
				", solrIndex=" + index + 
				", viewName=" + viewName + "]";
	}
	
	/**
	 * Override the default de-serialization to inflate an instance.
	 * @param is is the input stream from which this instance is being based on.
	 * @throws IOException is propagated from the {@link ObjectInputStream#readObject()} method.
	 * @throws ClassNotFoundException is propagated from the {@link ObjectInputStream#readObject()} method.
	 */
	private void readObject(ObjectInputStream is) throws IOException, ClassNotFoundException {
		is.defaultReadObject();
		filterNameToTypeMap = buildFilterNameToTypeMap(workListViewResourceData);
	}
	
	/**
	 * Fetches the compiled work list view for the given view name.
	 * @param viewName is the view name.
	 * @return the compiled work list view for the given view name.
	 * @throws ResourceException if the compiled view could not be found. 
	 */
	public static CompiledWorkListView get(String viewName) throws ResourceException {
	    PerfObject perfObject = PerfObjectCollector.startNew(CompiledWorkListView.class.getName() + ".get() "+viewName );
        try {
    		RestResource resource = new RestResource();
    		StringBuffer buffer = new StringBuffer();
    		buffer.append(WORKLISTVIEW_URL).append("/").append(viewName);
    		resource.initialize(buffer.toString());
    		RestResponse response = resource.read();
            Element workListViewDefinition =  response.getResults().getObject().getChild("workListViewDefinition").detach();
    		CompiledWorkListView compiledSolrWorkListView = new  CompiledWorkListView(resource, new Document(workListViewDefinition));
    		return compiledSolrWorkListView;
        }finally {
            perfObject.stop();
        }
	}
	
	/**
	 * Returns the filter type for a given filter name.
	 * @param filterName is the filter name.
	 * @return the filter type for a given filter name.
	 */
	public FilterType getFilterType(String filterName){
		return this.filterNameToTypeMap.get(filterName);
	}

	/**
	 * @return the name of the index.
	 */
	public String getIndex() {
		return index;
	}
	
	/**
	 * Gets access to the related index mapping resource.
	 * @return the related index mapping resource.
	 * @throws ResourceException propagated by {@link CompiledIndexMapping#get(String)} 
	 */
	public ISearchIndexMapping getIndexMap() throws ResourceException {
		return CompiledIndexMapping.get(index);
	}
	
}
