/*
 * Created on Dec 9, 2014 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.view;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.agencyport.worklist.pojo.Filter;
import com.agencyport.worklist.pojo.ListView;
import com.agencyport.worklist.pojo.QueryField;
import com.agencyport.worklist.pojo.QueryInfo;
import com.agencyport.worklist.pojo.SavedSearch;
import com.agencyport.worklist.pojo.SortInfo;
import com.agencyport.worklist.pojo.SortOption;
import com.agencyport.worklist.pojo.WorkListView;
import com.agencyport.database.DatabaseAgent;
import com.agencyport.database.DatabaseKey;
import com.agencyport.database.IDCreator;
import com.agencyport.database.provider.BaseDBProvider;
import com.agencyport.database.provider.DatabaseResourceAgent;
import com.agencyport.id.Id;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.logging.LoggingManager;
import com.agencyport.product.ProductDefinitionsManager;
import com.agencyport.rest.RestValidationException;
import com.agencyport.rest.Serializer;
import com.agencyport.shared.APException;
import com.agencyport.utils.APDate;
import com.agencyport.utils.text.CharacterEncoding;
import com.agencyport.utils.text.StringUtilities;

/**
 * The ViewDBProvider class provides the CRUD operations
 * against the work_list_view table.
 * @since 5.1
 */
public class ViewDBProvider extends BaseDBProvider {
	/**
	 * The <code>LOGGER</code> is our logger.
	 */
	private static final Logger LOGGER = LoggingManager.getLogger(ViewDBProvider.class.getPackage().getName());
	
	
	/**
	 * The <code>EMPTY_STRING</code> a constant for an empty string.
	 */
	private static final String EMPTY_STRING = "";
	
	/**
	 * The <code>DB_BOOLEAN_FALSE</code> constant for a "false" short integer. 
	 */
	private static final short DB_BOOLEAN_FALSE = 0;
	
	/**
	 * The <code>DB_BOOLEAN_TRUE</code> constant for a "true" short integer.
	 */
	private static final short DB_BOOLEAN_TRUE = 1;
	
	/**
	 * The <code>TABLE_NAME</code> specifies the table upon which this
	 * provider operates against.
	 */
	private static final String TABLE_NAME = "view_search_info";
	/**
	 * The <code>SUBJECT_ID_COLUMN_NAME</code> a constant for the subject_id column name.
	 */
	private static final String SUBJECT_ID_COLUMN_NAME = "subject_id";
	/**
	 * The <code>VIEW_NAME_COLUMN_NAME</code> a constant for the view_name column name.
	 */
	private static final String VIEW_NAME_COLUMN_NAME = "view_name";
	/**
	 * The <code>VERSION_COLUMN_NAME</code> a constant for the version column name.
	 */
	private static final String VERSION_COLUMN_NAME = "version";
	/**
	 * The <code>INFO_COLUMN_NAME</code> a constant for the info column name.
	 */
	private static final String INFO_COLUMN_NAME = "search_info";
	
	/**
	 * The <code>SEARCH_INFO_ID_COLUMN_NAME</code> a constant for the search info id column name.
	 */
	private static final String SEARCH_INFO_ID_COLUMN_NAME = "search_info_id";
	
	/**
	 * The <code>ACTIVE_FLAG_COLUMN_NAME</code> a constant for the active flag column name. 
	 */
	private static final String ACTIVE_FLAG_COLUMN_NAME = "active_flag";

	/**
	 * The <code>SEARCH_INFO_NAME_COLUMN_NAME</code> a constant for the search info name column name.
	 */
	private static final String SEARCH_INFO_NAME_COLUMN_NAME = "search_info_name";
	/**
	 * The <code>LAST_UPDATE_TIME_COLUMN_NAME</code> a constant for the last update time column name.
	 */
	private static final String LAST_UPDATE_TIME_COLUMN_NAME = "last_update_time"; 
	/**
	 * The <code>SEARCH_INFO_ID_COLUMNS</code> is an array of the non-key columns to the search_info table.
	 */
	private static final String[] SEARCH_INFO_ID_COLUMNS = {
		SEARCH_INFO_ID_COLUMN_NAME
	};
	
	/**
	 * The AllRecordsAccess class contains the SQL statements and column name references for retrieving all saved search items
	 * for a given user / view combination.
	 */
	private static final class AllRecordsAccess {
		/**
		 * Prevent construction.
		 */
		private AllRecordsAccess(){
			
		}
		/**
		 * The <code>KEY_COLUMNS</code> is an array of the key columns to the search_info table.
		 */
		private static final String[] KEY_COLUMNS = {
			SUBJECT_ID_COLUMN_NAME,
			VIEW_NAME_COLUMN_NAME
		};
		
		/**
		 * The <code>NON_KEY_COLUMNS</code> is an array of the non-key columns to the search_info table.
		 */
		private static final String[] NON_KEY_COLUMNS = {
			ACTIVE_FLAG_COLUMN_NAME,
			SEARCH_INFO_ID_COLUMN_NAME,
			SEARCH_INFO_NAME_COLUMN_NAME,
		};
		/**
		 * The <code>SELECT_SQL_STATEMENT</code> is the SQL select statement for retrieving previously saved search info records. 
		 */
		private static final String SELECT_SQL_STATEMENT = DATABASE_AGENT.normalizeSQLStatement(BaseDBProvider.buildSelectStatementExt(TABLE_NAME, KEY_COLUMNS, NON_KEY_COLUMNS));
		
	}
	/**
	 * The ByIdSpecificAccess class contains the SQL statements and column name references for retrieving a saved search record using
	 * the saved search id and the subject id. 
	 */
	private static final class ByIdSpecificAccess {
		
		/**
		 * Prevent construction.
		 */
		private ByIdSpecificAccess(){
			
		}
		/**
		 * The <code>KEY_COLUMNS</code> is an array of the key columns to the search_info table.
		 */
		private static final String[] KEY_COLUMNS = {
			SUBJECT_ID_COLUMN_NAME,
			SEARCH_INFO_ID_COLUMN_NAME,
			VIEW_NAME_COLUMN_NAME,
		};
		
		/**
		 * The <code>NON_KEY_COLUMNS</code> is an array of the non-key columns to the search_info table.
		 */
		private static final String[] NON_KEY_COLUMNS = {
			ACTIVE_FLAG_COLUMN_NAME,
			SEARCH_INFO_NAME_COLUMN_NAME,
			VERSION_COLUMN_NAME,
			LAST_UPDATE_TIME_COLUMN_NAME,
			INFO_COLUMN_NAME
		};
		/**
		 * The <code>NON_KEY_STANDARD_COLUMNS</code> is an array of the non-key columns to the search_info table minus the 
		 */
		private static final String[] NON_KEY_STANDARD_COLUMNS = {
			ACTIVE_FLAG_COLUMN_NAME,
			SEARCH_INFO_NAME_COLUMN_NAME,
			VERSION_COLUMN_NAME,
			LAST_UPDATE_TIME_COLUMN_NAME
		};
		
		/**
		 * The <code>SELECT_SQL_STATEMENT</code> is the SQL select statement for retrieving previously saved search info records. 
		 */
		private static final String SELECT_SQL_STATEMENT = DATABASE_AGENT.normalizeSQLStatement(BaseDBProvider.buildSelectStatementExt(TABLE_NAME, KEY_COLUMNS, NON_KEY_COLUMNS));
		/**
		 * The <code>INSERT_SQL_STATEMENT</code> is the SQL select statement for inserting a new search info record.
		 */
		private static final String INSERT_SQL_STATEMENT = DATABASE_AGENT.normalizeSQLStatement(BaseDBProvider.buildInsertStatementExt(TABLE_NAME, KEY_COLUMNS, NON_KEY_COLUMNS));
		
		/**
		 * The <code>UPDATE_SQL_STATEMENT</code> is the SQL select statement for updating previously saved search info records.
		 */
		private static final String UPDATE_SQL_STATEMENT = DATABASE_AGENT.normalizeSQLStatement(BaseDBProvider.buildUpdateStatementExt(TABLE_NAME, KEY_COLUMNS, NON_KEY_STANDARD_COLUMNS));
		/**
		 * The <code>DELETE_SQL_STATEMENT</code> is the SQL select statement for deleting previously saved search info records.
		 */
		private static final String DELETE_SQL_STATEMENT = DATABASE_AGENT.normalizeSQLStatement(BaseDBProvider.buildDeleteStatement(TABLE_NAME, KEY_COLUMNS));
		
		/**
		 * The <code>INFO_COLUMN_INDEX</code> is the column index for the actual search information column.
		 */
		private static final int INFO_COLUMN_INDEX = KEY_COLUMNS.length + NON_KEY_COLUMNS.length;
	}
	/**
	 * The ByNameSpecificAccess class contains the SQL statements and column name references for retrieving a saved search record using
	 * the saved search name and the subject id. 
	 */
	private static final class ByNameSpecificAccess {
		/**
		 * Prevent construction.
		 */
		private ByNameSpecificAccess(){
			
		}
		/**
		 * The <code>KEY_COLUMNS</code> is an array of the key columns to the search_info table.
		 */
		private static final String[] KEY_COLUMNS = {
			SUBJECT_ID_COLUMN_NAME,
			SEARCH_INFO_NAME_COLUMN_NAME,
			VIEW_NAME_COLUMN_NAME
		};
		
		/**
		 * The <code>EXISTS_SQL_STATEMENT</code> is the SQL select statement for determining the pre-existance of saved search info records.
		 */
		private static final String EXISTS_SQL_STATEMENT = DATABASE_AGENT.normalizeSQLStatement(BaseDBProvider.buildSelectStatementExt(TABLE_NAME, KEY_COLUMNS, SEARCH_INFO_ID_COLUMNS)); 
		
	}
	/**
	 * Constructs an instance.
	 */
	public ViewDBProvider() {
	}

	/**
	 * Constructs an instance.
	 * @param dbResourceAgent for callers who want to control the commit unit
	 * of work.
	 */
	public ViewDBProvider(DatabaseResourceAgent dbResourceAgent) {
		super(dbResourceAgent);
	}
	/**
	 * Creates a saved search instance from a result set.
	 * @param resultSet is the result set to read from.
	 * @return a saved search instance from a result set.
	 * @throws SQLException propagated from result set access failures.
	 */
	private static SavedSearch createSavedSearchInstance(ResultSet resultSet) throws SQLException {
		int id = resultSet.getInt(SEARCH_INFO_ID_COLUMN_NAME);
		String name = resultSet.getString(SEARCH_INFO_NAME_COLUMN_NAME);
		boolean isActive = resultSet.getShort(ACTIVE_FLAG_COLUMN_NAME) == DB_BOOLEAN_TRUE;
		return new SavedSearch(id, name, isActive);
	}
	/**
	 * Reads all of the "non active" saved search items for a given user / view combination.
	 * @param viewName is the view name.
	 * @param subjectId is the id of the user. 
	 * @return all of the "non active" saved search items for a given user / view combination.  
	 * @throws APException if the attempt of accessing the saved searches failed.
	 */
	public List<SavedSearch> getSavedSearches(String viewName, Id subjectId) throws APException {
		try {
			dbResourceAgent.getConnection(true);
			PreparedStatement pStmt = dbResourceAgent.getPreparedStatement(AllRecordsAccess.SELECT_SQL_STATEMENT);
			pStmt.setInt(1, subjectId.intValue());
			pStmt.setString(2, viewName);
			
			ResultSet resultSet = dbResourceAgent.executeQuery(pStmt);
			List<SavedSearch> savedSearches = new ArrayList<>();

			while (resultSet.next()) {
				SavedSearch savedSearch = createSavedSearchInstance(resultSet);
				if (!SavedSearch.isDefaultSavedSearchId(savedSearch.getId())){
					savedSearches.add(savedSearch);
				}
			}
			return savedSearches;
		} catch (APException | SQLException e) {
			ExceptionLogger.log(e, this.getClass(), "getSavedSearches");
			throw APException.create(e);
		} finally {
			dbResourceAgent.closeDatabaseResources(this);
		}
	}
	
	/**
	 * Reads a search info record.
	 * @param viewName is the view name.
	 * @param savedSearchId is the id of the saved search record.
	 * @param subjectId is the id of the user. 
	 * @return a SearchInfo instance or null if none found. If no record is located then a null 
	 * is returned (versus throwing an exception).
	 * @throws APException propagated from a JDBC issue or a serialization issue.
	 */
	public WorkListView load(String viewName, int savedSearchId, Id subjectId) throws APException {
		LOGGER.info(String.format("Attempting to load saved work list view '%s' with saved search id of %d for subject id %s", viewName, savedSearchId, subjectId.asString()));
		try {
			dbResourceAgent.getConnection(true);
			PreparedStatement pStmt = null;
			pStmt = dbResourceAgent.getPreparedStatement(ByIdSpecificAccess.SELECT_SQL_STATEMENT);
			pStmt.setInt(1, subjectId.intValue());
			pStmt.setInt(2, savedSearchId);
			pStmt.setString(3, viewName);
			
			ResultSet resultSet = dbResourceAgent.executeQuery(pStmt);

			if (resultSet.next()) {
				InputStream inStream = DATABASE_AGENT.getLOB(resultSet, ByIdSpecificAccess.INFO_COLUMN_INDEX, DatabaseAgent.LOB_TEXT_TYPE);
				try {
					WorkListView workListView = Serializer.get(MediaType.APPLICATION_JSON_TYPE).deserialize(inStream, WorkListView.obtainClassInfo());
					workListView.setVersion(resultSet.getString(VERSION_COLUMN_NAME));
					workListView.getSavedSearchInfo().setCurrentSearch(createSavedSearchInstance(resultSet));
					return workListView;
				} finally {
					inStream.close();
				}
			} else {
				if (SavedSearch.isDefaultSavedSearchId(savedSearchId)){
					return null;
				} else  {
					final String errMsg = String.format("Unable to retrieve saved search record for saved search id = '%d', view='%s' and subject id=%d.", savedSearchId, viewName, subjectId.intValue());
					LOGGER.warning(errMsg);
					// no search-info for this view/saved search id/user combination.
					throw new RestValidationException(errMsg, Response.Status.NOT_FOUND);	
				}
			}
		} catch (APException | SQLException e) {
			ExceptionLogger.log(e, this.getClass(), "load");
			throw APException.create(e);
		} catch (IOException ioException) {
			LOGGER.log(Level.INFO, 
					String.format("IO Exception encountered deserializing search information for saved search id = '%d', view='%s' and subject id=%d. IO Exception text: '%s'", savedSearchId, viewName, subjectId.intValue(), ioException.getMessage()),
					ioException);
			LOGGER.info("Default configuration will be used.");
			return null;
		} finally {
			dbResourceAgent.closeDatabaseResources(this);
		}
	}
	
	/**
	 * This method minimizes the footprint of each work list view before it is saved to the database.
	 * @param view is the view to minimize.
	 * @return a minimizes view instance.
	 * @throws CloneNotSupportedException propagated from {@link WorkListView#clone()}
	 */
	private WorkListView minimizeWorkListView(WorkListView view) throws CloneNotSupportedException {
		WorkListView viewToSave = (WorkListView) view.clone();
		viewToSave.setLookupLinks(null);
		viewToSave.getSavedSearchInfo().setSavedSearches(null);
		viewToSave.setFilters(viewToSave.getSelectedFilters());
		for (Filter filter : viewToSave.getFilters()){
			if(!filter.isSaveable()){
				filter.setLink(null);
				filter.setFormat(EMPTY_STRING);
				filter.setRelatedFieldId(EMPTY_STRING);
				filter.setStyleClass(EMPTY_STRING);
				filter.setType(null);
			}
		}
		QueryInfo queryInfo = viewToSave.getSelectedQueries();
		viewToSave.setQueryInfo(queryInfo);
		queryInfo.setLinks(null);
		queryInfo.setPageId(EMPTY_STRING);
		queryInfo.setTitle(EMPTY_STRING);
		queryInfo.setTransactionId(EMPTY_STRING);
		for (QueryField queryField : queryInfo.getQueryFields()){
			if(!queryField.isSaveable()){
				queryField.setDataType(null);
				queryField.setPageFieldId(EMPTY_STRING);
				queryField.setRelatedFieldId(EMPTY_STRING);
				queryField.setTitle(EMPTY_STRING);
			}
		}
		SortInfo sortInfo = viewToSave.getSortInfo(); 
		sortInfo.setSortOptions(viewToSave.getSelectedSortOptions());
		sortInfo.setLink(null);
		sortInfo.setTitle(EMPTY_STRING);
		for (SortOption sortOption : sortInfo.getSortOptions()){
			if(!sortOption.isSaveable()){
				sortOption.setRelatedFieldId(EMPTY_STRING);
				sortOption.setTitle(EMPTY_STRING);
			}
		}
		for (ListView listView : viewToSave.getListViews()){
			listView.setFields(null);
		}
		viewToSave.setLink(null);
		viewToSave.setIndex(EMPTY_STRING);
		viewToSave.setTitle(EMPTY_STRING);
		viewToSave.setVersion(EMPTY_STRING);
		
		return viewToSave;
	}
	
	/**
	 * Inserts/updates a search info record.
	 * @param viewName is the view name.
	 * @param subjectId is the id of the user. 
	 * @param view is the search information to save.
	 * @throws APException propagated from a JDBC issue or a serialization issue.
	 */
	public void save(String viewName, Id subjectId, WorkListView view) throws APException {
		Writer writer = null;
		ByteArrayInputStream bis = null;
		try {
			Connection connection = dbResourceAgent.getConnection(false);
			boolean isActiveRecord = view.getSavedSearchInfo().getCurrentSearch().getActive();
			String name = view.getSavedSearchInfo().getCurrentSearch().getName();
			WorkListView viewToSave = minimizeWorkListView(view);
			int id = 0;
			PreparedStatement pStmt = null;
			if (StringUtilities.isEmpty(name)){
				pStmt = dbResourceAgent.getPreparedStatement(ByIdSpecificAccess.SELECT_SQL_STATEMENT);
				pStmt.setInt(1, subjectId.intValue());
				id = SavedSearch.getDefaultSavedSearchId();
				pStmt.setInt(2, id);
				pStmt.setString(3, viewName);
			} else {
				pStmt = dbResourceAgent.getPreparedStatement(ByNameSpecificAccess.EXISTS_SQL_STATEMENT);
				pStmt.setInt(1, subjectId.intValue());
				pStmt.setString(2, name);
				pStmt.setString(3, viewName);
			}
			ResultSet resultSet = dbResourceAgent.executeQuery(pStmt);
			if (resultSet.next()) {
				id = resultSet.getInt(SEARCH_INFO_ID_COLUMN_NAME);
			} else {
				// Do an insert first
				pStmt = dbResourceAgent.getPreparedStatement(ByIdSpecificAccess.INSERT_SQL_STATEMENT);
				pStmt.setInt(1, subjectId.intValue());
				if (!SavedSearch.isDefaultSavedSearchId(id)){
					IDCreator idCreator = new IDCreator(connection);
					id = Integer.valueOf(idCreator.createId(TABLE_NAME).intValue());
				}
				pStmt.setInt(2, id);
				pStmt.setString(3, viewName);
				pStmt.setShort(4, isActiveRecord ? DB_BOOLEAN_TRUE : DB_BOOLEAN_FALSE);
				pStmt.setString(5, name);
				pStmt.setString(6, ProductDefinitionsManager.getCurrentlyRunningVersion().toString());
				pStmt.setTimestamp(7, APDate.now().getTimestamp());
				pStmt.setString(8, "{}");
				dbResourceAgent.executeUpdate(pStmt);
			}
			LOGGER.info(String.format("Saving work list view '%s' with id of %d and name of '%s', for subject id %s", 
					viewName, id,
					name,  
					subjectId.asString()));
			pStmt = dbResourceAgent.getPreparedStatement(ByIdSpecificAccess.UPDATE_SQL_STATEMENT);
			pStmt.setInt(5, subjectId.intValue());
			pStmt.setInt(6, id);
			pStmt.setString(7, viewName);
			pStmt.setShort(1, isActiveRecord ? DB_BOOLEAN_TRUE : DB_BOOLEAN_FALSE);
			pStmt.setString(2, name);
			pStmt.setString(3, ProductDefinitionsManager.getCurrentlyRunningVersion().toString());
			pStmt.setTimestamp(4, APDate.now().getTimestamp());
			DatabaseKey key = new DatabaseKey(subjectId, SUBJECT_ID_COLUMN_NAME);
			Map<String, Object> additionalKeys = new HashMap<>();
			additionalKeys.put(SEARCH_INFO_ID_COLUMN_NAME, id);
			additionalKeys.put(VIEW_NAME_COLUMN_NAME, viewName);
			writer = new StringWriter();
			Serializer.get(MediaType.APPLICATION_JSON_TYPE).serialize(viewToSave, writer);
			bis = new ByteArrayInputStream(writer.toString().getBytes(CharacterEncoding.UTF_8));
			DATABASE_AGENT.updateLOB(connection, TABLE_NAME, INFO_COLUMN_NAME,
					key, bis, DatabaseAgent.LOB_TEXT_TYPE, additionalKeys);
			dbResourceAgent.executeUpdate(pStmt);
			dbResourceAgent.commit(this);
			SavedSearch savedSearch = new SavedSearch(id, name, isActiveRecord);
			view.getSavedSearchInfo().setCurrentSearch(savedSearch);
		} catch (CloneNotSupportedException | IOException | APException | SQLException e) {
			ExceptionLogger.log(e, this.getClass(), "save");
			throw APException.create(e);
		} finally {
			try {
				if (writer != null){
					writer.close();
				}
				if (bis != null){
					bis.close();
				}
			} catch (IOException ioException){
				ExceptionLogger.log(ioException, this.getClass(), "save");
			}
			dbResourceAgent.closeDatabaseResources(this);
		}
	}
	/**
	 * Deletes a search info record.
	 * @param viewName is the view name.
	 * @param savedSearchId is the id of the saved search record.
	 * @param subjectId is the id of the user. 
	 * @throws APException propagated from a JDBC issue.
	 */
	public void delete(String viewName, int savedSearchId, Id subjectId) throws APException {
		try {
			dbResourceAgent.getConnection(false);
			PreparedStatement pStmt = dbResourceAgent.getPreparedStatement(ByIdSpecificAccess.SELECT_SQL_STATEMENT);
			pStmt.setInt(1, subjectId.intValue());
			pStmt.setInt(2, savedSearchId);
			pStmt.setString(3, viewName);
			ResultSet resultSet = dbResourceAgent.executeQuery(pStmt);
			if (resultSet.next()) {
				pStmt = dbResourceAgent.getPreparedStatement(ByIdSpecificAccess.DELETE_SQL_STATEMENT);
				pStmt.setInt(1, subjectId.intValue());
				pStmt.setInt(2, savedSearchId);
				pStmt.setString(3, viewName);
				dbResourceAgent.executeUpdate(pStmt);
				dbResourceAgent.commit(this);
			} else {
				// no search-info for this view/user combination.
				throw new RestValidationException(String.format("Unable to delete saved search record for saved search id = '%d', view='%s' and subject id=%d.", savedSearchId, viewName, subjectId.intValue()), Response.Status.NOT_FOUND);	
			}
		} catch (APException | SQLException e) {
			ExceptionLogger.log(e, this.getClass(), "delete");
			throw APException.create(e);
		} finally {
			dbResourceAgent.closeDatabaseResources(this);
		}
	}

}
