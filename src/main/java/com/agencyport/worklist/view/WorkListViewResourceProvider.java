/*
 * Created on Jan 2, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.view;

import org.jdom2.Document;
import org.jdom2.Element;

import com.agencyport.resource.Resource;
import com.agencyport.resource.ResourceEntityKey;
import com.agencyport.resource.ResourceException;
import com.agencyport.resource.ResourceType;
import com.agencyport.resource.SimpleXMLResourceProvider;
import com.agencyport.resource.XMLCompiledResourceEntity;

/**
 * The SolrWorkListViewResourceProvider class serves up worklist view resources.
 */
public class WorkListViewResourceProvider extends SimpleXMLResourceProvider {

	/**
	 * Constructs an instance.
	 */
	public WorkListViewResourceProvider() {
		super(ResourceType.WORK_LIST_VIEW_DEFINITION);
	}
	
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	protected String readKey(Document document){
        Element rootElement = document.getRootElement();
        return rootElement.getAttributeValue("viewName");
	}

	/**
	 * Retrieves a worklist view APDataCollection resource.
	 * @param viewName is the worklist view name.
	 * @return a worklist view APDataCollection resource. Null if no match on key.
	 * @throws ResourceException if the resource repository could not be accessed.
	 */
	public CompiledWorkListView getWorkListViewResource(String viewName) throws ResourceException {
		ResourceEntityKey key = ResourceEntityKey.create(viewName);
		return (CompiledWorkListView) this.getCompiledEntity(key);
	}

	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	protected XMLCompiledResourceEntity createCompileResourceEntity(Resource resource, Document resourceContent) throws ResourceException {
		return new CompiledWorkListView(resource, resourceContent);
	}
}
