/*
 * Created on Apr 17, 2014 by dan AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.solr.manager;

import com.agencyport.factory.GenericFactory;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.shared.APException;
import com.agencyport.utils.AppProperties;


/**
 * The SolrUrlResolver class provides a defensible implementation of the ISolrUrlResolver whereby all resolution
 * of URLs for SOLR utilization is sourced from application properties.
 * @since 5.0
 */
public class SolrUrlResolver implements ISolrUrlResolver {
	
	/**
	 * The <code>ERROR_NO_CONFIGURED_URL</code> is the error message used when resolveUrl returns null.
	 */
	public static final String ERROR_NO_CONFIGURED_URL = "Unable to find a configured Solr url for index ";
	
	/**
	 * The <code>RESOLVER</code>
	 */
	private static final ISolrUrlResolver RESOLVER = createSolrUrlResolver();
	
	/**
	 * Creation method for the singleton instance of the ISolrUrlResolver.
	 * @return an instance of ISolrUrlResolver.
	 */
	private static ISolrUrlResolver createSolrUrlResolver() {
		try {
			return GenericFactory.create("solr_url_resolver", "com.agencyport.worklist.solr.manager.SolrUrlResolver");
		}
		catch(APException e) {
			ExceptionLogger.log(e, SolrManager.class, "createSolrUrlResolver");
			throw new IllegalStateException("Unable to instantiate instance of SolrUrlResolver.", e);
		}
	}
	
	/**
	 * Singleton getter for the ISolrUrlResolver.
	 * @return a singleton instance of ISolrUrlResolver.
	 */
	public static ISolrUrlResolver get() {
		return RESOLVER;
	}
	
	@Override
	public String resolveUrl(String indexType) {
		String url = null;
		if(indexType.equals(SolrManager.INDEX_TYPE_WORKLIST)) {
			url = AppProperties.getAppProperties().getStringProperty("application.worklist.indexsearch.provider.baseurl", null);
		}
		else if(indexType.equals(SolrManager.INDEX_TYPE_ACCOUNT)) {
			url = AppProperties.getAppProperties().getStringProperty("application.account.indexsearch.provider.baseurl", null);
		}
		
		return url;
	}

}
