package com.agencyport.worklist.solr.manager;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import org.apache.solr.client.solrj.SolrQuery;
import org.json.JSONObject;

import com.agencyport.logging.LoggingManager;
import com.agencyport.shared.APException;

/**
 * The SolrSecurityServlet class is responsible for accepting requests bound for Solr's
 * REST interface and decorating them with parameters that allow the Solr query to adhere
 * to the ACSI security policy.
 * @since 5.0
 */
public class SolrSecurityServlet extends HttpServlet {
	/**
	 * The <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = -3946460946331516494L;
	
	/**
	 * Logger for the SolrSecurityServlet class.
	 */
	private static final Logger LOGGER = LoggingManager.getLogger(SolrSecurityServlet.class.getPackage().getName());
	
	/**
	 * Entry point for solr search queries.
	 * Request parameter are used to perform the solr query and return a json
	 * @param request		The request object.
	 * @param response		The response object
	 * @throws IOException is the exception thrown
	 * @throws ServletException is the exception thrown
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		ISolrManager manager = SolrManager.getSolrManager();
		JSONObject json = null;
		try {
			SolrQuery query = manager.createSolrQueryFromParams(request.getParameterMap());
			String indexType = request.getParameter(SolrManager.INDEX_TYPE_PARAM);
			json = manager.search(query, indexType);
			if (LOGGER.isLoggable(Level.FINE)){
				LOGGER.fine(json.toString());
			}
		} catch(APException e) {
			LOGGER.log(Level.SEVERE, "SolrSecurityServlet doGet operation failed", e);
			json = SolrManager.generateErrorJSON(e.getMessage());
		}
		response.setContentType(MediaType.APPLICATION_JSON);
		response.getWriter().append(json.toString());
	}
}
