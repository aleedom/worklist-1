package com.agencyport.worklist.spring;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.sql.DataSource;

import com.agencyport.env.EnvironmentResourceProvider;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.utils.AppProperties;

/**
 * The HealthMonitor class retrieves information for use by healthDashboard.jsp
 * 
 * @author bcooke
 */
public class HealthMonitor {
    private DataSource dataSource;

    /**
     * Constructor for the Health Monitor class
     * @param dataSource
     */
    public HealthMonitor(DataSource dataSource){
        this.dataSource = dataSource;
    }
    
    /**
     * @return the micro-service name
     */
    public String getService() {
        return AppProperties.getAppProperties().getStringProperty("APPLICATION_NAME", "");
    }

    /**
     * @return the hostname of the server running the user-authorization
     *         micro-service.
     */
    public String getHostname() {
        try {
            return InetAddress.getLocalHost().toString();
        } catch (UnknownHostException e) {
            ExceptionLogger.log(e, null, null);
            return "Error: " + e.getMessage();
        }
    }

    /**
     * @return the IP address of the server running the user-authorization
     *         micro-service.
     */
    public String getIpAddress() {
        try {
            return InetAddress.getLocalHost().toString();
        } catch (UnknownHostException e) {
            ExceptionLogger.log(e, null, null);
            return "Error: " + e.getMessage();
        }
    }

    /**
     * @return the location of the Tomcat application server on the server
     *         running the user-authorization micro-service.
     */
    public String getTomcatLocation() {
        return System.getProperty("catalina.home");
    }

    /**
     * @return Environment Resources Prefixes
     */
    public String getEnvResProvider() {
        return EnvironmentResourceProvider.getDefaultInstance().getPrefixes().toString();
    }

    /**
     * @return a HTML formatted list of properties files.
     * @throws IOException
     */
    public List<String> getPropertyFiles() throws IOException {
        return AppProperties.getResolvedAndLoadedPropertiesReference();
    }

    /**
     * @return the MySQL Schema.
     */
    public String getMySqlSchema() {
        return AppProperties.getAppProperties().getStringProperty("datasource", "");
    }

    /**
     * @return the URL of the MySQL database.
     */
    public String getMySqlUrl() {
        try (java.sql.Connection con = dataSource.getConnection()) {
            java.sql.DatabaseMetaData mtdt = con.getMetaData();
            return mtdt.getURL();
        } catch (SQLException e) {
            ExceptionLogger.log(e, null, null);
            return "Error: " + e.getMessage();
        }
    }

    /**
     * @return the username of the MySQL database.
     * @throws NamingException
     */
    public String getMySqlUsername() {
        try (java.sql.Connection con = dataSource.getConnection()){
            java.sql.DatabaseMetaData mtdt = con.getMetaData();
            return mtdt.getUserName();
        } catch (SQLException e) {
            ExceptionLogger.log(e, null, null);
            return "Error: " + e.getMessage();
        }         
    }

    /**
     * @return the hostname of the Redis server.
     */
    public String getRedisServer() {
        return AppProperties.getAppProperties().getStringProperty("redis.hostname", "");
    }

    /**
     * @return the port that the Redis server is running on.
     */
    public String getRedisPort() {
        return AppProperties.getAppProperties().getStringProperty("redis.port", "");
    }

}
