/*
 * Created on Mar 15, 2016 by pscott AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.spring;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.AccessControlException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.glassfish.jersey.servlet.ServletContainer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AbstractRefreshableWebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.context.support.GenericWebApplicationContext;
import org.springframework.web.context.support.StandardServletEnvironment;

import com.agencyport.api.RestfulAPIApplication;
import com.agencyport.env.EnvironmentContext;
import com.agencyport.fieldvalidation.validators.BaseValidator;
import com.agencyport.logging.ConsoleAppLogger;
import com.agencyport.logging.LoggingManager;
import com.agencyport.resource.FileResource;
import com.agencyport.resource.ResourceException;
import com.agencyport.servlets.shared.ThreadLocalCleanupFilter;
import com.agencyport.shared.APException;
import com.agencyport.utils.AppProperties;

/**
 * The SpringApplicationInitializer class is responsible for standing up the web
 * application (optionally, in conjunction with a web.xml)
 */
@Configuration
@Import(AppConfig.class)
@Order(value = Ordered.HIGHEST_PRECEDENCE)
public class SpringApplicationInitializer implements WebApplicationInitializer {

	/**
	 * The default property filename to use if nothing is specified via context
	 * parameters
	 */
	private static final String DEFAULT_PROPERTY_FILE = "/WEB-INF/framework";

	/**
	 * The default property filename to use if nothing is specified via context
	 * parameters
	 */
	private static final String DEFAULT_TEST_PROPERTY_FILE = "/WEB-INF/test";

	/**
	 * The profile name for code running in a servlet container
	 */
	public static final String SERVER_PROFILE = "server";

	/**
	 * The profile name for code running in a junit context
	 */
	public static final String TEST_PROFILE = "test";

	/**
	 * The <code>LOGGER</code> is a reference to our logger. Initialization is
	 * deferred until the logging system has been brought to life.
	 */
	private static Logger LOGGER; // NOSONAR

	/**
	 * Starts the webapp {@inheritDoc}
	 */
	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {

		WebApplicationContext rootAppContext = createRootApplicationContext(
				servletContext);
		if (rootAppContext != null) {
			servletContext.addListener(new ContextLoaderListener(rootAppContext) {
				@Override
				public void contextInitialized(ServletContextEvent event) {
					// no-op because the application context is already
					// initialized
				}
				
				/** 
				 * {@inheritDoc}
				 */ 
				@Override
				public void contextDestroyed(ServletContextEvent event) {
					super.contextDestroyed(event);
				}
			});
			
			ThreadLocalCleanupFilter.registerSelf(servletContext);
			
			ServletRegistration.Dynamic reg = servletContext.addServlet("jersey", ServletContainer.class);
			reg.setInitParameter("javax.ws.rs.Application", RestfulAPIApplication.class.getName());
			reg.setInitParameter("jersey.config.server.tracing.type", "ON_DEMAND");
			reg.setInitParameter("jersey.config.server.monitoring.statistics.enabled", "false");
			reg.setLoadOnStartup(1);
			reg.addMapping("/api/*");
		} else {
			ConsoleAppLogger.INFO.print("No ContextLoaderListener registered, as "
					+ "createRootApplicationContext() did not "
					+ "return an application context");
		}
	}

	/**
	 * Creates the root web application context.
	 * 
	 * @param servletContext
	 *            the JEE servletContext
	 * @return the newly inflated context
	 */
	protected WebApplicationContext createRootApplicationContext(ServletContext servletContext) {
		ApplicationContext parent = getExistingRootWebApplicationContext(servletContext);
		if (parent != null) {
			ConsoleAppLogger.INFO.print("Root context already created (using as parent).");
			servletContext.setAttribute(
					WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE, null);
		}

		AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
		try {

			ConfigurableEnvironment environment = new StandardServletEnvironment();
			context.setEnvironment(environment);
			context.setParent(parent);
			context.setServletContext(servletContext);

			// constructing one of these triggers sdk initialization
			SdkContextInitializer init = new SdkContextInitializer();

			init.initialize(context);

			LOGGER = LoggingManager.getLogger(SpringApplicationInitializer.class.getPackage().getName());

			context.register(getClass());
			// Refresh/load the context
			context.refresh();

			try {
				context.registerShutdownHook();
			} catch (AccessControlException ex) {
				// Not allowed in some environments.
			}

			return (WebApplicationContext) context;
		} catch (RuntimeException ex) {
			if (LOGGER == null) {
				ConsoleAppLogger.ERROR.print("could not initialize applacation context!");
				ConsoleAppLogger.ERROR.print(ex);
			}

			LOGGER.log(Level.SEVERE, "could not initialize applacation context!", ex);
			context.close();
			throw new IllegalStateException(ex);
		}

	}

	/**
	 * The ContextInitializerAdaptor class
	 * 
	 * @param <C>
	 *            the ConfigurableApplicationContext, supports
	 *            GenericWebApplicationContext or
	 *            AbstractRefreshableWebApplicationContext
	 */
	public static abstract class ContextInitializerAdaptor<C extends ConfigurableApplicationContext> implements ApplicationContextInitializer<C> {

		/**
		 * The <code>LOGGER</code> is a reference to our logger. Initialization
		 * is deferred until the logging system has been brought to life.
		 */
		private static Logger LOGGER; // NOSONAR

		/**
		 * The main sdk properties file to be loaded
		 */
		private String propertyFileName;

		/**
		 * The spring profiles to be activated
		 */
		private String[] activeProfiles;

		/**
		 * Prevents no-arg construction
		 */
		private ContextInitializerAdaptor() {

		}

		/**
		 * Constructs an instance
		 * 
		 * @param propertyFileName
		 *            the main sdk properties file to be loaded
		 * @param activeProfiles
		 *            the spring profiles to be activated
		 */
		public ContextInitializerAdaptor(String propertyFileName, String... activeProfiles) {
			this();
			this.propertyFileName = propertyFileName;
			this.activeProfiles = activeProfiles;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void initialize(C applicationContext) {
			ServletContext servletContext;
			if (applicationContext instanceof GenericWebApplicationContext) {
				servletContext = ((GenericWebApplicationContext) applicationContext).getServletContext();
			} else if (applicationContext instanceof AbstractRefreshableWebApplicationContext) {
				servletContext = ((AbstractRefreshableWebApplicationContext) applicationContext).getServletContext();
			} else {
				throw new IllegalArgumentException(
						"The applicationContext provided must be of type AbstractRefreshableWebApplicationContext or GenericWebApplicationContext");
			}

			initialize(servletContext, applicationContext.getEnvironment());
		}

		/**
		 * Boots up the framework environment properties, app properties, spring
		 * environment, spring profiles, logging framework and bootservice
		 * driver
		 * 
		 * @param servletContext
		 * @param configurableEnvironment
		 */
		protected void initialize(ServletContext servletContext, ConfigurableEnvironment configurableEnvironment) {
			configurableEnvironment.setActiveProfiles(this.activeProfiles);
			// sdk initialization stuff...
			EnvironmentContext.initialize(servletContext.getInitParameter("ENV_ID"));

			try {
				loadApplicationProperties(servletContext, getApplicationContextPath(servletContext));
				configurePropertySources(configurableEnvironment);
				initializePortalLoggingSubsystem();
				
			} catch (APException e) {
				throw new RuntimeException("Failed to bootstrap sdk resources", e);
			}
		}

		/**
		 * Gets the physical location on the file system where the application
		 * is deployed to within the application server container.
		 * 
		 * @param context
		 *            is the current ServletContext instance for the web
		 *            application.
		 * @return the physical location on the file system where the
		 *         application is deployed.
		 * @throws ResourceException
		 *             if the application context path could not be acquired
		 *             from the web container.
		 * @since 5.1 Converted class from a HttpServlet to a
		 *        {@link ServletContextListener} implementation. Changed
		 *        parameter type to {@link ServletContext}
		 */
		protected String getApplicationContextPath(ServletContext context) throws ResourceException {
			String propertyFileDirectory = context.getRealPath("");
			if (BaseValidator.checkIsEmpty(propertyFileDirectory)) {
				try {
					URL url = context.getResource("/");
					propertyFileDirectory = url.getPath();
				} catch (MalformedURLException mfURLEx) {
					throw new ResourceException("Could not acquire the physical application context root path", mfURLEx);
				}
			}
			// See that this is actually a directory on the local file system.
			File contextPath = new File(propertyFileDirectory);
			if (!contextPath.exists()) {
				ConsoleAppLogger.INFO.print("WARNING: Physical application context root path resolved to '" + propertyFileDirectory
						+ "' but is not an accessible path on the local file system.");
			}

			return propertyFileDirectory;
		}

		/**
		 * This method implements the default policy for application properties
		 * loading. Derived forms of this class can implement different
		 * application properties initialization policies by overriding this
		 * method.
		 * 
		 * @param servletContext
		 *            is the {@link ServletContext} instance for this web
		 *            application
		 * @param applicationContextPath
		 *            is the application context directory path
		 * @throws APException
		 *             if the PROPERTY_FILE_NAME application descriptor cannot
		 *             be resolved or the properties file that is configured is
		 *             not present or cannot be accessed.
		 * @since 5.1 Converted class from a HttpServlet to a
		 *        {@link ServletContextListener} implementation. Changed
		 *        parameter type to {@link ServletContext}
		 */
		protected void loadApplicationProperties(ServletContext servletContext, String applicationContextPath)
				throws APException {
			String propertyFileName = servletContext.getInitParameter("PROPERTY_FILE_NAME");
			if (propertyFileName == null) {
				propertyFileName = this.propertyFileName;
			}
			FileResource mainApplicationPropertyFile = new FileResource(applicationContextPath + propertyFileName);
			try {
				AppProperties.loadApplicationPropertiesFile(mainApplicationPropertyFile.getResourceId());
			} catch (Exception ex) {
				ConsoleAppLogger.ERROR.print("error loading appproperties");
				ConsoleAppLogger.ERROR.print(ex);
				throw APException.create(ex);
			}

			ConsoleAppLogger.INFO.print("PROPERTY_FILE_NAME ==> " + mainApplicationPropertyFile.getResourceId());

			ConsoleAppLogger.INFO.print("Setting application context path in APPLICATION Properties Subsystem to:'"
					+ applicationContextPath + "'");
			AppProperties.setContextPath(applicationContextPath);
			AppProperties.loadAdditionalApplicationProperties();
			ConsoleAppLogger.INFO.print("APPLICATION Properties Subsystem booted ... ");

		}

		/**
		 * Add, remove or re-order any {@link PropertySource}s in this
		 * application's environment.
		 * 
		 * @param environment
		 *            this application's environment
		 */
		protected void configurePropertySources(ConfigurableEnvironment environment) {
			MutablePropertySources sources = environment.getPropertySources();

			Properties appProps = AppProperties.getAppProperties().getPrivateProperties();

			sources.addLast(new PropertiesPropertySource("appProperties", appProps));
		}

		/**
		 * Initializes the portals logging subsystem.
		 * 
		 * @throws APException
		 */
		protected void initializePortalLoggingSubsystem() throws APException {
			ConsoleAppLogger.INFO.print();
			ConsoleAppLogger.INFO.print("APPLICATION ROOT LOGGER booting up ... ");
			ConsoleAppLogger.INFO.print();
			ConsoleAppLogger.INFO.flush();

			LoggingManager.initialize();

			ConsoleAppLogger.INFO.print("APPLICATION ROOT LOGGER booted.");
			ConsoleAppLogger.INFO.print();
			ConsoleAppLogger.INFO.flush();

			// now that the logging is set up - start logging from here on
			LOGGER = LoggingManager.getLogger(getClass().getPackage().getName());
			LOGGER.log(Level.INFO, "Web Application Logging subsystem is enabled.");
		}

	}

	/**
	 * The SdkContextInitializer class
	 */
	public static class SdkContextInitializer extends ContextInitializerAdaptor<ConfigurableApplicationContext> {

		/**
		 * Constructs an instance
		 */
		public SdkContextInitializer() {
			super(DEFAULT_PROPERTY_FILE, SERVER_PROFILE);
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void initialize(ConfigurableApplicationContext applicationContext) {
			super.initialize(applicationContext);
		}
	}

	/**
	 * The SdkContextInitializer class
	 */
	public static class SdkTestContextInitializer extends ContextInitializerAdaptor<ConfigurableApplicationContext> {

		/**
		 * Constructs an instance
		 */
		public SdkTestContextInitializer() {
			super(DEFAULT_TEST_PROPERTY_FILE, TEST_PROFILE);
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void initialize(ConfigurableApplicationContext applicationContext) {
			super.initialize(applicationContext);
		}
	}

	/**
	 * Checks for an existing parent context on the serveltContext
	 * 
	 * @param servletContext
	 *            the JEE servlet context
	 * @return the root {@link ApplicationContext} or null
	 */
	private ApplicationContext getExistingRootWebApplicationContext(ServletContext servletContext) {
		Object context = servletContext.getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);
		if (context instanceof ApplicationContext) {
			return (ApplicationContext) context;
		}
		return null;
	}

	/**
	 * Add support for loading application/environment properties via
	 * <code>@Value("${property.name}")</code>
	 * 
	 * @return the {@link PropertySourcesPlaceholderConfigurer} bean
	 */
	@Bean
	public static PropertySourcesPlaceholderConfigurer properties() {
		PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer = new PropertySourcesPlaceholderConfigurer();
		return propertySourcesPlaceholderConfigurer;
	}
}
