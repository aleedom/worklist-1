/*
 * Created on Mar 11, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Repository;

import com.agencyport.security.profile.ISecurityProfile;
import com.agencyport.security.repository.RedisRepository;

/**
 * Register this SpringContextBridge as a Spring Component.
 * SpringContextBridge is the bridge to the "non managed" world.
 */
@Repository
public class SpringContextBridge 
        implements SpringContextBridgedServices, ApplicationContextAware {

    /**
     * We put the application context into a static appContextContainer object so that
     * non-Spring managed Java classes can statically get an instance of this bean.
     */
    private static final AppContextContainer APP_CONTEXT_CONTAINER = new AppContextContainer();
	
    @Autowired
    private RedisRepository<String, ISecurityProfile> redisRepository;
    
	@Autowired
    private HealthMonitor healthMonitor;
	
	/**
	 * Called by Spring for all beans which implement {@link ApplicationContextAware}. 
	 * We put the application context into a static applicationContext container object so that
	 * non-Spring managed Java classes can statically get an instance of this bean.
	 * This looks like it's not thread-safe, but it is, as this bean is a singleton.
	 */
	@Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        APP_CONTEXT_CONTAINER.setContext(applicationContext);
    }
	
    /**
     * Get  Spring ContextBridgedServices
     * @return SpringContextBridgedServices
     */
    public static SpringContextBridgedServices services() {
        return APP_CONTEXT_CONTAINER.getContext().getBean(SpringContextBridgedServices.class);
    }
    

    public HealthMonitor getHealthMonitor(){
        return healthMonitor;
    }
    
    @Override
    public RedisRepository<String, ISecurityProfile> getRedisRepository() {
        return redisRepository;
    }
    
    /**
     * We put the application context into a static appContextContainer object so that
     * non-Spring managed Java classes can statically get an instance of this bean.
     * @author ahayes
     */
    private static class AppContextContainer {
        private ApplicationContext applicationContext;

        public void setContext(ApplicationContext applicationContext) {
            this.applicationContext = applicationContext;
        }
        public ApplicationContext getContext() {
            return applicationContext;
        }
    }
}