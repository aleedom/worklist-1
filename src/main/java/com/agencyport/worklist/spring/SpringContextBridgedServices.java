/*
 * Created on Mar 11, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.spring;

import com.agencyport.security.profile.ISecurityProfile;
import com.agencyport.security.repository.RedisRepository;

/**
 * 
 * The SpringContextBridgedServices class
 */
public interface SpringContextBridgedServices {
	/**
	 * HealthMonitor
	 * @return HealthMonitor
	 */
	public HealthMonitor getHealthMonitor();

	/**
     * RedisRepository
     * @return RedisRepository
     */
    public RedisRepository<String, ISecurityProfile> getRedisRepository();
	
}
