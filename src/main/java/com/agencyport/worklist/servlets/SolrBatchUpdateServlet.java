/*
 * Created on Mar 10, 2016 by ldeane AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.agencyport.logging.ExceptionLogger;
import com.agencyport.servlets.base.APBaseServlet;
import com.agencyport.servlets.base.IBaseConstants;
import com.agencyport.worklist.search.indexing.SolrBatchIndexProcessor;
import com.agencyport.worklist.solr.manager.SolrManager;

/**
 * 
 * The SolrBatchUpdateServlet class is to handle requests for displaying JSP for Batch Update UI and invoking the actual
 * batch updates when a POST comes in.
 */
public class SolrBatchUpdateServlet extends APBaseServlet {
	/**
	 * The <code>INDEX_WORK_ITEM</code> is a function name that is sent to logging several times. 
	 */
	private static final String INDEX_WORK_ITEM = "indexWorkItem";
	/**
	 * Version ID for this servlet
	 */
	private static final long serialVersionUID = -5119532840381798622L;
	
	/** 
	 * {@inheritDoc}
	 * Serve up the jsp.
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		JSONObject options = new JSONObject();
		
		SolrBatchIndexProcessor batchIndexProcessor = SolrBatchIndexProcessor.get();
		/*
		 * Make the list of indexer names available to the JSP.
		 */
		try {
			options.put("indexers", batchIndexProcessor.getIndexerList());
		} catch (JSONException e) {
			ExceptionLogger.log(e, getClass(), "doGet");
		}
		request.setAttribute("options", options.toString());
		
		request.setAttribute(IBaseConstants.NEXT_PAGE, "../solrbatchupdate/solrbatchupdate.jsp");
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(IBaseConstants.SITE_SHELL);
        dispatcher.forward(request, response);
	}
	
	/** 
	 * {@inheritDoc}
	 * Update the appropriate Solr index based on the type that is sent over in the request.
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		/*
		 * Which indexer do we want?
		 */
		String type = request.getParameter("type");
		
		SolrBatchIndexProcessor batchIndexProcessor = SolrBatchIndexProcessor.get();
		/*
		 * The requested indexer is a recognized type.
		 */
		if(batchIndexProcessor.validateIndexType(type)) {
			
			JSONObject output = new JSONObject();
			try {
				batchIndexProcessor.proccessBatchIndex(type);
				response.getWriter().append(SolrManager.generateSuccessJSON().toString());
			} catch (Exception e) {
				ExceptionLogger.log(e, getClass(), INDEX_WORK_ITEM);
				try {
					output.put("error_msg", e.getMessage());
				} catch (JSONException e1) {
					ExceptionLogger.log(e1, getClass(), INDEX_WORK_ITEM);
				}
				response.getWriter().append(output.toString());
			}
		}
		else {
			JSONObject output = new JSONObject();
			try {
				output.put("error_msg", "No indexer paired with label " + type);
				response.getWriter().append(output.toString());
			} catch (JSONException e) {
				ExceptionLogger.log(e, getClass(), INDEX_WORK_ITEM);
			}
		}
	}
}
