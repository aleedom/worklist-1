package com.agencyport.worklist.search.indexing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

import org.apache.solr.common.SolrInputDocument;

import com.agencyport.factory.GenericFactory;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.logging.LoggingManager;
import com.agencyport.shared.APException;
import com.agencyport.utils.AppProperties;
import com.agencyport.utils.PerfObject;
import com.agencyport.utils.PerfObjectCollector;
import com.agencyport.worklist.solr.manager.ISolrManager;
import com.agencyport.worklist.solr.manager.SolrManager;

/**
 * The SolrBatchIndexProcessor is the main entry point for working with the framework's Solr Batch Index framework. It is responsible
 * for initializing and invoking the individual batch indexers for each known collection on behalf of callers.
 * 
 * @since 5.0
 */
public class SolrBatchIndexProcessor {

	/**
	 * The <code>SINGLETON</code> is the single instance of this class as initialized by the {@link #createSolrBatchIndexProcessor()} routine. 
	 */
	private static final SolrBatchIndexProcessor SINGLETON = createSolrBatchIndexProcessor();

	/**
	 * Logger for the SolrBatchUpdateServlet class.
	 */
	private static final Logger LOGGER = LoggingManager.getLogger(SolrBatchIndexProcessor.class.getPackage().getName());
	
	/**
	 * Pairings of <label|class> for the indexers.
	 * 
	 * The label is used for display purposes on the UI.  It gets sent over in POSTs
	 * to help identify which index updater class to trigger.
	 */
	private static Map<String, ISolrBatchIndexer> indexers;
	
	/**
	 * The list of indexers (labels) that gets sent in a GET.
	 */
	private static List<String> indexerList;
	
	/**
	 * When the class gets loaded, we want to parse out the list of labels/indexers from
	 * solr_batch_indexers in framework.properties and set up the indexers and indexerList
	 * data structures so they're ready by the first GET or POST.
	 */
	static {
		indexerList = new ArrayList<String>();
		indexers = new HashMap<String, ISolrBatchIndexer>();
		
		String solrBatchIndexers = AppProperties.getAppProperties().getStringProperty("solr_batch_indexers");
		String[] indexerPieces = solrBatchIndexers.split(";");
		for(int i=0; i<indexerPieces.length; i++) {
			String[] currIndexerPieces = indexerPieces[i].split("\\|");
			
			try {
				indexers.put(currIndexerPieces[0], (ISolrBatchIndexer) GenericFactory.create(currIndexerPieces[1]));
				indexerList.add(currIndexerPieces[0]);
			} catch (APException e) {
				LOGGER.severe(e.getMessage());
				ExceptionLogger.log(e, SolrBatchIndexProcessor.class, "<static initializer>");
			}
		}
	}
	
	/**
	 * Returns the singleton instance of this class. To extend this singleton with your own implementation, use the factory property:<br/>
	 * <code>solr_batch_index_processor</code>
	 * @return the singleton instance
	 */
	public static SolrBatchIndexProcessor get(){
		return SINGLETON;
	}
	
	/**
	 * Factory instantiation of this class. Uses generic factory to load up this class by default or custom subclass
	 * as specified by the <code>solr_batch_index_processor</code> application property.  
	 * @return the instance of this class (or a custom subclass implementation)
	 */
	private static SolrBatchIndexProcessor createSolrBatchIndexProcessor() {
		
		SolrBatchIndexProcessor processor = null;
		try {
			processor = GenericFactory.create("solr_batch_index_processor", SolrBatchIndexProcessor.class.getName());
		}
		catch(APException e) {
			ExceptionLogger.log(e, SolrBatchIndexProcessor.class, "createSolrBatchIndexProcessor");
			throw new IllegalStateException("Unable to instantiate instance of SolrBatchIndexProcessor.", e);
		}
		
		return processor;
	}

	/**
	 * Gets the list of all batch indexers known to the application. This is used by the SolrBatchUpdate servlet to render
	 * the index selection UI. 
	 * @return the list of all batch indexers known to the application
	 */
	public List<String> getIndexerList() {
		return indexerList;
	}
	
	/**
	 * Validates the given <code>indexType</code> against the actual list of known indexer keys. Useful for validating an incoming
	 * type which came from an external client. 
	 * @param indexType the indexer type or indexer "key"
	 * @return true if valid, else false
	 */
	public boolean validateIndexType(String indexType){
		return indexers.containsKey(indexType);
	}

	/**
	 * Engages a full batch index process on the Solr index with the given <code>indexType</code>. This starts by delete all documents
	 * under the given index. It then invokes the applicable {@link ISolrBatchIndexer} which is responsible for providing the list of
	 * new documents to our batch {@link ISolrDocumentIndexer} implementation. This implementation will send off new documents to solr in
	 * batches of 10000 by default (configurable via the ${indexType}.batch_index_size_threshold application property). 
	 * @param indexType
	 * @throws APException
	 */
	public void proccessBatchIndex(final String indexType) throws APException{
		if(indexers.containsKey(indexType)) {
			ISolrBatchIndexer indexer = indexers.get(indexType);
			
			/*
			 * Generate the XML and POST to Solr.
			 */
			final ISolrManager solrManager = SolrManager.getSolrManager();
			final int batchIndexSizeThreshold = AppProperties.getAppProperties().getIntProperty(indexType + ".batch_index_size_threshold", 10000);
			
			//Start by deleting all current documents in the index, if any
			solrManager.deleteAll(indexType);
			
			indexer.provideDocumentsToIndexer(new ISolrDocumentIndexer() {
				
				/**
				 * The <code>documentQueue</code>is the queue of documents to be indexed in batches
				 */
				private final List<SolrInputDocument> documentQueue = new ArrayList<>();
				
				/**
				 * {@inheritDoc}
				 */
				@Override
				public void indexDocument(SolrInputDocument documentToIndex) throws APException {

					documentQueue.add(documentToIndex);
					//once the queue reaches the batch size threshold, we send off the batch to solr
					if(documentQueue.size() == batchIndexSizeThreshold){
						indexBatch();
					}
				}
				
				/**
				 * Sends the current batch of Solr documents in the documentQueue off to the {@link ISolrManager} for indexing 
				 * @throws APException
				 */
				private void indexBatch() throws APException{
					if(!documentQueue.isEmpty()) {
						PerfObject batchPerf = PerfObjectCollector.startNew("SolrBatchIndexProcessor$ISolrDocoumentIndexer.indexBatch() for indexType: " + indexType);
						solrManager.index(documentQueue, indexType);
						documentQueue.clear();
						batchPerf.stop();
					}
				}
				
				/**
				 * {@inheritDoc}
				 */
				@Override
				public void commit() throws APException {
					//index remaining documents in queue
					if(!documentQueue.isEmpty()) {
						
						indexBatch();
					}
					solrManager.commit(indexType);
				}
			});
			
		}
		else {
			throw new IllegalArgumentException(String.format("Invalid indexType value provided. '%s' indexType was not recognized", indexType));
		}
	}
	
	/**
	 * Convenience routine which engages the {@link #proccessBatchIndex(String)} method for all batch indexers known to the application
	 * @throws APException
	 */
	public void processAllBatchIndexers() throws APException {
		
		for (Entry<String, ISolrBatchIndexer> entry:  indexers.entrySet()){
			proccessBatchIndex(entry.getKey());
		}
	}
}
