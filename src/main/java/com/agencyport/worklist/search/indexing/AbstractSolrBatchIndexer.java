/*
 * Created on Feb 2, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.search.indexing;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

import org.apache.solr.common.SolrInputDocument;

import com.agencyport.database.provider.DatabaseResourceAgent;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.logging.LoggingManager;
import com.agencyport.shared.APException;
import com.agencyport.workitem.model.WorkItemType;
import com.agencyport.worklist.searchindex.IDataReader;
import com.agencyport.worklist.searchindex.IndexDataMapper;
import com.agencyport.worklist.searchindex.SearchIndexFactory;

/**
 * The AbstractSolrBatchIndexer class is the base derivation point for all SOLR
 * batch indexers.
 * @since 5.1
 */
public abstract class AbstractSolrBatchIndexer implements ISolrBatchIndexer {
	/**
	 * The <code>LOGGER</code> a copy of our logger.
	 */
	private static final Logger LOGGER = LoggingManager.getLogger(AbstractSolrBatchIndexer.class.getPackage().getName());
	/**
	 * The <code>ONE_HUNDRED</code> constant for the numeric value for 100.
	 */
	private static final int ONE_HUNDRED = 100;
	
	/**
	 * The <code>solrIndex</code> is the name of the SOLR index.
	 */
	private final String solrIndex;
	
	/**
	 * The <code>workItemType</code> is the work item type.
	 */
	private final WorkItemType workItemType;
	/**
	 * Constructs an instance.
	 * @param solrIndex see {@link #solrIndex}
	 * @param workItemType see {@link #workItemType}
	 */
	protected AbstractSolrBatchIndexer(String solrIndex, WorkItemType workItemType) {
		this.solrIndex = solrIndex;
		this.workItemType = workItemType;
	}

	/** 
	 * {@inheritDoc}
	 */

	@Override
	public final void provideDocumentsToIndexer(ISolrDocumentIndexer indexer) throws APException {
		DatabaseResourceAgent dra = new DatabaseResourceAgent(this);
		LOGGER.info(String.format("Beginning gathering of data for reindexing of SOLR '%s' database", solrIndex));
		try {
			dra.getConnection(true);
			IDataReader dataReader = SearchIndexFactory.get().createResultSetDataReader(workItemType);
			IndexDataMapper dataMapper = SearchIndexFactory.get().createIndexDataMapper(solrIndex, dataReader);
			PreparedStatement pstmt = getPreparedStatement(dra);
			ResultSet rs = dra.executeQuery(pstmt);
			int workItemCount = 0;
			while(rs.next()) {
				workItemCount++;
				SolrInputDocument doc = (SolrInputDocument) dataMapper.createIndexDocument(rs);
				indexer.indexDocument(doc);
				if ((workItemCount % ONE_HUNDRED) == 0){
					LOGGER.info(String.format("Gathered data for %d items for reindexing of SOLR '%s' database", workItemCount, solrIndex));
				}
			}
			indexer.commit();
			LOGGER.info(String.format("Finished gathering of data for %d items for reindexing of SOLR '%s' database", workItemCount, solrIndex));
		} catch(SQLException e) {
			ExceptionLogger.log(e, getClass(), "provideDocumentsToIndexer");
			throw new APException(e);
		} finally {
			dra.closeDatabaseResources(this);
		}
	}
	
	
	/**
	 * Returns the prepared statement specific to the data source being indexed.
	 * @param dra is the database resource agent.
	 * @return the prepared statement specific to the data source being indexed. 
	 * @throws SQLException if a JDBC issue was encountered.
	 */
	protected abstract PreparedStatement getPreparedStatement(DatabaseResourceAgent dra) throws SQLException; 
	
}
