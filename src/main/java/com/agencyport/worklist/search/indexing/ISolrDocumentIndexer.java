package com.agencyport.worklist.search.indexing;

import org.apache.solr.common.SolrInputDocument;

import com.agencyport.shared.APException;

/**
 * The ISolrDocumentIndexer provides a generic API for indexing 1 to n documents
 * to Solr and later performing cleanup and commit operations via the
 * {@link #commit()} method.
 */
public interface ISolrDocumentIndexer {

	/**
	 * Adds a new document to the Solr index. Implementations should rely on the
	 * finalize method to perform any commit operations, if necessary. Callers
	 * should call {@link #commit()} once they're finished indexing documents.
	 * Failure to ultimately call commit could leave the index in and invalid
	 * state.finalize
	 * 
	 * @param documentToIndex
	 * @throws APException
	 */
	void indexDocument(SolrInputDocument documentToIndex) throws APException;
	
	/**
	 * Performs any cleanup or commit operations, if necessary. Should be called after all documents have been indexed. 
	 * Failure to do so could leave the index in and invalid state.  
	 * @throws APException
	 */
	void commit() throws APException;
}
