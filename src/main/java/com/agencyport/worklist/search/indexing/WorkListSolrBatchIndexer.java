package com.agencyport.worklist.search.indexing;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.agencyport.database.DatabaseAgent;
import com.agencyport.database.provider.DatabaseResourceAgent;
import com.agencyport.workitem.model.WorkItemType;
import com.agencyport.worklist.searchindex.IndexNames;

/**
 * SolrBatchIndexer responsible for creating Solr XML for batch updates on work items.
 */
public class WorkListSolrBatchIndexer extends AbstractSolrBatchIndexer {
	
	/**
	 * The <code>QUERY_STATEMENT</code> is the select statement that will gather all of the work items to apply to the SOLR index.
	 */
	private static final String QUERY_STATEMENT = DatabaseAgent.databaseNormalizeSQLStatement("select * from ${db_table_prefix}work_item w "
			+ "left outer join ${db_table_prefix}account_member am on w.work_item_id = am.work_item_id "
			+ "left outer join ${db_table_prefix}external_relation er on w.work_item_id = er.internal_id and er.relation_type = 'W'"); 
	
	/**
	 * Default constructor.
	 */
	public WorkListSolrBatchIndexer() {
		super(IndexNames.WORKITEM_SOLR_INDEX_NAME, WorkItemType.REGULAR_LOB_WORK_ITEM_TYPE);
	}
	
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	protected PreparedStatement getPreparedStatement(DatabaseResourceAgent dra) throws SQLException {
		return dra.getPreparedStatement(QUERY_STATEMENT);
	}
	
}
