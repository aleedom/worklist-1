/*
 * Created on Mar 14, 2016 by ldeane AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.search.indexing;

import com.agencyport.workitem.model.WorkItemType;

/**
 * 
 * The ISolrIndexer class is the interface for classes that perform updates to a Solr index.
 */
public interface ISolrIndexer {
	
	/**
	 * Returns the SOLR index map name for this work item type.
	 * @return the SOLR index map name for this work item type.
	 * @since 5.1
	 */
	String getSolrIndexMapName();
	
	/**
	 * Returns the SOLR index for this work item type.
	 * @return the SOLR index for this work item type.
	 */
	String getSolrIndexType();
	
	/**
	 * Returns the work item type for this instance.
	 * @return the work item type for this instance.
	 * @since 4.5
	 */
	WorkItemType getWorkItemType();

}
