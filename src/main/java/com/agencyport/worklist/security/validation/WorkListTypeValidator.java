/*
 * Created on Mar 3, 2016 by ldeane AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.security.validation;

import javax.servlet.http.HttpServletRequest;

import com.agencyport.resource.ResourceException;
import com.agencyport.resource.ResourceProvider;
import com.agencyport.resource.ResourceType;
import com.agencyport.security.validation.IInputValidator;
import com.agencyport.worklist.view.CompiledWorkListView;
import com.agencyport.worklist.view.WorkListViewResourceProvider;

/**
 * The WorkListTypeValidator class validates work list view names.
 * @since 5.1
 */
public class WorkListTypeValidator implements IInputValidator {

	/**
	 * Constructs an instance.
	 */
	public WorkListTypeValidator() {
	}

	/** 
	 * {@inheritDoc}
	 */

	@Override
	public boolean isValid(String parameterName, String parameterValue, HttpServletRequest request) {
		WorkListViewResourceProvider provider = (WorkListViewResourceProvider) ResourceProvider.getResourceProvider(ResourceType.WORK_LIST_VIEW_DEFINITION);
		try {
			CompiledWorkListView compiledSolrWorkListView =  provider.getWorkListViewResource(parameterValue);
			return compiledSolrWorkListView != null;
		} catch (ResourceException resourceException){ //NOSONAR
			return false;
		}
	}

}