/*
 * Created on Mar 14, 2016 by ldeane AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.pojo;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * The Property class models a Property of a Work Item or Account
 */
public class Property {
	
	/**
	 * The <code>name</code> is the work item property name.
	 */
	private String name;
	
	/**
	 * The <code>value</code> is the work item property value.
	 */
	private String value;
	
	/**
	 * The <code>type</code> is the type of the work item property.
	 */
	private String type;

	/**
	 * Returns the work item property name
	 * @return the work item property name
	 */
	@XmlElement(name="name")
	public String getName() {
		return name;
	}

	/**
	 * Sets the work item property name
	 * @param name is the work item property name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the work item property value
	 * @return the work item property value
	 */
	@XmlElement(name="value")
	public String getValue() {
		return value;
	}

	/**
	 * Sets the work item property value
	 * @param value is the work item property value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Returns the type of the work item property
	 * @return the type of the work item property
	 */
	@XmlElement(name="type")
	public String getType() {
		return type;
	}

	/**
	 * Sets the type of the work item property
	 * @param type is the type of the work item property to set
	 */
	public void setType(String type) {
		this.type = type;
	}

}
