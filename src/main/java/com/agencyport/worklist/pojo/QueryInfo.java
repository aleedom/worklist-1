/*
 * Created on Dec 3, 2014 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.pojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

import com.agencyport.api.URIBuilder;
import com.agencyport.api.pojo.Item;
import com.agencyport.worklist.api.AccountListResource;
import com.agencyport.worklist.api.WorkItemListResource;
import com.agencyport.rest.AtomLink;
import com.agencyport.rest.IConstants;
import com.agencyport.rest.ParameterNames;
import com.agencyport.utils.EqualityChecker;
import com.agencyport.utils.HashCodeCalculator;
import com.agencyport.workitem.model.WorkItemType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The QueryInfo class is the POJO class supporting the exchange of query info meta data.
 * @since 5.1
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public final class QueryInfo extends Item implements Cloneable {
	/**
	 * The <code>sortOption</code> are the associated sort options.
	 */
	private List<QueryField> queryFields;
	
	/**
	 * The <code>transactionId</code> is the TDF id of the TDF that contains the page definition for this query information
	 * dialog.
	 */
	private String transactionId;
	
	/**
	 * The <code>pageId</code> is the page of the TDF that contains the page definition for this query information
	 * dialog.
	 */
	private String pageId;
	
	/**
	 * The <code>links</code> contains the links for this query information intance.
	 */
	private List<AtomLink> links = new ArrayList<>();
	
	/**
	 * Constructs an empty instance.
	 */
	public QueryInfo() {
	}
	/**
	 * Constructs an instance.
	 * @param name see {@link #name}
	 * @param title see {@link #title}
	 * @param link is the link to itself (rel=self).
	 */
	public QueryInfo(String name, String title, AtomLink link){
		super(name, title);
		links.add(link);
	}
	
	/**
	 * Constructs an instance with just a name.
	 * @param name is the name.
	 */
	public QueryInfo(String name){
		super(name, null);
	}
	/**
	 * Returns the query fields.
	 * @return the query fields.
	 */
	@XmlElementWrapper(name="queryFields")
	@XmlElements(@XmlElement(name="queryField", type=QueryField.class))
	@JsonProperty("queryFields")
	public List<QueryField> getQueryFields() {
		return queryFields;
	}
	/**
	 * Sets the query fields.
	 * @param queryFields are the query fields to set.
	 */
	public void setQueryFields(List<QueryField> queryFields) {
		this.queryFields = queryFields;
	}
	/**
	 * Builds a QueryInfo instance.
	 * @param name is the name of the sort.
	 * @param title is the title of the sort.
	 * @param workItemType is the work item type.
	 * @param view is the view.
	 * @return a QueryInfo instance. 
	 */
	public static QueryInfo create(String name, String title, WorkItemType workItemType, String view){
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put(IConstants.VIEW_NAME, view);
		Class<?> relatedClass = workItemType.equals(WorkItemType.REGULAR_LOB_WORK_ITEM_TYPE) ? WorkItemListResource.class : AccountListResource.class;  
		AtomLink link = new AtomLink(URIBuilder.createURI(relatedClass, IConstants.QUERY_INFO_PATH, null, queryParams), ParameterNames.create(IConstants.VIEW_NAME));
		QueryInfo queryInfo = new QueryInfo(name, title, link);
		link = new AtomLink("operators", URIBuilder.createURI(relatedClass, IConstants.OPERATORS_INFO_PATH));
		queryInfo.links.add(link);
		return queryInfo;
	}


	/**
	 * Get a queryfield for a given field id
	 * @param fieldId is the fieldid of queryfield to be returned
	 * @return instance of QueryField
	 */
	public QueryField getQueryField(String fieldId){
		for(QueryField field: this.queryFields){
			if(field.getRelatedFieldId().equals(fieldId)){
				return field;
			}
		}
		return null;
	}
	
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public String toString() {
		return super.toString() + ", " + QueryInfo.class.getSimpleName() + 
				" [queryFields = " + queryFields + 
				", transactionId = " + transactionId + 
				", pageId = " + pageId
				+ "]";
	}
	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public boolean equals(Object other){
		if (other instanceof QueryInfo){
			QueryInfo item = (QueryInfo) other;
			return super.equals(other) && EqualityChecker.areEqual(this.queryFields, item.queryFields,
					this.pageId, item.pageId,
					this.transactionId, item.transactionId,
					this.links, item.links);
		} else {
			return false;
		}
	}
	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public int hashCode(){
		return super.hashCode() + HashCodeCalculator.calculate(this.queryFields, this.pageId, this.transactionId, this.links);
	}
	/**
	 * @return the transactionId
	 */
	@XmlAttribute(name="transactionId")
	@JsonProperty("transactionId")
	public String getTransactionId() {
		return transactionId;
	}
	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	/**
	 * @return the pageId
	 */
	@XmlAttribute(name="pageId")	
	@JsonProperty("pageId")
	public String getPageId() {
		return pageId;
	}
	/**
	 * @param pageId the pageId to set
	 */
	public void setPageId(String pageId) {
		this.pageId = pageId;
	}
	/**
	 * Returns the link array.
	 * @return the link array.
	 */
	@XmlElementWrapper(name="links")
	@XmlElements(@XmlElement(name="link", type=AtomLink.class))
	@JsonProperty("links")
	public List<AtomLink> getLinks() {
		return links;
	}
	/**
	 * Sets the link array.
	 * @param links is the link array to set.
	 */
	public void setLinks(List<AtomLink> links) {
		this.links = links;
	}
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public Object clone() throws CloneNotSupportedException {
		QueryInfo copy = (QueryInfo) super.clone();
		copy.links = new ArrayList<>(this.links);
		copy.queryFields = new ArrayList<>(this.queryFields);
		return copy;
	}
	
}
