/*
 * Created on Dec 10, 2014 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.pojo;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Operator class aggregates an op code and its localized title.
 * @since 5.1 
 */
@XmlRootElement
public final class Operator {
	/**
	 * The <code>title</code> is the localized title.
	 */
	private String title;
	/**
	 * The <code>opCode</code> is the operator code.
	 */
	private OpCode opCode;
	/**
	 * Constructs an instance.
	 */
	public Operator() {
	}
	
	/**
	 * Constructs an instance.
	 * @param title is the operator localized title.
	 * @param opCode is the op code.
	 */
	public Operator(String title, OpCode opCode){
		this.title = title;
		this.opCode = opCode;
	}
	/**
	 * @return the title
	 */
	@XmlAttribute(name="title")
	@JsonProperty("title")
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the opCode
	 */
	@XmlAttribute(name="opCode")
	@JsonProperty("opCode")
	public OpCode getOpCode() {
		return opCode;
	}
	/**
	 * @param opCode the opCode to set
	 */
	public void setOpCode(OpCode opCode) {
		this.opCode = opCode;
	}

}
