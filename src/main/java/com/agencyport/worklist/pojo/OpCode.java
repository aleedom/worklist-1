/*
 * Created on Dec 10, 2014 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.pojo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAttribute;

import com.agencyport.utils.EqualityChecker;
import com.agencyport.utils.HashCodeCalculator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The OpCode enumeration lists the various supported operators.
 * @since 5.1 
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public final class OpCode {
	/**
	 * The <code>ALL_OP_CODES</code> is the map of all known op code objects.
	 */
	private static final Map<String, OpCode> ALL_OP_CODES = new HashMap<>();
	
	/**
	 * The <code>EQUALS</code> is the exact equality operator.
	 */
	public static final OpCode EQUALS = new OpCode("EQUALS", false);
	
	/**
	 * The <code>GREATER_THAN</code> is the greater than operator. 
	 */
	public static final OpCode GREATER_THAN = new OpCode("GREATER_THAN", true);
	
	/**
	 * The <code>LESS_THAN</code> is the less than operator.
	 */
	public static final OpCode LESS_THAN = new OpCode("LESS_THAN", true);
	
	/**
	 * The <code>BETWEEN</code> is the between operator. This is the only operator
	 * that requires to comparison operands. 
	 */
	public static final OpCode BETWEEN = new OpCode("BETWEEN", true);
	
	/**
	 * The <code>CONTAINS</code> is the contains operator.
	 */
	public static final OpCode CONTAINS = new OpCode("CONTAINS", false);
	
	/**
	 * The <code>ONE_OF</code> is the one of operator.
	 */
	public static final OpCode ONE_OF = new OpCode("ONE_OF", false);
	
	/**
	 * The <code>NOT_EQUAL</code> is the not equal operator.
	 */
	public static final OpCode NOT_EQUAL = new OpCode("NOT_EQUAL", false);
	
	 /**
	  * <code>needsTwoOperands</code> indicates if the operator needs two operands
	  */
	 private boolean needsTwoOperands;
	 
	 
	 /**
	 * The <code>value</code> is the value of this op code.
	 */
	private String value;
	
	 /**
	  * Constructs an instance.
	  * @param value is the value of this op code.
	  * @param needsTwoOperands is a boolean which indicates if the operator needs two operands
	  */
	 private OpCode(String value, boolean needsTwoOperands) {
		 this.value = value;
		 this.needsTwoOperands = needsTwoOperands;
		 ALL_OP_CODES.put(value, this);
	 }
	 
	/**
	 * Constructs an instance.
	 */
	public OpCode(){
	 }
 
	 /**
	  * Return boolean indicating if the operator needs two operands.
	  * @return boolean indicating if the operator needs two operands.
	  */
	@XmlAttribute(name="needsTwoOperands")
	@JsonProperty("needsTwoOperands")	 
	 public boolean getNeedsTwoOperands() {
	   return needsTwoOperands;
	 }
	 
	/**
	 * Returns the value of this enumeration.
	 * @return the value of this enumeration.
	 */
	@XmlAttribute(name="value")
	@JsonProperty("value")	 
	public String getValue(){
		 return value;
	 }
	
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public int hashCode(){
		return HashCodeCalculator.calculate(value);
	}
	
	
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public boolean equals(Object other){
		if (other == this){
			return true;
		} else if (other instanceof OpCode){
			OpCode otherOpCode = (OpCode) other;
			return EqualityChecker.areEqual(value, otherOpCode.value); 
		} else {
			return false;
		}
	}

	/**
	 * Sets the needsTwoOperands to needsTwoOperands
	 * @param needsTwoOperands the needsTwoOperands to set
	 */
	public void setNeedsTwoOperands(boolean needsTwoOperands) {
		this.needsTwoOperands = needsTwoOperands;
	}

	/**
	 * Sets the value to value
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Return all of the op codes.
	 * @return all of the op codes.
	 */
	public static Iterable<OpCode> values(){
		return Collections.unmodifiableList(new ArrayList<OpCode>(ALL_OP_CODES.values())); 
	}
	
	/**
	 * Returns an op code object for its value.
	 * @param opCodeValue is the op code value.
	 * @return the op code object for a given value. Throws an IllegalStateException if not found.
	 */
	public static OpCode valueOf(String opCodeValue){
		OpCode opCode = ALL_OP_CODES.get(opCodeValue);
		if (opCode != null){
			return opCode;
		} else {
			throw new IllegalArgumentException(String.format("Unknown op code value of %s", opCodeValue));
		}
	}

}
