/*
 * Created on Jan 2, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.pojo;

/**
 * The ListType enumeration provides all of the allowable types of lists.
 * @since 5.1
 */
public enum ListType {
	/**
	 * The <code>CARD</code> is the card type view.
	 */
	CARD,
	/**
	 * The <code>CUSTOM</code> provisions for an application defined view.
	 */
	CUSTOM,
	/**
	 * The <code>TABULAR</code> is the tabular type view.
	 */
	TABULAR
}
