/*
 * Created on Dec 8, 2014 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.pojo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

import com.agencyport.api.pojo.DataType;
import com.agencyport.api.pojo.IKey;
import com.agencyport.utils.EqualityChecker;
import com.agencyport.utils.HashCodeCalculator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The QueryField class models a searchable field.
 * @since 5.1 
 */
@XmlRootElement
public final class QueryField implements Cloneable, IKey {
	/**
	 * The <code>relatedFieldId</code> is the related field id.
	 */
	private String relatedFieldId;
	
	/**
	 * The <code>title</code> is the title for the item.
	 */
	private String title;
	/**
	 * The <code>selected</code> determines whether this query field is selected or not.
	 */
	private boolean selected = false;
	
	/**
	 * The <code>opCode</code> is the op code to apply.
	 */
	private OpCode opCode;
	
	/**
	 * The <code>operands</code> are the operands. Typically the only operators that require more than
	 * one operand are the between and one-of operators.
	 */
	private List<String> operands = new ArrayList<>();
	
	/**
	 * The <code>dataType</code> is the data type for this query field.
	 */
	private DataType dataType;
	
	/**
	 * The <code>interactive</code> is a flag which determines whether or not this query field interfaces with the user.
	 */
	private boolean interactive = true;
	
	/**
	 * The <code>pageFieldId</code> is the unique id of the field element in the supporting TDF.
	 */
	private String pageFieldId;

	/**
	 * The <code>isSaveable</code> indicates if the query field can be saved by save search operation
	 */
	private boolean isSaveable;
	
	/**
	 * Constructs an instance.
	 */
	public QueryField() {
	}

	/**
	 * Constructs an instance.
	 * @param relatedFieldId see {@link #relatedFieldId}
	 * @param title see {@link #title}
	 * @param dataType see {@link #dataType}
	 */
	public QueryField(String relatedFieldId, String title, DataType dataType) {
		this.dataType = dataType;
		this.relatedFieldId = relatedFieldId;
		this.title = title;
	}
	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public boolean equals(Object other){
		if (other instanceof QueryField){
			QueryField item = (QueryField) other;
			return EqualityChecker.areEqual(this.relatedFieldId, item.relatedFieldId,
					this.title, item.title,
					this.dataType, item.dataType, 
					this.interactive, item.interactive, 
					this.pageFieldId, item.pageFieldId,
					this.isSaveable, item.isSaveable);
		} else {
			return false;
		}
	}
	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public int hashCode(){
		return HashCodeCalculator.calculate(this.relatedFieldId, 
				this.title, 
				this.dataType, 
				this.interactive, 
				this.pageFieldId,
				this.isSaveable);
	}
	/**
	 * Returns whether this item is selected or not.
	 * @return true if this item is selected.
	 */
	@XmlAttribute(name="selected")
	@JsonProperty("selected")
	public boolean getSelected(){
		return selected;
	}
	
	/**
	 * Sets the selected flag.
	 * @param selected is the selected flag to set.
	 */
	public void setSelected(boolean selected){
		this.selected = selected;
	}
	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public String toString() {
		return super.toString() + ", " + QueryField.class.getSimpleName() + 
				" [selected=" + selected +
				", opcode=" + opCode + 
				", operands=" + operands + 
				", dataType=" + dataType + 
				", interactive=" + interactive + 
				", pageFieldId=" + pageFieldId +
				", isSaveable=" + isSaveable +
				"]";
	}

	/**
	 * Returns the operands.
	 * @return the operands.
	 */
	@XmlElementWrapper(name="operands")
	@XmlElements(@XmlElement(name="operand", type=String.class))
	@JsonProperty("operands")
	public List<String> getOperands() {
		return operands;
	}
	/**
	 * Sets the operands.
	 * @param operands are the operands to set.
	 */
	public void setOperands(List<String> operands) {
		this.operands = operands;
	}

	/**
	 * @return the type
	 */
	@XmlAttribute(name="dataType")
	@JsonProperty("dataType")
	public DataType getDataType() {
		return dataType;
	}

	/**
	 * @param dataType the type to set
	 */
	public void setDataType(DataType dataType) {
		this.dataType = dataType;
	}

	/**
	 * @return the opCode
	 */
	@XmlElement(name="opCode")
	@JsonProperty("opCode")
	public OpCode getOpCode() {
		return opCode;
	}

	/**
	 * @param opCode the opCode to set
	 */
	public void setOpCode(OpCode opCode) {
		this.opCode = opCode;
	}

	/**
	 * @return the interactive
	 */
	@XmlAttribute(name="interactive")
	@JsonProperty("interactive")
	public boolean isInteractive() {
		return interactive;
	}

	/**
	 * @param interactive the interactive to set
	 */
	public void setInteractive(boolean interactive) {
		this.interactive = interactive;
	}

	/**
	 * @return the pageFieldId
	 */
	@XmlAttribute(name="pageFieldId")
	@JsonProperty("pageFieldId")
	public String getPageFieldId() {
		return pageFieldId;
	}

	/**
	 * @param pageFieldId the pageFieldId to set
	 */
	public void setPageFieldId(String pageFieldId) {
		this.pageFieldId = pageFieldId;
	}
	
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public Object clone() throws CloneNotSupportedException {
		QueryField copy = (QueryField) super.clone();
		copy.setOperands(new ArrayList<>(copy.getOperands()));
		return copy;
	}

	/**
	 * @return the relatedFieldId
	 */
	@XmlAttribute(name="relatedFieldId")
	@JsonProperty("relatedFieldId")
	public String getRelatedFieldId() {
		return relatedFieldId;
	}

	/**
	 * @param relatedFieldId the relatedFieldId to set
	 */
	public void setRelatedFieldId(String relatedFieldId) {
		this.relatedFieldId = relatedFieldId;
	}

	/**
	 * @return the title
	 */
	@XmlAttribute(name="title")
	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return isSaveable query field boolean indicator
	 */
	@XmlAttribute(name="isSaveable")
	@JsonProperty("isSaveable")
	public boolean isSaveable() {
		return isSaveable;
	}

	/**
	 * Set the can be saved boolean indicator
	 * @param isSaveable is the flag indicating if the query field can be saved.
	 */
	public void setSaveable(boolean isSaveable) {
		this.isSaveable = isSaveable;
	}

	@Override
	public String getKey() {
		return relatedFieldId;
	}

}
