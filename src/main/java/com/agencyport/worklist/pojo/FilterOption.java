/*
 * Created on Dec 2, 2014 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.pojo;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import com.agencyport.api.pojo.IKey;
import com.agencyport.utils.EqualityChecker;
import com.agencyport.utils.HashCodeCalculator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The FilterOption class is the POJO class supporting the exchange of filter option meta data. A filter option has an implicit equality
 * operator such as (lob=BOP+OR+lob=AUTOB) 
 * @since 5.1
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public final class FilterOption implements IKey {
	/**
	 * The <code>title</code> is the title for the filter option.
	 */
	private String title;
	
	/**
	 * The <code>value</code> is the value for the filter option.
	 */
	private String value;
	/**
	 * The <code>selected</code> determines whether this filter option is selected or not.
	 */
	private boolean selected = false;
	/**
	 * Constructs an instance.
	 */
	public FilterOption() {
	}
	/**
	 * Constructs an instance.
	 * @param value see {@link #value}
	 * @param title see {@link #title}
	 */
	public FilterOption(String value, String title){
		this.value = value;
		this.title = title;
	}
	/**
	 * Builds a FilterOption instance.
	 * @param value is the value of the filter option.
	 * @param title is the title of the filter option.
	 * @return a FilterOption instance. 
	 */
	public static FilterOption create(String value, String title){
		return new FilterOption(value, title);
	}
	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public boolean equals(Object other){
		if (other instanceof FilterOption){
			FilterOption option = (FilterOption) other;
			return EqualityChecker.areEqual(this.value, option.value,
					this.title, option.title);
					
		} else {
			return false;
		}
	}
	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public int hashCode(){
		return HashCodeCalculator.calculate(this.value, this.title);
	}
	
	/**
	 * Returns whether this item is selected or not.
	 * @return true if this item is selected.
	 */
	@XmlAttribute(name="selected")
	@JsonProperty("selected")
	public boolean getSelected(){
		return selected;
	}
	
	/**
	 * Sets the selected flag.
	 * @param selected is the selected flag to set.
	 */
	public void setSelected(boolean selected){
		this.selected = selected;
	}
	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public String toString() {
		return FilterOption.class.getSimpleName() + " [selected=" + selected +
				", value=" + value + 
				", title=" + title +
				"]";
	}
	/**
	 * Gets the title for this filter.
	 * @return the title
	 */
	@XmlAttribute(name="title")
	@JsonProperty("title")
	public String getTitle() {
		return title;
	}
	/**
	 * Sets the title for this item.
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the value
	 */
	@XmlAttribute(name="value")
	@JsonProperty("value")
	public String getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	@Override
	public String getKey() {
		return value;
	}

}
