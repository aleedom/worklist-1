/*
 * Created on Dec 2, 2014 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.pojo;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import com.agencyport.api.pojo.IKey;
import com.agencyport.utils.EqualityChecker;
import com.agencyport.utils.HashCodeCalculator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The SortOption class is the POJO class supporting the exchange of sort option meta data.
 * @since 5.1
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public final class SortOption implements IKey {
	/**
	 * The <code>relatedFieldId</code> is the related field id.
	 */
	private String relatedFieldId;
	
	/**
	 * The <code>title</code> is the title for the item.
	 */
	private String title;
	/**
	 * The <code>ascending</code> determines whether the sort order is ascending or descending. If this
	 * flag is true then ascending sort order is implied. If false then a descending sort order is implied.
	 * It is defaulted to false.
	 */
	private boolean ascending = false;
	
	/**
	 * The <code>selected</code> determines whether this sort option is selected or not.
	 */
	private boolean selected = false;
	
	/**
	 * The <code>isSaveable</code> indicates if this sort option can be saved by save search operation
	 */
	private boolean isSaveable;

	/**
	 * Constructs an instance.
	 */
	public SortOption() {
	}
	/**
	 * Constructs an instance.
	 * @param relatedFieldId see {@link #relatedFieldId}
	 * @param title see {@link #title}
	 */
	public SortOption(String relatedFieldId, String title){
		this.relatedFieldId = relatedFieldId;
		this.title = title;
	}
	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public String toString() {
		return SortOption.class.getSimpleName() + " [ascending sort order=" + ascending + 
				", relatedFieldId=" + relatedFieldId + 
				", title=" + title + ", isSaveable=" + isSaveable + 
				", selected=" + selected + 
				"]";
	}
	/**
	 * @return the ascending
	 */
	@XmlAttribute(name="ascending")
	@JsonProperty("ascending")
	public boolean getAscending() {
		return ascending;
	}
	/**
	 * @param ascending the ascending to set
	 */
	public void setAscending(boolean ascending) {
		this.ascending = ascending;
	}
	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public boolean equals(Object other){
		if (other instanceof SortOption){
			SortOption sortOption = (SortOption) other;
			return EqualityChecker.areEqual(this.relatedFieldId, sortOption.relatedFieldId,
					this.title, sortOption.title,
					this.isSaveable, sortOption.isSaveable);
		} else {
			return false;
		}
	}
	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public int hashCode(){
		return HashCodeCalculator.calculate(this.relatedFieldId, this.title, this.isSaveable);
	}
	/**
	 * Returns whether this item is selected or not.
	 * @return true if this item is selected.
	 */
	@XmlAttribute(name="selected")
	@JsonProperty("selected")
	public boolean getSelected(){
		return selected;
	}
	
	/**
	 * Sets the selected flag.
	 * @param selected is the selected flag to set.
	 */
	public void setSelected(boolean selected){
		this.selected = selected;
	}
	@Override
	public String getKey() {
		return this.relatedFieldId;
	}
	/**
	 * @return the relatedFieldId
	 */
	@XmlAttribute(name="relatedFieldId")
	@JsonProperty("relatedFieldId")
	public String getRelatedFieldId() {
		return relatedFieldId;
	}
	/**
	 * @param relatedFieldId the relatedFieldId to set
	 */
	public void setRelatedFieldId(String relatedFieldId) {
		this.relatedFieldId = relatedFieldId;
	}
	/**
	 * @return the title
	 */
	@XmlAttribute(name="title")
	@JsonProperty("title")
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return isSaveable sort option boolean indicator
	 */
	@XmlAttribute(name="isSaveable")
	@JsonProperty("isSaveable")
	public boolean isSaveable() {
		return isSaveable;
	}

	/**
	 * Set the can be saved boolean indicator
	 * @param isSaveable is the flag indicating if this sort option can be saved.
	 */
	public void setSaveable(boolean isSaveable) {
		this.isSaveable = isSaveable;
	}
}
