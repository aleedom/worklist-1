APPLICATION_NAME=worklist
#	General Syntax:
#	The backslash \ is the line continuation character.
#		When the property supports multiple entries, the separator is the semi-colon ;
#
#	@since apbase 3.6 supports property_name+=value where value is concatenated to
#		to an existing value of that name. See Squish #17298 for more details
#	@since apwebapp 3.6 references to the SDK's html_element_definitions.txt and
#			DTR schema (transactionDefinitionBehavior.xsd) are no longer needed in
#			the application's properties file since they are loaded as resources
#			using the Java class loader.
#
#
# Example on how to override one of the product's lightbox jsps.
# Uncomment out the next line and the confirm_exit.jsp in the application context
# will be used instead of the SDK's
#lightbox_jsp_list=lightboxes/custom/confirm_exit.jsp

# Bootstrap configuration
# BOOT SERVICE PROVIDERS
# Specify the classes that implement the IBootServiceIntf
# interface.  These classes are classes that do some type
# of application boot strapping work and need to be loaded
# before the application can begin accepting requests.
# A semi-colon ; delimited list of classes
boot_service_providers=com.agencyport.worklist.bootservice.BootServiceProvider

# The following flag will force all XML product resources
# to undergo XSD validation at bootstrap time or any other time they are
# parsed afresh from the file system.
bootup_validate_schemas=true
# Link to other properties - LOB and otherwise
resources_root=${my_context_path}WEB-INF/
additional_properties_to_load=${resources_root}acsi.properties;\
${resources_root}localization.properties;\
(asResource)properties/apdashboard.properties;\
${resources_root}cache.properties;\
${resources_root}version.properties
# REST API resource class registration.
application.rest_resource_classes=com.agencyport.security.filter.SecurityRequestFilter;\
com.agencyport.worklist.api.WorkItemListResource;\
com.agencyport.worklist.api.AccountListResource;\
com.agencyport.worklist.lookup.api.LookupResource;\
com.agencyport.worklist.api.WorkItemSolrIndexResource;\
com.agencyport.worklist.api.HeartbeatFunction;\
com.agencyport.worklist.api.AccountSolrIndexResource



# APDataCollection parameters
default_xml_pretty_format=true

# The following property instructs the SDK
# to use a different algorithm for id attribute
# values for those attribute values itself generates
# @see com.agencyport.domXML.APDataCollection#generateUID()
# @see com.agencyport.domXML.visitor.IdAttributeUpdater.getNextUID(APDataCollection)
# The following values are recognized:
#		1) docuid - uses an algorithm which significantly shortens the id attribute value
#				ensuring that the value is unique only within the scope of the XML document
#		2) guid (default if missing) - uses the traditional RandomGUID globally unique identifier
#			algorithm
system_generated_id_attribute_type=docuid

# Need this to generate modinfo's
should_retain_id_attributes=true

# Environment Config

my_context_path=${context_path}/

# DATABASE
#
# The JNDI name needs to be set up on your application server and
# match the datasource property value below.
# See your application server documentation to setup the JNDI connection.

#db_table_prefix
# will prefix tables in sql statements. This is used when the JNDI connection
# user id is different than the owner of the tables.  Value must have end with a dot ('.').
# Example:
#	db_table_prefix=AGENCYPORTAL.
db_table_prefix=

#datasource_prefix
# is used to prefix the JNDI connection name (datasource) with
# the required prefix.  Some application server require a prefix to be
# provided as part of the JNDI name.  Below some examples are provided
# for some of the application servers.
# examples:
#    Tomcat,Websphere ( if 'indirect' JNDI lookup type is used)
#     	datasource_prefix=java:comp/env/
#
#    Jboss
#       datasource_prefix=java:/
#
# this property can be left blank or the reference can be removed from the datasource property
datasource_prefix=java:comp/env/

#datasource
# is the name of the JNDI connection setup on your application server.
# this also includes a prefix if necessary.
# Note:  the datasource_prefix is not part of the JNDI name setup in your application server
datasource=${datasource_prefix}agencyportal

#database_agent_class_name
# The database_agent_class_name specifies the SDK database agent class to use for the application.
#		SQL Server support using sequences
#			database_agent_class_name=com.agencyport.database.SQLDatabaseAgent
#		SQL Server legacy 2008 support
#			database_agent_class_name=com.agencyport.database.LegacySQLDatabaseAgent
#
#		Oracle support
#			database_agent_class_name=com.agencyport.database.OracleDatabaseAgent
#
#		MySQL support
#			database_agent_class_name=com.agencyport.database.MySQLDatabaseAgent
#
#		DB2 support using CLOB
#			database_agent_class_name=com.agencyport.database.DB2DatabaseAgent
#		DB2 support using XML (recommended)
#			database_agent_class_name=com.agencyport.database.DB2XMLDatabaseAgent
database_agent_class_name=com.agencyport.database.MySQLDatabaseAgent



# Logging configuration
defer_logging_initialization=false

# Logging destination
# output_log_dir is used by the logging.properties file
output_log_dir=${catalina.base}/logs/${APPLICATION_NAME}

# application.logging_config_file contains logging configuration which
# is at application scope. Do not use JDK level logging configuration
# as it doesn't bode well in a shared JVM space with other applications
# that use JDK logging.
application.logging_config_file=${my_context_path}WEB-INF/application.logging.properties
logging_config_file_monitor=true
application.logger_name_prefix=${APPLICATION_NAME}

application.account.indexsearch.provider=com.agencyport.account.search.SolrIndexSearchProvider
application.account.indexsearch.provider.baseurl=${base_solr_url}/account
#application.account_management_exit_next_page=/DisplayWorkInProgress?action=Open&WorkListType=AccountsView&WORKITEMID=${ACCOUNTID}

################################################################################
# Search and filter settings
################################################################################
base_solr_app=http://localhost:8983
base_solr_url=${base_solr_app}/solr
solr_token_valid_lifespan_in_seconds=5

# The base URL for Solr queries on the worklist.
application.worklist.indexsearch.provider.baseurl=${base_solr_url}/worklist


################################################################################
# Solr settings
################################################################################

# The implementation of ISolrManager that is responsible for doing
# any preprocessing of Solr requests (i.e. security) prior to forwarding the
# request on to Solr.
solr_manager=com.agencyport.worklist.solr.manager.SolrManager

# The implementation of ISolrSecurityManager that is responsible for security-related
# tasks such as generating the Solr security token.
solr_security_manager=com.agencyport.worklist.solr.security.SolrSecurityManager

# The implementation of ISolrUrlResolver that is responsible for determining the
# base url of a Solr instance based on the index type provided.
solr_url_resolver=com.agencyport.worklist.solr.manager.SolrUrlResolver

#Class used to intercept outbound HTTP requests to Solr before the leave the portal (to add security tokens and other request headers)
solr_http_request_interceptor=com.agencyport.worklist.solr.security.APSolrHttpRequestInterceptor

#Used to register all batch indexers to the application. Format is "<index_name>|<batch_indexer_classname>" (tokenized)
solr_batch_indexers=worklist|com.agencyport.worklist.search.indexing.WorkListSolrBatchIndexer;account|com.agencyport.worklist.search.indexing.AccountSolrBatchIndexer

#Tells the application where the SOLR_HOME directory is located on the filesystem.
solr_home=${my_context_path}WEB-INF/SOLR_HOME

################################################################################
#SolrCloud Settings - each node in the cluster needs these properties 
#
# Each of the properties below can also be specified via jvm args (system properties)
# 	- JVM arguments/system properties have a higher order of precednce than the 
#	  settings below
#	- The node URL settings will be read from SOLR_HOME/solr.xml if the setting was 
#	  not expressed as a system properties or an application property
# Note: These properties are only relevant when using the embedded solr engine
################################################################################

#this is a comma separated list of the zookeeper server host_name:clientPort which is ultimately redirected through to the -DzkHost sysprop 
#e.g:	solr_cloud.zookeeperHosts=zk-server1:2181,zk-server2:2181,zk-server3:2181
#alternatively configured by -DzkHost
solr_cloud.zookeeperHosts=

#The following SolrCloud URL settings are not to be confused with the base_solr_url property URL specified above. The base_solr_url property
#is a load balanced SOLR URL for portal to send search and index requests to. The info specified in the following three SolrCloud node
#URL properties refers to a specific node in the SolrCloud cluster. This node URL is sent to zookeeper and distributed to every other 
#node in the shard so that nodes in the shard can communicate with each other directly  

#alternatively configured by -Dhost or in SOLR_HOME/solr.xml
#solr_cloud.node.host=

#alternatively configured by -Dportal.port or in SOLR_HOME/solr.xml
#solr_cloud.node.hostPort=

#alternatively configured by -DhostContext or in SOLR_HOME/solr.xml
#solr_cloud.node.hostContext=${APPLICATION_NAME}/solr

################################################################################
#Solr Security Settings - properties for securing the embedded solr engine
################################################################################

#the name of the keystore file which contains a key named "internal_solr_key"
solr_keystore_filename=(asResource)keystore/solr.key

#the name of the SecurePasswordFile which contains the password to the keystore file
solr_keystore_password_filename=(asResource)keystore/solr-info.bin





################################################################################
#TEMPORARY UNTIL PRODUCT DEFINITION SERVICE CREATED AND INTEGRATED
################################################################################

application.product_types_filter=WORK

application.workitem_status_filter=NOTTAKEN;SUBMIT;DECLINE;QUOTED;INPROGRESS

my_portal_app_protocol=http
my_portal_app_domain=localhost
my_portal_app_port=8080
my_portal_app=${my_portal_app_protocol}://${my_portal_app_domain}:${my_portal_app_port}/${APPLICATION_NAME}
my_portal_app_frontservlet=${my_portal_app}/FrontServlet

config_svc_url=http://localhost:8080/configuration/api
indexmapping_resource=${config_svc_url}/indexmapping
worklist_view_resource=${config_svc_url}/worklistview

workerscomp_svc_url=http://localhost:8081/workerscomp/api
workflow_svc_url=${workerscomp_svc_url}/workflow

PERFORMANCE_RESULTS_FILENAME=${my_context_path}/perf.log