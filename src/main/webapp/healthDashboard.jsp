<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="com.agencyport.worklist.spring.SpringContextBridge"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
    href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link href="CSS/bootstrap.min.css" rel="stylesheet">
<link href="CSS/style.css" rel="stylesheet">
<script type="text/javascript"
    src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<title>Dashboard</title>
</head>
<body>

    <h1 class="page-header">Dashboard</h1>

    <div class="col-sm-6">
        <h2 class="sub-header">General Information</h2>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Value</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Service</td>
                        <td><%=SpringContextBridge.services().getHealthMonitor().getService()%></td>
                    </tr>
                    <tr>
                        <td>Tomcat Location</td>
                        <td><%=SpringContextBridge.services().getHealthMonitor().getTomcatLocation()%></td>
                    </tr>
                    <tr>
                        <td>Environment Resources Prefixes</td>
                        <td><%=SpringContextBridge.services().getHealthMonitor().getEnvResProvider()%></td>
                    </tr>
                    <tr>
                        <td>Property Files</td>
                        <td><ol>
                                <%
                                    for (String filename:  SpringContextBridge.services().getHealthMonitor().getPropertyFiles()){
                                        %>
                                        <li><%=filename %></li>
                                        <%
                                    }
                                %>
                            </ol></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-sm-6">
        <h2 class="sub-header">MySQL Information</h2>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Value</th>
                    </tr>
                </thead>
                <tbody>


                    <tr>
                        <td>Schema</td>
                        <td><%=SpringContextBridge.services().getHealthMonitor().getMySqlSchema()%></td>
                    </tr>
                    <tr>
                        <td>User</td>
                        <td><%=SpringContextBridge.services().getHealthMonitor().getMySqlUsername()%></td>
                    </tr>
                    <tr>
                        <td>URL</td>
                        <td><%=SpringContextBridge.services().getHealthMonitor().getMySqlUrl()%></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-sm-6">
        <h2 class="sub-header">Redis Information</h2>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Value</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Server</td>
                        <td><%=SpringContextBridge.services().getHealthMonitor().getRedisServer()%></td>
                    </tr>
                    <tr>
                        <td>Port</td>
                        <td><%=SpringContextBridge.services().getHealthMonitor().getRedisPort()%></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>



    <!-- Bootstrap Core JavaScript  -->
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript"
        src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript"
        src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

</body>
</html>