module.exports = (function() {
'use strict';

	angular.module('worklist')

	.service('CreateQuoteSrv', ['apiGatewayService', function(apiGatewayService){

		var self = this;

		self.createQuoteSubmission = function(successCallBack, errorCallBack){
			 return apiGatewayService.post('workerscomp/workflow/createQuoteSubmission', null, {
         params: {
           /* hardcoding all of these for now */
           'channel':'AGENT',
           'lob':'WORK',
           'tranid':'new_business',
           'ACCOUNTID':'1001'
         }
			 }).then(successCallBack, errorCallBack);
		};
	}])


})();
