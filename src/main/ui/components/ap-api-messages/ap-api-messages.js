module.exports = (function() {

  angular.module('app.shared')

  /*
   * This directive listens to error messages broadcasted by the http interceptor.
   * Uses messages.tpl.jsp template to display the error messages.
   * The messages are removed after 5 secs by default with an exception of security error.
   * An error is considered security error if error code is one of the error codes defined in app.shared (see site_shell.jsp)
   * When the security error is detected, the session is usually invalidated and hence "Click here to Continue" button is present
   * which will take the use to the Login page.
   * This directive can be engaged using <ANY messages /> or <messages />
   */
  .directive('apApiMessages',	[ '$timeout', '$log', 'securityErrorCodes', function($timeout, $log, securityErrorCodes) {
    // ap.consoleInfo('messages directive!!!');
    // TODO: remove console log
    // console.log('messages directive!!');
    return {
      restrict : 'E',
      link : function(scope, element, attrs) {
        scope.messages = [];
        scope.hasSecurityError = false;

        scope.$on('api.messages',function(event,eventData){
          var msg = eventData.data.response.message;
          scope.messages.splice(0, scope.messages.length);
          scope.messages.push(msg);
          console.log(securityErrorCodes);
          if(securityErrorCodes.indexOf(eventData.data.response.status) < 0 ){
            var index = scope.messages.indexOf(msg);
            $timeout(function(){
              scope.messages.splice(index,1);
            },5000);
          }else{
            scope.hasSecurityError = true;
          }
        });
      },
      template: require('raw!./ap-api-messages.html')
    };
  } ])
})();
