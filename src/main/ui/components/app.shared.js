module.exports = (function() {
    'use strict';

    var statuses = [];
    statuses.push(0); //Success
    statuses.push(-100); //REQUEST_VERIFICATION_FAILURE
    statuses.push(-101); //AUTHENTICATION_FAILURE
    statuses.push(-102); //AUTHORIZATION_FAILURE
    statuses.push(-103); //OTHER_SECURITY_FAILURE
    statuses.push(-104); //PRODUCT_DEFINITION_ISSUE
    statuses.push(-105); //WORKITEM_ISSUE
    statuses.push(-106); //WORKITEM_VALIDATION_ERRORS
    statuses.push(-107); //INVALID_VIEW_TYPE
    statuses.push(-108); //WORKITEM_ASSISTANT_ISSUE
    statuses.push(-109); //DASHBOARD_ISSUE
    statuses.push(-110); //DASHBOARD_NO_DATA
    statuses.push(-111); //MOVE_WORKITEM_ISSUE
    statuses.push(-112); //FILTER_ACCESS_ISSUE
    statuses.push(-113); //SORT_INFO_ACCESS_ISSUE
    statuses.push(-114); //QUERY_INFO_ACCESS_ISSUE
    statuses.push(-115); //VIEW_ACCESS_ISSUE
    statuses.push(-116); //QUERY_ISSUE
    statuses.push(-117); //SAVED_SEARCHES_ACCESS_ISSUE
    statuses.push(-118); //LOOKUP_ACCESS_ISSUE
    statuses.push(-119); //FILE_SIZE_ISSUE
    statuses.push(-120); //PAGE_MODEL_ACCESS_ISSUE
    statuses.push(-121); //WORK_FLOW_ISSUE

    angular.module('app.shared', ['base64'])
        .constant('contextPath', 'api-gateway/api/worklist')
        .constant('dateFormat', 'dd/MM/yyyy')
        .constant('securityErrorCodes', statuses)


    /*
     * Register a http interceptor that will handle error at a global level for all http calls
     * Intercepts the response when http error occurs (status code between 200 and 299 is considered a success status)
     * Broadcasts (with name 'ap.messages') the error response for whoever is listening
     */
    .config(['$provide', '$httpProvider', '$base64', '$windowProvider', function($provide, $httpProvider, $base64, $windowProvider) {
        $httpProvider.interceptors
            .push(function($q, $rootScope) {
                return {
                    'responseError': function(response) {
                        if (response.status != 404) {
                            $rootScope.$broadcast('api.messages', response);
                        }
                        return $q.reject(response);
                    }
                };
            });
    }])
    require('./ap-api-messages/ap-api-messages.js');

})();
