module.exports = (function() {

'use strict';

/*
 * Component for presenting the options to create a new quote via the UI.
 *
 * Example
		<ap-create-quote
			type="{{dataCtrl.workListModel.metaData.index}}"
			name="dataCtrl.workListModel.metaData.name"
			when-upload="dataCtrl.initiateUpload()"
			create-work-item="dataCtrl.createWorkItem(href)">
		</div>
 *
 */
angular.module('worklist')

	.component('apCreateQuote',	{
		controller : ['CreateQuoteSrv', CreateQuoteController],
		template : require('raw!./ap-create-quote.html'),
		require: {
			apWorklist:"^apWorklist"
		}
	});

	function CreateQuoteController(CreateQuoteSrv) {
		var self = this;

		self.createWorkItem = function(){
			/*
				Maybe do a please wait model here...
				var plsWaitModal = $modal.open({
					templateUrl: 'worklist/partials/modals/pleasewait.mdl.jsp',
					keyboard: false,
					backdrop: 'static'
				});
			*/
			CreateQuoteSrv.createQuoteSubmission(function(res){
				var workItemId = res.data.response.results.workItem.workitemId.value;
				console.log('craeted new worktem with id: ' + workItemId);
				self.apWorklist.navigateToWorkItem(workItemId);
			});
		};

		self.initiateUpload = function(){
			//TODO
			//var accountId = WorkListViewSrv.getParameterByName('WORKITEMID');
			//ap.turnstileWidget.launchUploadWidget(accountId);
		};
	}

})();
