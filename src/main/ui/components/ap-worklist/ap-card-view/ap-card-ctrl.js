module.exports = (function() {

  'use strict';

  angular.module('worklist')

  .controller('ap.worklist.cardCtrl', CardController)

  function CardController ($log, WorkItemActionsSrv, WorkListViewSrv){
    this.actionsOpen = false;
    this.isWorkItemSelected = function(){
      if( !this.workitemData.selected ){
        this.workitemData.selected = false;
      }
      return this.workitemData.selected;
    };

    this.isLinkedToAccount = function(){
      if( !this.workitemData.account_id ){
        this.workitemData.account_id = -1;
      }
      return this.workitemData.account_id != -1;
    };

    this.performDoubleClick = function(){
      var workitem = this.workitemData;
      if(workitem.actions && workitem.actions.length>0){
        var openAction = null;
        angular.forEach(workitem.actions,function(each){
          if(each.code == 'Open'){
            openAction = each;
          }
        });
        window.location = openAction.url;
      }else{
        this.getWorkItemActions(this.performDoubleClick);
      }
    };

    /*
     * Service call to get workitem actions
     */
    this.getWorkItemActions = function(callbackFunction){
      var workitem = this.workitemData;

      //Call back parent controller's function
      this.whenSelected({
        selectedWorkItem: workitem
      });

      if(workitem.actions && workitem.actions.length>0){
        return;
      }
      WorkItemActionsSrv.getWorkItemActions(workitem.work_item_id,
      function(res){
        workitem.actions = res.data.response.results.actions;
        if(callbackFunction){
          callbackFunction(workitem.actions);
        }
        $log.log("workitem actions retrieved");
      },function(res){
        $log.log("Error retrieving workitem actions: " + res);
      });
    };

    this.actionClicked = function(action){
      this.whenActionClicked({
        selectedWorkItem: this.workitemData,
        action: action
      })
    };

    this.lookupValue = function(key,code){
      return WorkListViewSrv.lookupValue(key,code);
    };

    this.isWorkItemSubmitted = function(){
      return this.workitemData.external_id  &&  this.workitemData.external_id > 0;
    };
  }
})();
