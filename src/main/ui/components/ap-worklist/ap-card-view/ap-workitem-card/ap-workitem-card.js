module.exports = (function() {

  'use strict';

  angular.module('worklist')

  .component('apWorkitemCard', {
    controller: 'ap.worklist.cardCtrl',
    template: require('raw!./ap-workitem-card.html'),
    bindings: {
      workitemData : '=',
      whenSelected: '&',
      whenActionClicked: '&'
    }
  })

})();
