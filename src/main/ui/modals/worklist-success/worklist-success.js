module.exports = (function() {

'use strict';

angular.module('worklist')

	/* Success controller */
	.controller('WorkListSuccessCtrl', ['$scope', '$modalInstance', '$log', 'response', function ($scope, $modalInstance, $log, response) {
	  $scope.response = response;
	  $scope.successMessage = ap.htmlDecode(response.message);
	  $log.log($scope.successMessage);
	  $scope.no = function () {
	    $modalInstance.dismiss('cancel');
	  };
	}])

})();
