module.exports = (function() {

  require('./components/app.shared.js');



  angular.module('worklist', [
    'ui.bootstrap',
    'app.shared',
    'base64',
    'ngResource'
  ])

  require('./components/ap-worklist/ap-worklist.js');
  require('./services/worklist/worklist.js');
  require('./services/workitem-actions/workitem-actions.js');
  require('./services/workitem-create-quote/workitem-create-quote.js');
  require('./modals/saved-filter-delete/saved-filter-delete.js');
  require('./modals/workitem-link/workitem-link.js');
  require('./modals/workitem-move/workitem-move.js');
  require('./modals/worklist-confirmation/worklist-confirmation.js');
  require('./modals/worklist-success/worklist-success.js');


})();
