# Context

LOCAL MySQL Server
	
	<Resource name="agencyportal"
		auth="Container"
		type="javax.sql.DataSource"
		description="MySQL database for AgencyPortal"
		maxActive="100"
		maxIdle="30" 	
		maxWait="10000"
		username="agencyportal"
		password="Password123"
		driverClassName="com.mysql.jdbc.Driver"
		url="jdbc:mysql://localhost:3306/agencyportal" />  