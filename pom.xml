<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>com.agencyport.digital</groupId>
  <artifactId>worklist</artifactId>
  <version>1.0-SNAPSHOT</version>
  <packaging>war</packaging>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <spring.version>4.2.5.RELEASE</spring.version>
    <springdatajpa.version>1.9.4.RELEASE</springdatajpa.version>
    <mysqldriver.version>5.1.38</mysqldriver.version>
  </properties>

	<repositories>
		<repository>
			<id>nexus</id>
			<url>http://lab-repo01.agencyport.com:8082/repository/maven-public/</url>
		</repository>
	</repositories>

	<!-- Nexus Repository to upload war to -->
    <distributionManagement>
        <repository>
            <id>releases</id>
            <url>http://lab-repo01.agencyport.com:8082/repository/releases/</url>
        </repository>
        <snapshotRepository>
        	<id>snapshots</id>
            <url>http://lab-repo01.agencyport.com:8082/repository/snapshots/</url>
        </snapshotRepository>
     </distributionManagement>

  <dependencies>
    <dependency>
			<groupId>com.agencyport.sdk</groupId>
			<artifactId>apbase</artifactId>
			<version>6.0.0-SNAPSHOT</version>
		</dependency>

		<dependency>
			<groupId>com.agencyport.sdk</groupId>
			<artifactId>apsecurity</artifactId>
			<version>6.0.0-SNAPSHOT</version>
		</dependency>

		<dependency>
			<groupId>com.agencyport.sdk</groupId>
			<artifactId>apwebapp</artifactId>
			<version>6.0.0-SNAPSHOT</version>
			<exclusions>
				<exclusion>
					<artifactId>solr-core</artifactId>
					<groupId>org.apache.solr</groupId>
				</exclusion>
			</exclusions>
		</dependency>

		<dependency>
			<groupId>com.agencyport.sdk</groupId>
			<artifactId>apredissecurity</artifactId>
			<version>6.0.0-SNAPSHOT</version>
		</dependency>

		<dependency>
			<groupId>com.agencyport.integration</groupId>
			<artifactId>clientAPI</artifactId>
			<version>3.0.0-SNAPSHOT</version>
		</dependency>
		
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context</artifactId>
			<version>${spring.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-web</artifactId>
			<version>${spring.version}</version>
		</dependency>
		
		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>javax.servlet-api</artifactId>
			<version>3.0.1</version>
		</dependency>
		
		<dependency>
			<groupId>org.apache.solr</groupId>
			<artifactId>solr-solrj</artifactId>
			<version>5.5.0</version>
		</dependency>

	    <dependency>
	      	<groupId>junit</groupId>
	      	<artifactId>junit</artifactId>
	      	<version>4.12</version>
	      	<scope>test</scope>
	    </dependency>
	    
	    <dependency>
			<groupId>com.agencyport.sdk</groupId>
			<version>6.0.0-SNAPSHOT</version>
			<artifactId>aptest</artifactId>
			<scope>test</scope>
		</dependency>
		
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-test</artifactId>
			<version>${spring.version}</version>
			<scope>test</scope>
		</dependency>
		
		<dependency>
			<groupId>org.springframework.data</groupId>
			<artifactId>spring-data-jpa</artifactId>
			<version>${springdatajpa.version}</version>
		</dependency>
		
		<dependency>
	      <groupId>com.h2database</groupId>
	      <artifactId>h2</artifactId> 
	      <version>1.3.166</version>
	      <scope>test</scope>
	    </dependency>
	    
	    <!-- My Sql -->
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
			<version>${mysqldriver.version}</version>
		</dependency>
    
  </dependencies>

  <build>
  	<finalName>${project.artifactId}</finalName>
		<plugins>
			
			<!-- Source Code Compliance settings -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.5</version>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
					<encoding>UTF-8</encoding>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-war-plugin</artifactId>
				<version>2.6</version>
				<configuration>
					<failOnMissingWebXml>false</failOnMissingWebXml>
					<archiveClasses>true</archiveClasses>
					<attachClasses>true</attachClasses>
					<archive>
						<manifest>
							<addClasspath>true</addClasspath>
							<addDefaultImplementationEntries>true</addDefaultImplementationEntries>
							<addDefaultSpecificationEntries>true</addDefaultSpecificationEntries>
						</manifest>
						<manifestEntries>
							<Implementation-Vendor>Agencyport Software</Implementation-Vendor>
							<Build-Date>${timestamp}</Build-Date>
						</manifestEntries>
					</archive>
				</configuration>
			</plugin>

			<!-- all submodule monifest files need this timestamp -->
			<plugin>
				<groupId>com.keyboardsamurais.maven</groupId>
				<artifactId>maven-timestamp-plugin</artifactId>
				<version>1.0</version>
				<configuration>
					<propertyName>timestamp</propertyName>
					<timestampPattern>MMMMM dd yyyy h:mm aaa</timestampPattern>
				</configuration>
				<executions>
					<execution>
						<goals>
							<goal>create</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>2.16</version>
			</plugin>
			
			<plugin>
				<artifactId>maven-resources-plugin</artifactId>
				<version>2.6</version>
				<executions>
					<execution>
						<id>copy-resources</id>
						<phase>process-test-classes</phase>
						<goals>
							<goal>copy-resources</goal>
						</goals>
						<configuration>
							<outputDirectory>${project.build.directory}/junit-runtime/product</outputDirectory>
							<resources>
								<resource>
									<directory>${project.basedir}/src/main/webapp/WEB-INF</directory>
									<filtering>true</filtering>
								</resource>
							</resources>
						</configuration>
					</execution>
				</executions>
			</plugin>

		</plugins>

		<pluginManagement>
			<plugins>
				<!--This plugin's configuration is used to store Eclipse m2e settings
					only. It has no influence on the Maven build itself. -->
				<plugin>
					<groupId>org.eclipse.m2e</groupId>
					<artifactId>lifecycle-mapping</artifactId>
					<version>1.0.0</version>
					<configuration>
						<lifecycleMappingMetadata>
							<pluginExecutions>
								<pluginExecution>
									<pluginExecutionFilter>
										<groupId>
											com.keyboardsamurais.maven
										</groupId>
										<artifactId>
											maven-timestamp-plugin
										</artifactId>
										<versionRange>
											[1.0,)
										</versionRange>
										<goals>
											<goal>create</goal>
										</goals>
									</pluginExecutionFilter>
									<action>
										<ignore></ignore>
									</action>
								</pluginExecution>
							</pluginExecutions>
						</lifecycleMappingMetadata>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>

</project>
